
class DataMonitoring {
  final String textForm, img;
  final int id;
  DataMonitoring({
    this.textForm,
    this.img,
    this.id,
  });
}

List<DataMonitoring> dataMonitoring = [
  DataMonitoring(
    textForm: "Chấm công",
    img: "assets/images/ic_time_sheet.png",
    id: 0,
  ),
  // DataMonitoring(
  //   textForm:"Viếng thăm",
  //   img: "assets/images/ic_located.png",
  //   id: 1,
  // ),
  DataMonitoring(
    textForm: "KPis",
    img: "assets/images/ic_kpi.png",
    id: 1,
  ),
  DataMonitoring(
    textForm: "Công việc",
    img: "assets/images/ic_calendar.png",
    id: 2,
  ),
  // DataMonitoring(
  //   textForm: "Khách hàng",
  //   img: "assets/images/ic_people.png",
  //   id: 4,
  // ),
  // DataMonitoring(
  //   textForm: "Cơ hội bán hàng",
  //   img: "assets/images/ic_responsibility.png",
  //   id: 5,
  // ),
  // DataMonitoring(
  //   textForm: "Chăm sóc khách hàng",
  //   img: "assets/images/ic_take_care.png",
  //   id: 6,
  // ),
  DataMonitoring(
    textForm: "Duyệt đơn",
    img: "assets/images/ic_browse.png",
    id: 3,
  ),
  DataMonitoring(
    textForm: "Duyệt đề xuất",
    img: "assets/images/ic_proposal.png",
    id: 4,
  ),
  // DataMonitoring(
  //   textForm: "Hủy",
  //   img: "assets/images/ic_cancel.png",
  //   id: 9,
  // ),
];
