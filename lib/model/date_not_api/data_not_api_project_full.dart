
class DataProjectFull {
  final String textReadReport,textImportant,textRework,textTime, imgLeft,imgMid,imgRight;
  final int id;

  DataProjectFull({
    this.textReadReport,
    this.textImportant,
    this.textRework,
    this.textTime,
    this.imgLeft,
    this.imgMid,
    this.imgRight,
    this.id,
  });
}

List<DataProjectFull> dataProjectFull = [
  DataProjectFull(
    textReadReport: "Viết báo cáo",
    textImportant: "Quan trọng",
    textRework: "Làm lại",
    textTime: "03/03 08:00 - 04/03 10:00 giao bởi",
    imgLeft: "assets/images/ic_check.png",
    imgMid: "assets/images/ic_calender.png",
    imgRight:"assets/images/ic_three_dot_two.png",
    id: 0,
  ),
  DataProjectFull(
    textReadReport: "Viết báo cáo",
    textImportant: "Quan trọng",
    textRework: "Làm lại",
    textTime: "03/03 08:00 - 04/03 10:00 giao bởi",
    imgLeft: "assets/images/ic_check.png",
    imgMid: "assets/images/ic_calender.png",
    imgRight:"assets/images/ic_three_dot_two.png",
    id: 1,
  ),
  DataProjectFull(
    textReadReport: "Viết báo cáo",
    textImportant: "Quan trọng",
    textRework: "Làm lại",
    textTime: "03/03 08:00 - 04/03 10:00 giao bởi",
    imgLeft: "assets/images/ic_check.png",
    imgMid: "assets/images/ic_calender.png",
    imgRight:"assets/images/ic_three_dot_two.png",
    id: 2,
  ),
  DataProjectFull(
    textReadReport: "Viết báo cáo",
    textImportant: "Quan trọng",
    textRework: "Làm lại",
    textTime: "03/03 08:00 - 04/03 10:00 giao bởi",
    imgLeft: "assets/images/ic_check.png",
    imgMid: "assets/images/ic_calender.png",
    imgRight:"assets/images/ic_three_dot_two.png",
    id: 3,
  ),
  DataProjectFull(
    textReadReport: "Viết báo cáo",
    textImportant: "Quan trọng",
    textRework: "Làm lại",
    textTime: "03/03 08:00 - 04/03 10:00 giao bởi",
    imgLeft: "assets/images/ic_check.png",
    imgMid: "assets/images/ic_calender.png",
    imgRight:"assets/images/ic_three_dot_two.png",
    id: 4,
  ),DataProjectFull(
    textReadReport: "Viết báo cáo",
    textImportant: "Quan trọng",
    textRework: "Làm lại",
    textTime: "03/03 08:00 - 04/03 10:00 giao bởi",
    imgLeft: "assets/images/ic_check.png",
    imgMid: "assets/images/ic_calender.png",
    imgRight:"assets/images/ic_three_dot_two.png",
    id: 5,
  ),
];
