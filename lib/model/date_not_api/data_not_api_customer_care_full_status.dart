
class DataStatusFull {
  final String textForm, img;
  final int id,number;
  DataStatusFull({
    this.textForm,
    this.img,
    this.id,
    this.number
  });
}

List<DataStatusFull> dataStatusFull = [
  DataStatusFull(
    textForm: "Tất cả trạng thái",
    img: "assets/images/ic_dotnet_one.png",
    number: 0,
    id: 0,
  ),
  DataStatusFull(
    textForm: "Đã viếng thăm",
    img: "assets/images/ic_dotnet_one.png",
    number: 0,
    id: 1,
  ),
  DataStatusFull(
    textForm: "Chưa viếng thăm",
    img: "assets/images/ic_dotnet_one.png",
    number: 0,
    id: 2,
  ),
  DataStatusFull(
    textForm: "Không viếng thăm được",
    img: "assets/images/ic_dotnet_one.png",
    number: 0,
    id: 3,
  ),



];
