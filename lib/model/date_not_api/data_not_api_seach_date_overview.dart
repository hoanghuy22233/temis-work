
class DataDateOverview {
  final String textForm;
  final int id;
  DataDateOverview({
    this.textForm,
    this.id,
  });
}

List<DataDateOverview> dataDateOverview = [
  DataDateOverview(
    textForm: "Hôm nay",
    id: 0,
  ),
  DataDateOverview(
    textForm: "Tuần này",
    id: 1,
  ),
  DataDateOverview(
    textForm: "Tháng này",
    id: 2,
  ),
  DataDateOverview(
    textForm: "Ngày mai",
    id: 3,
  ),
  DataDateOverview(
    textForm: "Tuần sau",
    id: 4,
  ),
  DataDateOverview(
    textForm: "Tháng trước",
    id: 5,
  ),


];
