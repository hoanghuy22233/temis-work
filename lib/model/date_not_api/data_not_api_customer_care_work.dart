
class DataCustomerCareWork {
  final String textForm, img;
  final int id,number;
  DataCustomerCareWork({
    this.textForm,
    this.img,
    this.id,
    this.number
  });
}

List<DataCustomerCareWork> dataCustomerCareWork = [
  DataCustomerCareWork(
    textForm: "Tất cả trạng thái",
    img: "assets/images/ic_dotnet_one.png",
    number: 0,
    id: 0,
  ),
  DataCustomerCareWork(
    textForm: "Chưa thực hiện",
    img: "assets/images/ic_dotnet_one.png",
    number: 0,
    id: 1,
  ),
  DataCustomerCareWork(
    textForm: "Đang thực hiện",
    img: "assets/images/ic_dotnet_one.png",
    number: 0,
    id: 2,
  ),
  DataCustomerCareWork(
    textForm: "Hoàn thành",
    img: "assets/images/ic_dotnet_one.png",
    number: 0,
    id: 3,
  ),



];
