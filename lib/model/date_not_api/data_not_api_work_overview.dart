import 'package:flutter/cupertino.dart';

class DataWorkOverView {
  final String title, textWork,textPercent,textDivide;
  Color backgroundColor ;
  final int id;

  DataWorkOverView({
    this.title,
    this.textWork,
    this.textPercent,
    this.textDivide,
    this.backgroundColor,
    this.id,
  });
}

List<DataWorkOverView> dataWorkOverView = [
  DataWorkOverView(
    title: "Đang làm",
    textWork: "Công việc",
    textDivide: "7/25",
    textPercent: "28%",
    backgroundColor: Color(0xff68ba4e),
    id: 0,
  ),
  DataWorkOverView(
    title: "Đang làm quá hạn",
    textWork: "Công việc đang làm",
    textDivide: "6/7",
    textPercent: "86%",
    backgroundColor: Color(0xff48a7fa),
    id: 1,
  ),
  DataWorkOverView(
    title: "Hoàn thành",
    textWork: "Công việc",
    textDivide: "7/25",
    textPercent: "68%",
    backgroundColor: Color(0xfffc8562),
    id: 2,
  ),
  DataWorkOverView(
    title: "Hoàn thành quá hạn",
    textWork: "Công việc hoàn thành",
    textDivide: "2/17",
    textPercent: "12%",
    backgroundColor: Color(0xff3ae0c1),
    id: 3,
  ),
  DataWorkOverView(
    title: "việc chưa làm",
    textWork: "Công việc",
    textDivide: "1/25",
    textPercent: "4%",
    backgroundColor: Color(0xfffacb2f),
    id: 4,
  ),

];
