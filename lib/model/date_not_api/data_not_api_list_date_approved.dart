
class DataListDateApproved {
  final String textForm;
  final int id;
  DataListDateApproved({
    this.textForm,
    this.id,
  });
}

List<DataListDateApproved> dataListDateApproved = [
  DataListDateApproved(
    textForm: "Năm nay",
    id: 0,
  ),
  DataListDateApproved(
    textForm: "Quý trước",
    id: 1,
  ),
  DataListDateApproved(
    textForm: "Tháng trước",
    id: 2,
  ),
  DataListDateApproved(
    textForm: "Quý này",
    id: 3,
  ),
  DataListDateApproved(
    textForm: "Tháng này",
    id: 4,
  ),
  DataListDateApproved(
    textForm: "Tuần này",
    id: 5,
  ),
  DataListDateApproved(
    textForm: "Hôm nay",
    id: 6,
  ),
];
