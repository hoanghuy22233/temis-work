
class DataSeachDateWork {
  final String textForm;
  final int id;
  DataSeachDateWork({
    this.textForm,
    this.id,
  });
}

List<DataSeachDateWork> dataSeachDateWork = [
  DataSeachDateWork(
    textForm: "Tất cả công việc",
    id: 0,
  ),
  DataSeachDateWork(
    textForm: "Công việc đã hoàn thành",
    id: 1,
  ),
  DataSeachDateWork(
    textForm: "Công việc hoàn thành quá hạn",
    id: 2,
  ),
  DataSeachDateWork(
    textForm: "Công việc đang thực hiện",
    id: 3,
  ),
  DataSeachDateWork(
    textForm: "Công việc đang thực hiện quá hạn",
    id: 4,
  ),
  DataSeachDateWork(
    textForm: "Công việc chưa thực hiện",
    id: 5,
  ),
  DataSeachDateWork(
    textForm: "Công việc chưa thực hiện quá hạn",
    id: 6,
  ),
  DataSeachDateWork(
    textForm: "Công việc tạm dừng",
    id: 7,
  ),
  DataSeachDateWork(
    textForm: "Công việc hủy",
    id: 8,
  ),

];
