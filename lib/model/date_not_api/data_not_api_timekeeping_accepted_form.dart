class DataTimeKeepingAccepted{
  final String name;
  final String date;
  final String reason;
  final String shift;

  final String timeBegin;
  final String timeEnd;
  final String description;
  final String reasonAccepted;
  final String approvedBy;
  final String supervisor;
  final String supervisorEmail;
  final String createdDate;
  final String createdTime;
  final String creator;
  final String approvedTime;
  final String approvedDate;


  DataTimeKeepingAccepted({this.name,this.createdTime, this.date, this.reason, this.shift, this.timeBegin, this.timeEnd,
    this.description, this.reasonAccepted, this.approvedBy, this.supervisor, this.supervisorEmail, this.createdDate, this.creator, this.approvedTime, this.approvedDate});


}
List<DataTimeKeepingAccepted> dataTimeKeepingAccepted = [
  DataTimeKeepingAccepted(
      name: "Đơn xin vắng mặt",
      date: "31/3/2021",
      reason: "Bận việc gia đình",
      shift: "Ca hành chính",
      timeBegin: "08:00",
      timeEnd: "17:00",
      description: "Ốm bệnh",
      reasonAccepted: "Lý do hợp lý",
      approvedBy: "Lê Thị Hằng",
      supervisor: "Nguyễn Hoàng Huy",
      supervisorEmail: "hoanghuy@temisvn",
      createdDate: "19/02/2020",
      createdTime: "09:06",
      approvedTime: "09:16",
      approvedDate: "16/3/2021",
      creator: "Nguyễn Bùi Tuấn Anh"
  ),

];