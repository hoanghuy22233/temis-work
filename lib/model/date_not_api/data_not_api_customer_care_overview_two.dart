
class DataOverviewTwo {
  final String textForm, img;
  final int id,number;
  DataOverviewTwo({
    this.textForm,
    this.img,
    this.id,
    this.number
  });
}

List<DataOverviewTwo> dataOverviewTwo = [
  DataOverviewTwo(
    textForm: "Số lịch phải thực hiện",
    img: "assets/images/ic_dotnet_one.png",
    number: 0,
    id: 0,
  ),
  DataOverviewTwo(
    textForm: "Số lịch đã thực hiện",
    img: "assets/images/ic_dotnet_one.png",
    number: 0,
    id: 0,
  ),
  DataOverviewTwo(
    textForm: "Số lịch không thực hiện được",
    img: "assets/images/ic_dotnet_one.png",
    number: 0,
    id: 0,
  ),
  DataOverviewTwo(
    textForm: "Số lịch chưa thực hiện",
    img: "assets/images/ic_dotnet_one.png",
    number: 0,
    id: 0,
  ),
  DataOverviewTwo(
    textForm: "Số lịch quá hạn thực hiện",
    img: "assets/images/ic_dotnet_one.png",
    number: 0,
    id: 0,
  ),
  DataOverviewTwo(
    textForm: "Số lần liên hệ với khách hàng",
    img: "assets/images/ic_dotnet_one.png",
    number: 0,
    id: 0,
  ),
  DataOverviewTwo(
    textForm: "Số lần viếng thăm khách hàng",
    img: "assets/images/ic_dotnet_one.png",
    number: 0,
    id: 0,
  ),



];
