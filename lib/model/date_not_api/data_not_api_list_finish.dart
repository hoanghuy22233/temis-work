
class DataListFinish {
  final String textForm;
  final int id;
  DataListFinish({
    this.textForm,
    this.id,
  });
}

List<DataListFinish> dataListFinish = [
  DataListFinish(
    textForm: "Hoàn thành tháng này",
    id: 0,
  ),
  DataListFinish(
    textForm: "Hoàn thành tháng trước",
    id: 1,
  ),
  DataListFinish(
    textForm: "Hoàn thành tháng trước nữa",
    id: 2,
  ),
  DataListFinish(
    textForm: "Tạo tháng này",
    id: 3,
  ),
  DataListFinish(
    textForm: "Tạo tháng trước",
    id: 4,
  ),
  DataListFinish(
    textForm: "Tạo tháng trước nữa",
    id: 5,
  ),

];
