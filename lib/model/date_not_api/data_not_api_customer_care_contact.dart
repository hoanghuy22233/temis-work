
class DataCustomerCareContact {
  final String textForm, img;
  final int id,number;
  DataCustomerCareContact({
    this.textForm,
    this.img,
    this.id,
    this.number
  });
}

List<DataCustomerCareContact> dataCustomerCareContact = [
  DataCustomerCareContact(
    textForm: "Tất cả trạng thái",
    img: "assets/images/ic_dotnet_one.png",
    number: 0,
    id: 0,
  ),
  DataCustomerCareContact(
    textForm: "Đã liên lạc",
    img: "assets/images/ic_dotnet_one.png",
    number: 0,
    id: 1,
  ),
  DataCustomerCareContact(
    textForm: "Chưa liên lạc",
    img: "assets/images/ic_dotnet_one.png",
    number: 0,
    id: 2,
  ),
  DataCustomerCareContact(
    textForm: "Không liên lạc được",
    img: "assets/images/ic_dotnet_one.png",
    number: 0,
    id: 3,
  ),



];
