import 'package:flutter/cupertino.dart';

class DataHome {
  final String title, img;
  Color backgroundColor ;
  final int id;

// final int backgroundColor;
  DataHome({
    this.title,
    this.img,
    this.backgroundColor,
    this.id,
  });
}

List<DataHome> dataHome = [
  DataHome(
    title: "Giám sát",
    img: "assets/images/checklists.png",
    backgroundColor: Color(0xff68ba4e),
    id: 0,
  ),
  DataHome(
    title: "Lịch biểu",
    img: "assets/images/bar_chart_two.png",
    backgroundColor: Color(0xff48a7fa),
    id: 1,
  ),
  DataHome(
    title: "WORK+",
    img: "assets/images/ic_work.png",
    backgroundColor: Color(0xfffc8562),
    id: 2,
  ),
  DataHome(
    title: "OFFICE+",
    img: "assets/images/ic_office_two.png",
    backgroundColor: Color(0xff3ae0c1),
    id: 3,
  ),
  DataHome(
    title: "CRM+",
    img: "assets/images/ic_crm_one.png",
    backgroundColor: Color(0xfffacb2f),
    id: 4,
  ),
  DataHome(
    title: "HRM+",
    img: "assets/images/ic_hrm_three.png",
    backgroundColor: Color(0xff8479d4),
    id: 5,
  ),
];
