class DataProposeAccepted{
  final String name;
  final String date;
  final String type;
  final String proposer;
  final String approver;
  final String description;
  final String target_name;
  final String department;
  final String reason;
  final String solution;
  final String status;
  final String procedure;
  final String executor;
  final String notification_Recipients;
  final String approvedBy;
  final String supervisor;
  final String approvedDate;
  final String approvedTime;

  DataProposeAccepted({this.approvedTime,this.approvedDate,this.supervisor,this.approvedBy,this.name, this.date, this.type, this.proposer, this.approver, this.description, this.target_name, this.department, this.reason, this.solution, this.status, this.procedure, this.executor, this.notification_Recipients});




}
List <DataProposeAccepted> dataProposeAccepted =[
  DataProposeAccepted(
    name: "Kỷ luật trưởng phòng",
    type: "Kỷ luật",
    description: ":v",
    target_name: "Nguyễn Hoàng Huy",
    department: "IT",
    reason: "làm ồn",
    solution: "Đuổi việc",
    date: "11/3/2021",
    proposer: "Nguyễn Bùi Tuấn Anh",
    status: "Đã duyệt",
    procedure: "Chỉ cần một người duyệt",
    approver: "Giám đốc",
    executor: "Nguyễn Hoàng Huy",
    approvedBy: "abcxyz",

    notification_Recipients: "Nguyễn Hoàng Huy"
  ),
  DataProposeAccepted(
      name: "Kỷ luật trưởng phòng",
      type: "Kỷ luật",
      description: ":v",
      target_name: "Nguyễn Hoàng Huy",
      department: "IT",
      reason: "làm ồn",
      solution: "Đuổi việc",
      date: "11/3/2021",
      proposer: "Nguyễn Bùi Tuấn Anh",
      status: "Đã duyệt",
      procedure: "Chỉ cần một người duyệt",
      approver: "Giám đốc",
      executor: "Nguyễn Hoàng Huy",
      approvedBy: "abcxyz",

      notification_Recipients: "Nguyễn Hoàng Huy"
  ),
  DataProposeAccepted(
      name: "Kỷ luật trưởng phòng",
      type: "Kỷ luật",
      description: ":v",
      target_name: "Nguyễn Hoàng Huy",
      department: "IT",
      reason: "làm ồn",
      solution: "Đuổi việc",
      date: "11/3/2021",
      proposer: "Nguyễn Bùi Tuấn Anh",
      status: "Đã duyệt",
      procedure: "Chỉ cần một người duyệt",
      approver: "Giám đốc",
      executor: "Nguyễn Hoàng Huy",
      approvedBy: "abcxyz",

      notification_Recipients: "Nguyễn Hoàng Huy"
  ),
  DataProposeAccepted(
      name: "Kỷ luật trưởng phòng",
      type: "Kỷ luật",
      description: ":v",
      target_name: "Nguyễn Hoàng Huy",
      department: "IT",
      reason: "làm ồn",
      solution: "Đuổi việc",
      date: "11/3/2021",
      proposer: "Nguyễn Bùi Tuấn Anh",
      status: "Đã duyệt",
      procedure: "Chỉ cần một người duyệt",
      approver: "Giám đốc",
      executor: "Nguyễn Hoàng Huy",
      approvedBy: "abcxyz",

      notification_Recipients: "Nguyễn Hoàng Huy"
  ),
  DataProposeAccepted(
      name: "Kỷ luật trưởng phòng",
      type: "Kỷ luật",
      description: ":v",
      target_name: "Nguyễn Hoàng Huy",
      department: "IT",
      reason: "làm ồn",
      solution: "Đuổi việc",
      date: "11/3/2021",
      proposer: "Nguyễn Bùi Tuấn Anh",
      status: "Đã duyệt",
      procedure: "Chỉ cần một người duyệt",
      approver: "Giám đốc",
      executor: "Nguyễn Hoàng Huy",
      approvedBy: "abcxyz",

      notification_Recipients: "Nguyễn Hoàng Huy"
  ),

];
