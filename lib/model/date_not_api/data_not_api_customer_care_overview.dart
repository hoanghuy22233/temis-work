
class DataOverview {
  final String textForm, img;
  final int id,number;
  DataOverview({
    this.textForm,
    this.img,
    this.id,
    this.number
  });
}

List<DataOverview> dataOverview = [
  DataOverview(
    textForm: "Số khách hàng phải chăm sóc",
    img: "assets/images/ic_dotnet_one.png",
    number: 0,
    id: 0,
  ),
  DataOverview(
    textForm: "Số khách hàng đã chăm sóc",
    img: "assets/images/ic_dotnet_one.png",
    number: 0,
    id: 0,
  ),
  DataOverview(
    textForm: "Số khách hàng không chăm sóc được",
    img: "assets/images/ic_dotnet_one.png",
    number: 0,
    id: 0,
  ),
  DataOverview(
    textForm: "Số khách hàng chưa chăm sóc",
    img: "assets/images/ic_dotnet_one.png",
    number: 0,
    id: 0,
  ),
  DataOverview(
    textForm: "Số khách hàng đã viếng thăm",
    img: "assets/images/ic_dotnet_one.png",
    number: 0,
    id: 0,
  ),


];
