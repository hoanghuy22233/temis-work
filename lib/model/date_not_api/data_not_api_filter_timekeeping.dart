
class DataFillterTimeKeeping {
  final String textForm;
  final int id;

  DataFillterTimeKeeping({
    this.textForm,
    this.id,
  });
}

List<DataFillterTimeKeeping> dataFillterTimeKeeping = [
  DataFillterTimeKeeping(
    textForm: "Tất cả",
    id: 0,
  ),
  DataFillterTimeKeeping(
    textForm: "Lọc theo đơn nghỉ phép",
    id: 1,
  ),
  DataFillterTimeKeeping(
    textForm: "Lọc theo đơn vắng mặt",
    id: 2,
  ),
  DataFillterTimeKeeping(
    textForm: "Lọc theo đơn tăng ca",
    id: 3,
  ),
  DataFillterTimeKeeping(
    textForm: "Lọc theo đơn làm thêm",
    id: 4,
  ),
  DataFillterTimeKeeping(
    textForm: "Lọc theo đơn công tác",
    id: 5,
  ),
  DataFillterTimeKeeping(
    textForm: "Lọc theo đơn chế độ",
    id: 6,
  ),
  DataFillterTimeKeeping(
    textForm: "Lọc theo đơn đổi ca",
    id: 7,
  ),
  DataFillterTimeKeeping(
    textForm: "Lọc theo đơn giải trình chấm công",
    id: 8,
  ),
];