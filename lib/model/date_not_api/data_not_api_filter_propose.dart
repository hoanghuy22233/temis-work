class DataFillterPropose {
  final String textForm;
  final int id;

  DataFillterPropose({
    this.textForm,
    this.id,
  });
}

List<DataFillterPropose> dataFillterPropose = [
  DataFillterPropose(
    textForm: "Tất cả",
    id: 0,
  ),
  DataFillterPropose(
    textForm: "Tôi đề xuất",
    id: 1,
  ),
  DataFillterPropose(
    textForm: "Tôi theo dõi",
    id: 2,
  ),
  DataFillterPropose(
    textForm: "Tôi phê duyệt",
    id: 3,
  ),
  DataFillterPropose(
    textForm: "Tôi nhận thông báo",
    id: 4,
  ),
  DataFillterPropose(
    textForm: "Tôi thực hiện",
    id: 5,
  ),
];