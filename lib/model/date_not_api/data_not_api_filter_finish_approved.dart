class DataFilterFinishApproved {
  final String textForm;
  final int id;
  DataFilterFinishApproved({
    this.textForm,
    this.id,
  });
}

List<DataFilterFinishApproved> dataFilterFinishApproved = [
  DataFilterFinishApproved(
    textForm: "Đơn tôi duyệt",
    id: 0,
  ),
  DataFilterFinishApproved(
    textForm: "Tất cả",
    id: 1,
  ),
  DataFilterFinishApproved(
    textForm: "Temisvn",
    id: 2,
  ),
  DataFilterFinishApproved(
    textForm: "IT",
    id: 3,
  ),DataFilterFinishApproved(
    textForm: "Marketing",
    id: 4,
  ),
];
