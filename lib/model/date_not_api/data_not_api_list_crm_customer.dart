
class DataListCrmCustomer {
  final String textForm;
  final int id;
  DataListCrmCustomer({
    this.textForm,
    this.id,
  });
}

List<DataListCrmCustomer> dataListCrmCustomer = [
  DataListCrmCustomer(
    textForm: "Tăng dần theo tên khách hàng",
    id: 0,
  ),
  DataListCrmCustomer(
    textForm: "Giảm dần theo tên khách hàng",
    id: 1,
  ),
  DataListCrmCustomer(
    textForm: "Tăng dần theo ngày tạo",
    id: 2,
  ),
  DataListCrmCustomer(
    textForm: "Giảm dần theo ngày tạo",
    id: 3,
  ),
  DataListCrmCustomer(
    textForm: "Tăng dần theo ngày liên hệ",
    id: 4,
  ),
  DataListCrmCustomer(
    textForm: "Giảm dần theo ngày liên hệ",
    id: 5,
  ),
  DataListCrmCustomer(
    textForm: "Tăng dần theo ngày cập nhật",
    id: 6,
  ),
  DataListCrmCustomer(
    textForm: "Tăng dần theo ngày cập nhật",
    id: 7,
  ),

];
