
class DataListCrmContact {
  final String textForm;
  final int id;
  DataListCrmContact({
    this.textForm,
    this.id,
  });
}

List<DataListCrmContact> dataListCrmContact = [
  DataListCrmContact(
    textForm: "Tăng dần theo tên liên hệ",
    id: 0,
  ),
  DataListCrmContact(
    textForm: "Giảm dần theo tên liên hệ",
    id: 1,
  ),
  DataListCrmContact(
    textForm: "Tăng dần theo ngày tạo",
    id: 2,
  ),
  DataListCrmContact(
    textForm: "Giảm dần theo ngày tạo",
    id: 3,
  ),
  DataListCrmContact(
    textForm: "Tăng dần theo ngày cập nhật",
    id: 4,
  ),
  DataListCrmContact(
    textForm: "Tăng dần theo ngày cập nhật",
    id: 5,
  ),

];
