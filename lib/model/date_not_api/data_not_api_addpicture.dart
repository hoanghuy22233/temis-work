class DataAddPicture{
  final String textForm;
  final String img;
  final int id;

  DataAddPicture({this.textForm, this.img, this.id});

}
List <DataAddPicture> dataAddPicture = [
  DataAddPicture(
    id: 0,
    img: "assets/images/img_camera.png",
    textForm: "Chụp ảnh",

  ),
  DataAddPicture(
    id: 1,
    img: "assets/images/insert-picture-icon.png",
    textForm: "Chọn ảnh từ bộ sưu tập",

  ),


];