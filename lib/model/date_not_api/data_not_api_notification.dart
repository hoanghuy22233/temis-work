
class DataNotification {
  final String textForm,textDate,textDateFull, img,imgDate;
  final int id;
  DataNotification({
    this.textForm,
    this.textDate,
    this.textDateFull,
    this.img,
    this.imgDate,
    this.id,
  });
}

List<DataNotification> dataNotification = [
  DataNotification(
    textForm: "Nguyễn Bùi Tuấn Anh đã chuyển trạng thái Đang thực hiện, cập nhật tiến độ 50% công việc Giao diện demo",
    textDate: "2 giờ trước",
    textDateFull: "Thứ 3, 09/03/2021",
    img: "assets/images/image_avartar_girl_two.png",
    imgDate:"assets/images/date_off_three.png",
    id: 0,
  ),
  DataNotification(
    textForm: "Nguyễn Bùi Tuấn Anh đã chuyển trạng thái Đang thực hiện, cập nhật tiến độ 50% công việc Giao diện demo",
    textDate: "2 giờ trước",
    textDateFull: "Thứ 4, 09/03/2021",
    img: "assets/images/image_avartar_girl_two.png",
    imgDate:"assets/images/date_off_three.png",
    id: 0,
  ),
  DataNotification(
    textForm: "Nguyễn Bùi Tuấn Anh đã chuyển trạng thái Đang thực hiện, cập nhật tiến độ 50% công việc Giao diện demo",
    textDate: "2 giờ trước",
    textDateFull: "Thứ 5, 09/03/2021",
    img: "assets/images/image_avartar_girl_two.png",
    imgDate:"assets/images/date_off_three.png",
    id: 0,
  ),
  DataNotification(
    textForm: "Nguyễn Bùi Tuấn Anh đã chuyển trạng thái Đang thực hiện, cập nhật tiến độ 50% công việc Giao diện demo",
    textDate: "2 giờ trước",
    textDateFull: "Thứ 6, 09/03/2021",
    img: "assets/images/image_avartar_girl_two.png",
    imgDate:"assets/images/date_off_three.png",
    id: 0,
  ),




];
