
class DataSeachDateRequire {
  final String textForm, img;
  final int id,number;
  DataSeachDateRequire({
    this.textForm,
    this.img,
    this.id,
    this.number
  });
}

List<DataSeachDateRequire> dataSeachDateRequire = [
  DataSeachDateRequire(
    textForm: "Năm nay",
    number: 0,
    id: 0,
  ),
  DataSeachDateRequire(
    textForm: "Tháng trước",
    number: 0,
    id: 1,
  ),
  DataSeachDateRequire(
    textForm: "Quý trước",
    number: 0,
    id: 2,
  ),
  DataSeachDateRequire(
    textForm: "Quý này",
    number: 0,
    id: 3,
  ),
  DataSeachDateRequire(
    textForm: "Tháng này",
    number: 0,
    id: 4,
  ),
  DataSeachDateRequire(
    textForm: "30 ngày qua",
    number: 0,
    id: 5,
  ),
  DataSeachDateRequire(
    textForm: "Tuần này",
    number: 0,
    id: 6,
  ),
  DataSeachDateRequire(
    textForm: "Hôm nay",
    number: 0,
    id: 7,
  ),




];
