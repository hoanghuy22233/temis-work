import 'package:base_code_project/model/api/response/barrel_response.dart';
import 'package:base_code_project/model/entity/barrel_entity.dart';
import 'package:json_annotation/json_annotation.dart';

part 'app_config_response.g.dart';

@JsonSerializable()
class AppConfigResponse extends BaseResponse {
  AppConfigData data;

  AppConfigResponse(this.data);

  factory AppConfigResponse.fromJson(Map<String, dynamic> json) =>
      _$AppConfigResponseFromJson(json);

  Map<String, dynamic> toJson() => _$AppConfigResponseToJson(this);

  @override
  String toString() {
    return 'AppConfigResponse{data: $data}';
  }
}
