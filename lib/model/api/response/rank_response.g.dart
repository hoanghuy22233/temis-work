// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'rank_response.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

RankResponse _$RankResponseFromJson(Map<String, dynamic> json) {
  return RankResponse(
    json['data'] == null
        ? null
        : RankData.fromJson(json['data'] as Map<String, dynamic>),
  )
    ..status = json['status'] as int
    ..message = json['message'] as String;
}

Map<String, dynamic> _$RankResponseToJson(RankResponse instance) =>
    <String, dynamic>{
      'status': instance.status,
      'message': instance.message,
      'data': instance.data,
    };
