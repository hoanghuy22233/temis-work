import 'dart:io';

import 'package:base_code_project/app/constants/barrel_constants.dart';
import 'package:base_code_project/model/api/request/forgot_password_request.dart';
import 'package:base_code_project/model/api/request/forgot_password_reset_request.dart';
import 'package:base_code_project/model/api/request/forgot_password_verify_request.dart';
import 'package:base_code_project/model/api/request/register_app_request.dart';
import 'package:base_code_project/model/api/request/register_verify_request.dart';
import 'package:base_code_project/model/api/request/resend_otp_request.dart';
import 'package:base_code_project/model/api/request/update_birthday_request.dart';
import 'package:base_code_project/model/api/request/update_email_request.dart';
import 'package:base_code_project/model/api/request/update_email_verify_request.dart';
import 'package:base_code_project/model/api/request/update_name_request.dart';
import 'package:base_code_project/model/api/request/update_phone_request.dart';
import 'package:base_code_project/model/api/request/update_phone_verify_request.dart';
import 'package:base_code_project/model/api/response/app_config_response.dart';
import 'package:base_code_project/model/api/response/forgot_password_reset_response.dart';
import 'package:base_code_project/model/api/response/forgot_password_response.dart';
import 'package:base_code_project/model/api/response/forgot_password_verify_response.dart';
import 'package:base_code_project/model/api/response/profile_response.dart';
import 'package:base_code_project/model/api/response/rank_response.dart';
import 'package:base_code_project/model/api/response/register_response.dart';
import 'package:base_code_project/model/api/response/register_verify_response.dart';
import 'package:base_code_project/model/api/response/resend_otp_response.dart';
import 'package:base_code_project/model/api/response/update_avatar_response.dart';
import 'package:base_code_project/model/api/response/update_background_image_response.dart';
import 'package:base_code_project/model/api/response/update_birthday_response.dart';
import 'package:base_code_project/model/api/response/update_email_response.dart';
import 'package:base_code_project/model/api/response/update_name_response.dart';
import 'package:base_code_project/model/api/response/update_phone_response.dart';
import 'package:dio/dio.dart';
import 'package:retrofit/retrofit.dart';

import 'request/barrel_request.dart';
import 'response/barrel_response.dart';

part 'rest_client.g.dart';

@RestApi(baseUrl: Endpoint.BASE_URL)
abstract class RestClient {
  factory RestClient(Dio dio, {String baseUrl}) = _RestClient;

  @POST(Endpoint.LOGIN_APP)
  Future<LoginRegisterResponse> loginApp(
      @Body() LoginAppRequest loginAppRequest);
  @POST(Endpoint.FORGOT_PASSWORD)
  Future<ForgotPasswordResponse> forgotPassword(
      @Body() ForgotPasswordRequest forgotPasswordRequest);
  @POST(Endpoint.FORGOT_PASSWORD_VERIFY)
  Future<ForgotPasswordVerifyResponse> forgotPasswordVerify(
      @Body() ForgotPasswordVerifyRequest forgotPasswordVerifyRequest);
  @POST(Endpoint.RESEND_OTP)
  Future<ResendOtpResponse> resendOtp(
      @Body() ResendOtpRequest resendOtpRequest);
  @POST(Endpoint.FORGOT_PASSWORD_RESET)
  Future<ForgotPasswordResetResponse> forgotPasswordReset(
      @Body() ForgotPasswordResetRequest forgotPasswordResetRequest);
  @POST(Endpoint.REGISTER_ANH_QUAN_APP)
  Future<RegisterResponse> registerApp(
      @Body() RegisterAppRequest registerAppRequest);
  @POST(Endpoint.REGISTER_ANH_QUAN_VERIFY)
  Future<RegisterVerifyResponse> registerVerify(
      @Body() RegisterVerifyRequest registerVerifyRequest);
  @GET(Endpoint.PROFILE)
  Future<ProfileResponse> getProfile();
  @GET(Endpoint.APP_CONFIGS)
  Future<AppConfigResponse> getAppConfigs();
  @POST(Endpoint.UPDATE_AVATAR)
  Future<UpdateAvatarResponse> updateAvatar(
      @Part(name: 'avatar_image') File avatarFile);
  @POST(Endpoint.UPDATE_BACKGROUND_IMAGE)
  Future<UpdateBackgroundImageResponse> updateBackgroundImage(
      @Part(name: 'background_image') File backgroundImageFile);
  @POST(Endpoint.UPDATE_BIRTHDAY)
  Future<UpdateBirthdayResponse> updateBirthday(
      @Body() UpdateBirthdayRequest updateBirthdayRequest);
  @POST(Endpoint.UPDATE_EMAIL_VERIFY)
  Future<UpdateEmailResponse> updateEmailVerify(
      @Body() UpdateEmailVerifyRequest updateEmailVerifyRequest);
  @POST(Endpoint.UPDATE_EMAIL)
  Future<UpdateEmailResponse> updateEmail(
      @Body() UpdateEmailRequest updateEmailRequest);
  @POST(Endpoint.UPDATE_PHONE)
  Future<UpdatePhoneResponse> updatePhone(
      @Body() UpdatePhoneRequest updatePhoneRequest);
  @POST(Endpoint.UPDATE_PHONE_VERIFY)
  Future<UpdatePhoneResponse> updatePhoneVerify(
      @Body() UpdatePhoneVerifyRequest updatePhoneVerifyRequest);
  @POST(Endpoint.UPDATE_NAME)
  Future<UpdateNameResponse> updateName(
      @Body() UpdateNameRequest updateNameRequest);
  @GET(Endpoint.RANK)
  Future<RankResponse> getRank();
}
