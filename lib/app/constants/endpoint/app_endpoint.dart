class Endpoint {
  Endpoint._();

  static const BASE_URL = 'https://app.cchatclothes.vn';
  //AUTH ROUTE
  static const LOGIN_APP = '/api/loginApp';
  static const FORGOT_PASSWORD = '/api/forgetPassword';
  static const FORGOT_PASSWORD_VERIFY =
      '/api/checkOtpInForgotPassword';
  static const RESEND_OTP = '/api/resendCodeOtp';
  static const FORGOT_PASSWORD_RESET = '/api/resetPassword';
  static const REGISTER_ANH_QUAN_APP = '/api/registerApp';
  static const REGISTER_ANH_QUAN_VERIFY = '/api/checkOtpRegister';
  static const PROFILE = '/api/profileUser';
  static const APP_CONFIGS = '/api/getInfoAppConfig';
  static const UPDATE_AVATAR = '/api/updateAvatarUser';
  static const UPDATE_BACKGROUND_IMAGE = '/api/updateBackgroundImageUser';
  static const UPDATE_BIRTHDAY = '/api/updateBirthdayUser';
  static const UPDATE_EMAIL_VERIFY = '/api/sendOtpInUpdateEmailUser';
  static const UPDATE_EMAIL = '/api/updateEmailUser';
  static const UPDATE_PHONE = '/api/updatePhoneNumberUser';
  static const UPDATE_PHONE_VERIFY = '/api/sendOtpInUpdatePhoneUser';
  static const UPDATE_NAME = '/api/updateFullNameUser';
  static const RANK = '/api/ranking';

  static const int DEFAULT_LIMIT = 20;

  // request failed
  static const int FAILURE = 0;

  // request success
  static const int SUCCESS = 1;

  // request with token expire
  static const int TOKEN_EXPIRE = 2;

  // receiveTimeout
  static const int receiveTimeout = 15000;

  // connectTimeout
  static const int connectionTimeout = 15000;

  // method
  static const GET = 'GET';
  static const POST = 'POST';
  static const PUT = 'PUT';
  static const DELETE = 'DELETE';

  // get path
  static String getPath(String path) {
    return '$BASE_URL$path';
  }
}
