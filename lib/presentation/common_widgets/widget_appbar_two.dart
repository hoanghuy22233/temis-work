import 'package:base_code_project/app/constants/style/style.dart';
import 'package:base_code_project/app/constants/value/value.dart';
import 'package:base_code_project/presentation/common_widgets/widget_spacer.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class WidgetAppbarWithColor extends StatefulWidget {
  final String title;
  final List<Widget> left;
  final List<Widget> right;
  final Color indicatorColor;
  final Color backgroundColor;
  final Color textColor;
  final bool hasIndicator;
  final double height;

  WidgetAppbarWithColor(
      {Key key,
        this.title,
        this.left,
        this.right,
        this.indicatorColor,
        this.backgroundColor,
        this.hasIndicator = false,
        this.textColor,
        this.height = 55})
      : super(key: key);

  @override
  _WidgetAppbarWithColorState createState() => _WidgetAppbarWithColorState();
}

class _WidgetAppbarWithColorState extends State<WidgetAppbarWithColor> {
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Container(
          width: double.infinity,
          height: widget.height,
          decoration: BoxDecoration(
            color: widget.backgroundColor,
          ),
          //padding: EdgeInsets.only(top: 20),
          child: Stack(
            overflow: Overflow.visible,
            children: [
              widget.left != null
                  ? Positioned.fill(

                child: Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: widget.left,
                ),
              )
                  : SizedBox(),
              widget.right != null
                  ? Positioned.fill(
                bottom: 5,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: widget.right,
                ),
              )
                  : SizedBox(),
              if (widget.title != null) Positioned.fill(
                child: FractionallySizedBox(
                    widthFactor: .8,
                    child: Container(
                      height: AppValue.ACTION_BAR_HEIGHT * 1.25,
                      child: Center(
                        child: Text(
                          widget.title.toUpperCase(),
                          style: AppStyle.DEFAULT_MEDIUM.copyWith(
                              color: widget.textColor != null
                                  ? widget.textColor
                                  : Colors.black,
                              fontWeight: FontWeight.bold),
                        ),
                      ),
                    )),
              ) else SizedBox(),
            ],
          ),
        ),
        widget.hasIndicator
            ? Divider(
          height: 1,
          thickness: 1,
          color: widget.indicatorColor != null
              ? widget.indicatorColor
              : Colors.grey,
        )
            : WidgetSpacer(
          height: 0,
        )
      ],
    );
  }
}
