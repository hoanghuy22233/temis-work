import 'package:base_code_project/app/constants/barrel_constants.dart';
import 'package:base_code_project/presentation/common_widgets/widget_appbar_home.dart';
import 'package:flutter/material.dart';

class WidgetAppbarNotificationSeach extends StatelessWidget {
  final Function onTap;

  const WidgetAppbarNotificationSeach({Key key, this.onTap}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
        margin: const EdgeInsets.only(left: AppValue.ACTION_BAR_HEIGHT * 0.2),
        width: AppValue.ACTION_BAR_HEIGHT * 0.8,
        height: AppValue.ACTION_BAR_HEIGHT * 0.8,
        child: WidgetAppbarHome(
          icon: Image.asset('assets/images/ic_filter_mini_two.png',width:45,height: 45,color: Colors.white,),
          onTap: onTap ??
              () {
                // AwesomeDialog(
                //   context: context,
                //   // dialogType: DialogType.INFO,
                //   animType: AnimType.BOTTOMSLIDE,
                //   title: 'Thông báo',
                //   // desc: 'Chi tiết nội dung thông báo',
                //   body: Center(child: Text(
                //     'Thông báo nội bộ, vui lòng bấm "Đồng ý để biết rõ hơn"',
                //     style: TextStyle(fontStyle: FontStyle.italic),
                //   ),),
                //   // btnOkText: "text",
                //    btnCancelOnPress: () {
                //     // Text("thoát");
                //    },
                //   btnOkOnPress: () {
                //     Navigator.push(
                //         context,
                //         MaterialPageRoute(builder: (context) => NotificationPageScreen()));
                //         },
                // )..show();


        // AppNavigator.navigateAddProject();
              },
        )
    );
  }
}
