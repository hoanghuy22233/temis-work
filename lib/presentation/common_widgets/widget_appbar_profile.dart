import 'package:base_code_project/app/constants/style/style.dart';
import 'package:base_code_project/app/constants/value/value.dart';
import 'package:base_code_project/presentation/common_widgets/widget_spacer.dart';
import 'package:bubble_tab_indicator/bubble_tab_indicator.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';


class WidgetAppbarProfile extends StatefulWidget {
  final String title;
  final List<Widget> left;
  final List<Widget> right;
  final Color indicatorColor;
  final Color backgroundColor;
  final Color textColor;
  final bool hasIndicator;

  WidgetAppbarProfile(
      {Key key,
      this.title,
      this.left,
      this.right,
      this.indicatorColor,
      this.backgroundColor,
      this.hasIndicator = false,
      this.textColor,})
      : super(key: key);

  @override
  _WidgetAppbarProfileState createState() => _WidgetAppbarProfileState();
}

class _WidgetAppbarProfileState extends State<WidgetAppbarProfile> {
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Container(

          width: double.infinity,
          // height: AppValue.ACTION_BAR_HEIGHT * 2,
          height: AppValue.ACTION_BAR_HEIGHT ,
          decoration: BoxDecoration(
              color: Colors.blue,

          ),
          //padding: EdgeInsets.only(top: 15),
          child: Stack(
            // overflow: Overflow.visible,
            children: [
              widget.left != null
                  ? Positioned.fill(
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        //crossAxisAlignment: CrossAxisAlignment.center,
                        children: widget.left,
                      ),
                    )
                  : SizedBox(),
              widget.right != null
                  ? Positioned.fill(
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.end,
                        //crossAxisAlignment: CrossAxisAlignment.center,
                        children: widget.right,
                      ),
                    )
                  : SizedBox(),
              widget.title != null
                  ? Positioned.fill(
                child: Center(
                  child: Text(
                    widget.title,
                    style: AppStyle.DEFAULT_MEDIUM.copyWith(
                        color: widget.textColor != null ? widget.textColor : Colors.black,
                        fontWeight: FontWeight.bold),
                  ),
                ),

                // child: FractionallySizedBox(
                      //     widthFactor: .8,
                      //     child: Container(
                      //       height: AppValue.ACTION_BAR_HEIGHT * 1.25,
                      //       child: Center(
                      //         child: Text(
                      //           widget.title,
                      //           style: AppStyle.DEFAULT_MEDIUM.copyWith(
                      //               color: widget.textColor != null ? widget.textColor : Colors.black,
                      //               fontWeight: FontWeight.bold),
                      //         ),
                      //       ),
                      //     )),
                    )
                  : SizedBox(),
            ],
          ),
        ),
        widget.hasIndicator
            ? Divider(
                height: 1,
                thickness: 1,
                color: widget.indicatorColor != null
                    ? widget.indicatorColor
                    : Colors.grey,
              )
            : WidgetSpacer(
                height: 0,
              )
      ],
    );
  }

}


