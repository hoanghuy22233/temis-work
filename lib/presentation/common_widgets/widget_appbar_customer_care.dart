import 'package:base_code_project/app/constants/style/style.dart';
import 'package:base_code_project/app/constants/value/value.dart';
import 'package:base_code_project/presentation/common_widgets/widget_spacer.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';


class WidgetAppbarCustomerCare extends StatefulWidget {
  final String title;
  final String titleDate;
  final List<Widget> left;
  final List<Widget> right;
  final Color indicatorColor;
  final Color backgroundColor;
  final Color textColor;
  final bool hasIndicator;

  WidgetAppbarCustomerCare(
      {Key key,
      this.title, this.titleDate,
      this.left,
      this.right,
      this.indicatorColor,
      this.backgroundColor,
      this.hasIndicator = false,
      this.textColor,})
      : super(key: key);

  @override
  _WidgetAppbarProfileState createState() => _WidgetAppbarProfileState();
}

class _WidgetAppbarProfileState extends State<WidgetAppbarCustomerCare> {
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Container(
          margin: EdgeInsets.only(bottom: 10),
          width: double.infinity,
          height: AppValue.ACTION_BAR_HEIGHT ,
          decoration: BoxDecoration(
            color: Colors.blue,
            // borderRadius: BorderRadius.only(
            //     bottomLeft: Radius.circular(25),
            //     bottomRight: Radius.circular(25)
            // )
          ),
          padding: EdgeInsets.only(top: 5),
          child:
          Stack(
            // overflow: Overflow.visible,
            children: [
              widget.left != null
                  ? Positioned(
                top: 5,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: widget.left,
                ),
              )
                  : SizedBox(),
              widget.right != null
                  ? Positioned.fill(
                bottom: 20,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: widget.right,
                ),
              )
                  : SizedBox(),
              widget.title != null
                  ? Positioned.fill(
                // top: 5,
                left: 70,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      widget.title,
                      style: AppStyle.DEFAULT_MEDIUM.copyWith(
                          color: widget.textColor != null ? widget.textColor : Colors.black,
                          fontWeight: FontWeight.bold,fontSize: 14),
                    ),
                    widget.titleDate != null
                        ?   Text(
                      widget.titleDate,
                      style: AppStyle.DEFAULT_MEDIUM.copyWith(
                          color: widget.textColor != null ? widget.textColor : Colors.black,fontSize: 10
                      ),
                    )
                        : SizedBox(),
                  ],
                ),
              )
                  : SizedBox(),
            ],
          ),
        ),
        widget.hasIndicator
            ? Divider(
          height: 1,
          thickness: 1,
          color: widget.indicatorColor != null
              ? widget.indicatorColor
              : Colors.grey,
        )
            : WidgetSpacer(
          height: 0,
        )
      ],
    );
  }

}


