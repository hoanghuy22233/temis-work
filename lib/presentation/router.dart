import 'package:base_code_project/model/repo/barrel_repo.dart';
import 'package:base_code_project/presentation/screen/ProjectList/sc_list_project.dart';
import 'package:base_code_project/presentation/screen/add_project/sc_add_project.dart';
import 'package:base_code_project/presentation/screen/change_info_verify/sc_change_info_verify.dart';
import 'package:base_code_project/presentation/screen/contact/sc_contact.dart';
import 'package:base_code_project/presentation/screen/detail_staff_and_map/detail_staff_and_map.dart';
import 'package:base_code_project/presentation/screen/forgot_password/sc_forgot_password.dart';
import 'package:base_code_project/presentation/screen/forgot_password_reset/sc_forgot_password_reset.dart';
import 'package:base_code_project/presentation/screen/forgot_password_verify/sc_forgot_password_verify.dart';
import 'package:base_code_project/presentation/screen/login/login.dart';
import 'package:base_code_project/presentation/screen/login_cc/login_cc.dart';
import 'package:base_code_project/presentation/screen/menu/profile_detail/sc_profile_detail.dart';
import 'package:base_code_project/presentation/screen/menu/profile_detail/sc_profile_detail_two.dart';
import 'package:base_code_project/presentation/screen/navigation/sc_navigation.dart';
import 'package:base_code_project/presentation/screen/register/sc_register.dart';
import 'package:base_code_project/presentation/screen/register_verify/sc_register_verify.dart';
import 'package:base_code_project/presentation/screen/splash/sc_splash.dart';
import 'package:base_code_project/presentation/screen/follow_timekeeping/follow_timekeeping.dart';
import 'package:base_code_project/presentation/screen/term/sc_term.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'screen/detail_staff_and_map/detail_staff_and_map.dart';

class BaseRouter {
  static const String SPLASH = '/splash';
  static const String LOGIN = '/login';
  static const String LOGINCC = '/logincc';
  static const String NAVIGATION = '/navigation';
  static const String FORGOT_PASSWORD = '/forgot_password';
  static const String FORGOT_PASSWORD_VERIFY = '/forgot_password_verify';
  static const String FORGOT_PASSWORD_RESET = '/forgot_password_reset';
  static const String REGISTER_VERIFY = '/register_verify';
  static const String REGISTER = '/register';
  static const String PROFILE_DETAIL = '/profile_detail';
  static const String CHANGE_INFO_VERIFY = '/change_info_verify';
  static const String LIST_PROFILE = '/list_profile';
  static const String CONTACT = '/contact';
  static const String TERM = '/term';
  static const String ADD_PROJECT = '/add_project';
  static const String FULL_PROJECT = '/full_project';
  static const String TIME_KEEPING = '/time_keeping';
  static const String DETAIL_STAFF_AND_MAP = '/detail_staff_and_map';

  // static const String ADD_FULL_PROJECT = '/add_full_project';

  static Route<dynamic> generateRoute(RouteSettings settings) {
    switch (settings.name) {
      case SPLASH:
        return MaterialPageRoute(builder: (_) => SplashScreen());
      // case LOGIN:
      //   return MaterialPageRoute(builder: (_) => LoginScreen());
      case NAVIGATION:
        return MaterialPageRoute(builder: (_) => NavigationScreen());
      case PROFILE_DETAIL:
        return MaterialPageRoute(builder: (_) => ProfileDetailScreen());
      case CHANGE_INFO_VERIFY:
        return MaterialPageRoute(builder: (_) => ChangeInfoVerifyScreen());
      case CONTACT:
        return MaterialPageRoute(builder: (_) => ContactScreen());
      default:
        return MaterialPageRoute(
            builder: (_) => Scaffold(
                  body: Center(
                    child: Text('No route defined for ${settings.name}'),
                  ),
                ));
    }
  }

  static Map<String, WidgetBuilder> routes(BuildContext context) {
    var homeRepository = RepositoryProvider.of<HomeRepository>(context);
    var userRepository = RepositoryProvider.of<UserRepository>(context);
    var notificationRepository =
        RepositoryProvider.of<NotificationRepository>(context);
    var addressRepository = RepositoryProvider.of<AddressRepository>(context);
    var cartRepository = RepositoryProvider.of<CartRepository>(context);
    var paymentRepository = RepositoryProvider.of<PaymentRepository>(context);
    var invoiceRepository = RepositoryProvider.of<InvoiceRepository>(context);
    return {
      SPLASH: (context) => SplashScreen(),
      LOGIN: (context) => LoginScreen(),
      LOGINCC: (context) => LoginCCScreen(),
      NAVIGATION: (context) => NavigationScreen(),
      FORGOT_PASSWORD: (context) => ForgotPasswordScreen(),
      FORGOT_PASSWORD_VERIFY: (context) => ForgotPasswordVerifyScreen(),
      FORGOT_PASSWORD_RESET: (context) => ForgotPasswordResetScreen(),
      REGISTER_VERIFY: (context) => RegisterVerifyScreen(),
      REGISTER: (context) => RegisterScreen(),
      CHANGE_INFO_VERIFY: (context) => ChangeInfoVerifyScreen(),
      PROFILE_DETAIL: (context) => ProfileDetailTwoPageScreen(),
      LIST_PROFILE: (context) => ListProject(),
      CONTACT: (context) => ContactScreen(),
      TERM: (context) => TermScreen(),
      ADD_PROJECT: (context) => AddProjectScreen(),
      TIME_KEEPING: (context) => FollowTimekeepingScreen(),
      DETAIL_STAFF_AND_MAP: (context) => DetailStaffAndMap(),
      // FULL_PROJECT: (context) => FullProject(),

      // ADD_FULL_PROJECT: (context) => ListProfile(),




    };
  }
}
