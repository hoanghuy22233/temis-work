import 'package:base_code_project/model/date_not_api/data_not_api_seach_date_work.dart';
import 'package:flutter/material.dart';

class WidgetSeachDateWork extends StatefulWidget {
  @override
  _WidgetSeachDateWorkState createState() => _WidgetSeachDateWorkState();
}

class _WidgetSeachDateWorkState extends State<WidgetSeachDateWork> {
  int tappedIndex;
  @override
  void initState() {
    super.initState();
    tappedIndex = 0;
  }
  @override
  Widget build(BuildContext context) {
    return Container(
      // height: MediaQuery.of(context).size.height / 2,
      child: ListView.builder(
        itemCount: dataSeachDateWork.length,
        itemBuilder: (context, index) {
          return Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              // GestureDetector(
              // onTap: () {
              //   if (dataSeachDateWork[index].id == 0) {
              //     Text(
              //       "${dataSeachDateWork[index].textForm}",
              //       style: TextStyle(color: Colors.blue),
              //     );
              //   }
              // }),
              GestureDetector(
                // onTap: () {
                //   if (dataSeachDateWork[index].id == 0) {
                //     Navigator.push(
                //         context,
                //         MaterialPageRoute(builder: (context) => HrmPageScreen()));
                //     // Text(
                //     //   "${dataSeachDateWork[index].textForm}",
                //     //   style: TextStyle(color: Colors.blue),
                //     // );
                //   }
                // },
                onTap: () {
                  setState(() {
                    tappedIndex = index;
                  });
                },
                child: Padding(
                  padding:
                      const EdgeInsets.symmetric(horizontal: 10, vertical: 15),
                  child: Text("${dataSeachDateWork[index].textForm}",style: TextStyle(fontWeight: FontWeight.bold,color: tappedIndex == index ? Colors.blue : Colors.black,),),
                ),
              )
            ],
          );
        },
      ),
    );
  }
}
