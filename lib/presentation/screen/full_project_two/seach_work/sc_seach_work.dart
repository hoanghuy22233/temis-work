import 'package:base_code_project/presentation/screen/full_project_two/seach_work/list_seach_work.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';


class SeachWorkPageScreen extends StatefulWidget {
  SeachWorkPageScreen({Key key, this.title}) : super(key: key);
  final String title;

  @override
  SeachWorkPageScreenState createState() => SeachWorkPageScreenState();
}

class SeachWorkPageScreenState extends State<SeachWorkPageScreen> {
  @override
  Widget build(BuildContext context) {
    return Container(
      width: MediaQuery.of(context).size.width,
      height: MediaQuery.of(context).size.height/2.8 ,
      child: (
          Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Expanded(
            child: Padding(
              padding:
              const EdgeInsets.symmetric(horizontal: 15, vertical: 5),
              child:  WidgetSeachWork(),
            ),
          ),
        ],
      )),
    );
  }
}
