import 'package:base_code_project/model/date_not_api/data_not_api_seach_work.dart';
import 'package:flutter/material.dart';

class WidgetSeachWork extends StatefulWidget {
  @override
  _WidgetSeachWorkState createState() => _WidgetSeachWorkState();
}

class _WidgetSeachWorkState extends State<WidgetSeachWork> {
  int tappedIndex;
  @override
  void initState() {
    super.initState();
    tappedIndex = 0;
  }
  @override
  Widget build(BuildContext context) {
    return Container(
      // height: MediaQuery.of(context).size.height / 2,
      child: ListView.builder(
        itemCount: dataSeachWork.length,
        itemBuilder: (context, index) {
          return Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              // GestureDetector(
              // onTap: () {
              //   if (dataSeachWork[index].id == 0) {
              //     Text(
              //       "${dataSeachWork[index].textForm}",
              //       style: TextStyle(color: Colors.blue),
              //     );
              //   }
              // }),
              GestureDetector(
                // onTap: () {
                //   if (dataSeachWork[index].id == 0) {
                //     Navigator.push(
                //         context,
                //         MaterialPageRoute(builder: (context) => HrmPageScreen()));
                //     // Text(
                //     //   "${dataSeachWork[index].textForm}",
                //     //   style: TextStyle(color: Colors.blue),
                //     // );
                //   }
                // },
                onTap: () {
                  setState(() {
                    tappedIndex = index;
                  });
                },
                child: Padding(
                  padding:
                      const EdgeInsets.symmetric(horizontal: 10, vertical: 15),
                  child: Text("${dataSeachWork[index].textForm}",style: TextStyle(fontWeight: FontWeight.bold,color: tappedIndex == index ? Colors.blue : Colors.black,),),
                ),
              )
            ],
          );
        },
      ),
    );
  }
}
