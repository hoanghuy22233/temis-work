import 'package:base_code_project/model/date_not_api/data_not_api_project_full.dart';
import 'package:base_code_project/presentation/screen/full_project_two/list_project_full_two.dart';
import 'package:flutter/material.dart';

class WidgetFullProjectTwo extends StatefulWidget {
  const WidgetFullProjectTwo({
    Key key,
  }) : super(key: key);

  @override
  _WidgetFullProjectTwoState createState() => _WidgetFullProjectTwoState();
}

class _WidgetFullProjectTwoState extends State<WidgetFullProjectTwo>
    with SingleTickerProviderStateMixin {
  final controller = PageController();
  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        width: MediaQuery.of(context).size.width,
        height: MediaQuery.of(context).size.height,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Container(
              width: MediaQuery.of(context).size.width,
              height: MediaQuery.of(context).size.height / 15,
              margin: EdgeInsets.symmetric(horizontal: 17),
              child: Row(
                children: [
                  // Expanded(flex: 1,
                  //   child: Container(
                  //     width: MediaQuery.of(context).size.width,
                  //     height: MediaQuery.of(context).size.height / 14,
                  //     decoration: BoxDecoration
                  //       (
                  //       border: Border(
                  //         top: BorderSide( //                   <--- left side
                  //           color: Colors.grey[400],
                  //           width: 2.0,
                  //         ),
                  //         bottom: BorderSide( //                    <--- top side
                  //           color: Colors.grey[400],
                  //           width: 2.0,
                  //         ),
                  //       ),
                  //     ),
                  //     // margin: EdgeInsets.symmetric(horizontal: 17),
                  //     child: Row(
                  //       children: [
                  //         SizedBox(width: 15,),
                  //         Image.asset(
                  //           "assets/images/img_search.png",
                  //           width: 15,
                  //           height: 15,
                  //         ),
                  //         SizedBox(
                  //           width: 20,
                  //         ),
                  //         Expanded(
                  //           flex: 8,
                  //           child: TextField(
                  //             decoration: new InputDecoration.collapsed(
                  //               // border: InputBorder.none,
                  //                 hintText: 'Tìm kiếm'),
                  //           ),
                  //         )
                  //       ],
                  //     ),
                  //   ),
                  // ),
                  Image.asset(
                    "assets/images/img_search.png",
                    width: 15,
                    height: 15,
                  ),
                  SizedBox(
                    width: 20,
                  ),
                  Expanded(
                    flex: 8,
                    child: TextField(
                      decoration: new InputDecoration.collapsed(
                          // border: InputBorder.none,
                          hintText: 'Tìm kiếm'),
                    ),
                  )
                ],
              ),
            ),
            Container(
              width: MediaQuery.of(context).size.width,
              height: 3,
              // color: Colors.grey[300],
              decoration: BoxDecoration(
                border: Border(
                  bottom: BorderSide(width: 2.0, color: Colors.grey[400]),
                ),
                boxShadow: [
                  BoxShadow(
                    color: Colors.grey.withOpacity(0.3),
                    spreadRadius: 1,
                    blurRadius: 7,
                    offset: Offset(0, 2), // changes position of shadow
                  ),
                ],
              ),
            ),
            Container(
              color: Colors.grey[350],
              width: MediaQuery.of(context).size.width,
              height: MediaQuery.of(context).size.height / 22,
              child: Row(
                children: [
                  Container(
                      margin: EdgeInsets.symmetric(horizontal: 17),
                      child: Text(
                        "Hạn hoàn thành thứ 5, 04/03/2021",
                        style: TextStyle(fontWeight: FontWeight.bold),
                      )),
                ],
              ),
            ),
            Container(
              width: MediaQuery.of(context).size.width,
              height: 1,
              color: Colors.grey[400],
            ),
            Container(
              // height: MediaQuery.of(context).size.height / 2,
              child: Expanded(
                child: Stack(
                  children: [
                    ListView.builder(
                      scrollDirection: Axis.vertical,
                      // physics:  AlwaysScrollableScrollPhysics(),
                      itemCount: dataProjectFull.length,
                      itemBuilder: (context, index) {
                        return Container(
                          width: MediaQuery.of(context).size.width,
                          height: MediaQuery.of(context).size.height / 6,
                          // color: Colors.blue,
                          // margin: EdgeInsets.symmetric(horizontal: 10,vertical: 2),
                          child: ChildListProjectFullTwo(
                            id: index,
                          ),
                        );
                      },
                    ),
                    Positioned(
                        right: 10,
                        top: 250,
                        child: Image.asset(
                          "assets/images/image_add.png",
                          width: 30,
                          height: 30,
                        )),
                  ],
                ),
              ),
            ),

          ],
        ),
      ),
    );

  }
}
