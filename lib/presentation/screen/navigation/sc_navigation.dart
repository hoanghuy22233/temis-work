import 'package:base_code_project/presentation/common_widgets/widget_fab_bottom_nav.dart';
import 'package:base_code_project/presentation/screen/TimeKeepingTwo/sc_time_keeping_two.dart';
import 'package:base_code_project/presentation/screen/full_project_two/sc_full_project_two.dart';
import 'package:base_code_project/presentation/screen/menu/home/sc_home.dart';
import 'package:base_code_project/presentation/screen/menu/profile/sc_profile.dart';
import 'package:base_code_project/presentation/screen/menu/profile/sc_profile_two.dart';
import 'package:flutter/material.dart';

class TabNavigatorRoutes {
//  static const String root = '/';
  static const String home = '/home';
  static const String store = '/store';
  static const String information = '/info';
  static const String news = '/news';
  static const String account = '/account';
  static const String product = '/product';
  static const String notification = '/notification';
}

class NavigationScreen extends StatefulWidget {
  NavigationScreen();

  @override
  _NavigationScreenState createState() => _NavigationScreenState();
}

class _NavigationScreenState extends State<NavigationScreen> {
  List<FABBottomNavItem> _navMenus = List();
  PageController _pageController;
  int _selectedIndex = 0;
  GlobalKey<ScaffoldState> _drawerKey = GlobalKey();
  bool appbar = true;

  // CAN CO DU LIEU ;;

  // _NavigationScreenState({this.type});

  @override
  void initState() {
    _pageController =
    new PageController(initialPage: _selectedIndex, keepPage: true);
    super.initState();
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    _navMenus = _getTab();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        key: _drawerKey,
        // appBar: appbar
        //     ? AppBar(
        //         backgroundColor: Colors.blue,
        //         actions: <IconButton>[
        //           IconButton(
        //             icon: Icon(Icons.search),
        //             onPressed: () {
        //               // AppNavigator.navigateSearch();
        //             },
        //           ),
        //         ],
        //       )
        //     : null,
        // _selectedIndex==1?Container():Container(),
        bottomNavigationBar:WidgetFABBottomNav(
          backgroundColor: Color(0xFF3490dc),
          selectedIndex: _selectedIndex,
          onTabSelected: (index) async {
            goToPage(page: index);
          },
          items: _navMenus,
          selectedColor: Colors.black,
          color: Colors.white,
        ),

        // drawer: Container(
        //   width: MediaQuery.of(context).size.width/2.5,
        //   child: AppDrawer(
        //     drawer: _drawerKey,
        //     // menu: goToPage,
        //     // notification: goToPage,
        //     // account: goToPage,
        //   ),
        // ),
        body: PageView(
          controller: _pageController,
          physics: NeverScrollableScrollPhysics(),
          onPageChanged: (newPage) {
            setState(() {
              this._selectedIndex = newPage;
            });
          },
          children: [
            TimeKeepingTwoScreen(
            ),
            // BannerPages(
            //   drawer: _drawerKey,
            // ),
            FullProjectTwoScreen(),
            HomeFourPageScreen(),
            ProfileTwoPageScreen(),
          ],
        ));
  }

  List<FABBottomNavItem> _getTab() {
    return List.from([
      // FABBottomNavItem.asset(
      //   route: TabNavigatorRoutes.product,
      //   tabItem: TabItem.product,
      //   navigatorKey: GlobalKey<NavigatorState>(),
      //   assetUri: 'assets/icons/nav_bag.png',
      //   // text: "Home"
      // ),
      // FABBottomNavItem.asset(
      //   route: TabNavigatorRoutes.store,
      //   tabItem: TabItem.store,
      //   navigatorKey: GlobalKey<NavigatorState>(),
      //   assetUri: 'assets/icons/nav_news.png',
      //   // text: "Store"
      // ),
      FABBottomNavItem.asset(
        route: TabNavigatorRoutes.information,
        tabItem: TabItem.info,
        navigatorKey: GlobalKey<NavigatorState>(),
        assetUri: 'assets/images/note2.png',
        text: "Chấm Công"
      ),

      FABBottomNavItem.asset(
        route: TabNavigatorRoutes.account,
        tabItem: TabItem.news,
        navigatorKey: GlobalKey<NavigatorState>(),
        assetUri: 'assets/images/analytics.png',
        text: "Công Việc"
      ),
      FABBottomNavItem.asset(
          route: TabNavigatorRoutes.home,
          tabItem: TabItem.home,
          navigatorKey: GlobalKey<NavigatorState>(),
          assetUri: 'assets/images/img_payment_method_bank.png',
          text: "Trang chủ"
      ),
      FABBottomNavItem.asset(
        route: TabNavigatorRoutes.news,
        tabItem: TabItem.news,
        navigatorKey: GlobalKey<NavigatorState>(),
        assetUri: 'assets/icons/nav_user.png',
        text: "Tài Khoản"
      ),
    ]);
  }

  void goToPage({int page, int id = 0}) {
    if (page != _selectedIndex) {
      setState(() {
        this._selectedIndex = page;
      });
      _pageController.jumpToPage(_selectedIndex);
    }
    setState(() {
      this._selectedIndex = page;
    });
    _pageController.jumpToPage(_selectedIndex);
  }
  // goToPage({int page, int id = 0}) {
  //   WithAuth.isAuth(ifNotAuth: () {
  //     if (page == 0 ||page == 1 || page == 2) {
  //         AppNavigator.navigateLogin();
  //     } else {
  //       if (page != _selectedIndex) {
  //         setState(() {
  //           this._selectedIndex = page;
  //         });
  //         _pageController.jumpToPage(_selectedIndex);
  //       }
  //     }
  //   }, ifAuth: () {
  //     if (page != _selectedIndex) {
  //       setState(() {
  //         this._selectedIndex = page;
  //       });
  //       _pageController.jumpToPage(_selectedIndex);
  //     }
  //   });
  // }


}
