import 'package:equatable/equatable.dart';

class RankEvent extends Equatable {
  const RankEvent();

  List<Object> get props => [];
}

class LoadRank extends RankEvent {
}

class RefreshRank extends RankEvent {}
