
import 'package:base_code_project/model/repo/barrel_repo.dart';
import 'package:base_code_project/presentation/screen/ProjectList/bloc/rank_event.dart';
import 'package:base_code_project/presentation/screen/ProjectList/bloc/rank_state.dart';

import 'package:base_code_project/utils/utils.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:meta/meta.dart';

class RankBloc extends Bloc<RankEvent, RankState> {
  final UserRepository userRepository;

  RankBloc({@required this.userRepository});

  @override
  RankState get initialState => RankLoading();

  @override
  Stream<RankState> mapEventToState(RankEvent event) async* {
    if (event is LoadRank) {
      yield* _mapLoadRankToState();
    } else if (event is RefreshRank) {
      yield RankLoading();
      yield* _mapLoadRankToState();
    }
  }

  Stream<RankState> _mapLoadRankToState() async* {
    try {
      final response = await userRepository.getRank();
      yield RankLoaded(response.data);
    } catch (e) {
      yield RankNotLoaded(DioErrorUtil.handleError(e));
    }
  }
}
