import 'package:base_code_project/presentation/common_widgets/barrel_common_widgets.dart';
import 'package:base_code_project/presentation/common_widgets/widget_appbar_home_bell.dart';
import 'package:base_code_project/presentation/common_widgets/widget_appbar_home_page.dart';
import 'package:base_code_project/presentation/common_widgets/widget_appbar_schedule_page.dart';
import 'package:base_code_project/utils/locale/app_localization.dart';
import 'package:flutter/material.dart';

class WidgetScheduleMeetingPageAppbar extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: WidgetAppbarSchedulePage(
        title: AppLocalizations.of(context).translate('schedule_meeting.appbar').toUpperCase(),
        left: [WidgetAppbarMenuBack()],
        // right: [WidgetAppbarHomeBell()],
      ),
    );
  }
}
