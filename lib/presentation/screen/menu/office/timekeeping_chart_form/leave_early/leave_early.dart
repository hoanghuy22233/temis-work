import 'package:base_code_project/model/date_not_api/data_not_api_leave_early.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:base_code_project/presentation/screen/menu/office/timekeeping_chart_form/leave_early/leave_early_details.dart';

class WidgetLeaveEarly extends StatelessWidget {
  final int id;


  WidgetLeaveEarly({Key key, this.id}) : super(key: key);

  @override
  Widget build(BuildContext context) {

    return GestureDetector(
      onTap: () => Navigator.push(
        context,
        MaterialPageRoute(builder: (context) => LeaveEarlyDetailPage()),
      ),
      child: Card(

          margin: EdgeInsets.all(4),
          elevation: 5,

          child: Container(
            padding: EdgeInsets.all(10),

            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Container(
                  height: 25,
                  width: 110,
                  alignment: Alignment.center,
                  decoration: BoxDecoration(
                    color: Colors.blue,
                  ),
                  child: Text("Về sớm",
                      style: TextStyle(
                          fontWeight: FontWeight.bold, color: Colors.white)),
                ),
                SizedBox(height: 15,),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,

                  children: [
                    Wrap(
                      crossAxisAlignment: WrapCrossAlignment.center,
                      direction: Axis.vertical,
                      spacing: 15,
                      children: [
                        Text("Số lần"),

                        Image.asset("assets/images/deadline.png", color: Colors.red, width: 50, height: 50,),
                        Text("${dataLeaveEarly[0].totalLeaveEarly} \nlần", style: TextStyle(fontSize: 16,fontWeight: FontWeight.bold),textAlign: TextAlign.center,)
                      ],
                    ),
                    Wrap(
                      crossAxisAlignment: WrapCrossAlignment.center,
                      direction: Axis.vertical,
                      spacing: 15,
                      children: [
                        Text("Số phút"),
                        Image.asset("assets/images/clock.png", color: Colors.red, width: 50, height: 50,),
                        Center(child: Text("${dataLeaveEarly[0].leaveEarlyMinutes} \nphút", style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold), textAlign: TextAlign.center,))
                      ],
                    ),
                  ],
                )


              ],
            ),
          )),
    );
  }
}