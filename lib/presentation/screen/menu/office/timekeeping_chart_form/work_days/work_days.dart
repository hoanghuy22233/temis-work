import 'package:base_code_project/model/date_not_api/data_not_api_workdays.dart';
import 'package:base_code_project/presentation/screen/menu/office/timekeeping_chart_form/work_days/work_days_details.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:percent_indicator/percent_indicator.dart';

class WidgetWorkDays extends StatelessWidget {
  final int id;


  WidgetWorkDays({Key key, this.id}) : super(key: key);

  @override
  Widget build(BuildContext context) {

    return GestureDetector(
      onTap: () => Navigator.push(
        context,
        MaterialPageRoute(builder: (context) => WorkDaysDetailPage()),
      ),
      child: Card(

          margin: EdgeInsets.all(4),
          elevation: 5,

          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Container(
            height: 25,
            width: 110,
            alignment: Alignment.center,
            decoration: BoxDecoration(
              color: Colors.blue,
            ),
            child: Text("Ngày công",
                style: TextStyle(
                    fontWeight: FontWeight.bold, color: Colors.white)),
          ),

          CircularPercentIndicator(

            radius: 150.0,
            lineWidth: 13.0,
            animation: true,
            percent: (dataWorkDays[0].workDays/dataWorkDays[0].workMonth),
            center: new Text(
              "${dataWorkDays[0].workDays.toString()}",
              style:
              new TextStyle(fontWeight: FontWeight.bold, fontSize: 14.0),
            ),
            footer: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Text("Công tháng: ${dataWorkDays[0].workMonth.round().toString()} công", style: TextStyle(fontSize: 12),),
                Text("Số giờ: ${dataWorkDays[0].workHours.toString()} giờ",  style: TextStyle(fontSize: 12),),
              ],
            ),
            circularStrokeCap: CircularStrokeCap.round,
            progressColor: Colors.blue,
          ),

        ],
      )),
    );
  }
}

