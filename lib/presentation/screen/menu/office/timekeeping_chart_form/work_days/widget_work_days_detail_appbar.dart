import 'package:base_code_project/presentation/common_widgets/widget_appbar_home_bell.dart';
import 'package:base_code_project/presentation/common_widgets/widget_appbar_menu_back_two.dart';
import 'package:base_code_project/presentation/common_widgets/widget_appbar_time_keeping_page.dart';
import 'package:base_code_project/utils/locale/app_localization.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
class WidgetWorkDaysDetailAppbar extends StatelessWidget {
  var time = DateFormat.M().format(DateTime.now());

  @override
  Widget build(BuildContext context) {
    return Container(
      child: WidgetAppbarTimeKeepingPage(
        title: '${AppLocalizations.of(context).translate('work_days_detail.appbar').toUpperCase()} ${time}',
        // left: [WidgetAppbarMenuBackTwo()],
        right: [WidgetAppbarHomeBell()],
        left: [WidgetAppbarMenuBackTwo()],

      ),
    );
  }
}
