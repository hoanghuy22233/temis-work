
import 'package:base_code_project/presentation/screen/menu/office/timekeeping_chart_form/bussiness/bussiness.dart';
import 'package:base_code_project/presentation/screen/menu/office/timekeeping_chart_form/late/late.dart';
import 'package:base_code_project/presentation/screen/menu/office/timekeeping_chart_form/leave_early/leave_early.dart';
import 'package:base_code_project/presentation/screen/menu/office/timekeeping_chart_form/off_days/off_days.dart';
import 'package:base_code_project/presentation/screen/menu/office/timekeeping_chart_form/over_time/over_time.dart';
import 'package:base_code_project/presentation/screen/menu/office/timekeeping_chart_form/work_days/work_days.dart';
import 'package:base_code_project/presentation/screen/menu/office/timekeeping_chart_form/work_more/work_more.dart';
import 'package:flutter/material.dart';

class WidgetTimeKeepingChart extends StatefulWidget {
  const WidgetTimeKeepingChart({
    Key key,
  }) : super(key: key);

  @override
  _WidgetTimeKeepingChartState createState() => _WidgetTimeKeepingChartState();
}

class _WidgetTimeKeepingChartState extends State<WidgetTimeKeepingChart>
    with SingleTickerProviderStateMixin {
  final controller = PageController();
  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        children: [


          Expanded(

            child: GridView.count(

              primary: false,
              shrinkWrap: true,
              childAspectRatio: (2 /2.4),
              crossAxisSpacing: 2,
              mainAxisSpacing: 2,
              crossAxisCount: 2,
              children: [
                WidgetWorkDays(),

                WidgetOffDays(),
                WidgetLate(),
                WidgetLeaveEarly(),
                WidgetWorkMore(),
                WidgetBussiness(),
                WidgetOvertime(),

              ],
            ),
          ),
          SizedBox(height: 75,),

        ],


      ), // This trailing comma makes auto-formatting nicer for build methods.
    );
  }

}
