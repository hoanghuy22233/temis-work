
import 'package:base_code_project/presentation/screen/menu/office/timekeeping_chart_form/widget_time_keeping_chart.dart';
import 'package:base_code_project/presentation/screen/menu/office/timekeeping_chart_form/widget_time_keeping_chart_appbar.dart';
import 'package:flutter/material.dart';

class TimeKeepingChartScreen extends StatefulWidget {
  @override
  TimeKeepingChartState createState() => TimeKeepingChartState();
}

class TimeKeepingChartState extends State<TimeKeepingChartScreen> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        body: Column(
          children: [
            _buildAppbar(),

            Expanded(
              child: SingleChildScrollView(
                child: Container(
                  width: MediaQuery.of(context).size.width,
                  height: MediaQuery.of(context).size.height,
                  child: _buildCondition(),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  // Widget _buildContent() {
  //   return _buildCondition();
  // }

  Widget _buildAppbar() => WidgetTimeKeepingChartAppbar();

  Widget _buildCondition() => WidgetTimeKeepingChart();
}
