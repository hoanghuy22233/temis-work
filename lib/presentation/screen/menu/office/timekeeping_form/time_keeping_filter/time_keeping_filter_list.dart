
import 'package:base_code_project/model/date_not_api/data_not_api_filter_timekeeping.dart';
import 'package:flutter/material.dart';

class WidgetFilterTimeKeeping extends StatefulWidget {
  @override
  _WidgetFilterTimeKeepingState createState() => _WidgetFilterTimeKeepingState();
}
class _WidgetFilterTimeKeepingState extends State<WidgetFilterTimeKeeping> {
  int tappedIndex;
  @override
  void initState() {
    super.initState();
    tappedIndex = 0;
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: ListView.builder(
        itemCount: dataFillterTimeKeeping.length,
        itemBuilder: (context, index) {
          return Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              GestureDetector(
                onTap: () {
                  setState(() {
                    tappedIndex = index;
                  });
                },
                child: Padding(
                  padding:
                  const EdgeInsets.symmetric(horizontal: 10, vertical: 15),
                  child: Text(
                    "${dataFillterTimeKeeping[index].textForm}",
                    style: TextStyle(
                        color: tappedIndex == index ? Colors.blue : Colors.black,
                        fontWeight: FontWeight.bold
                    ),
                    // style: TextStyle(fontWeight: FontWeight.bold),
                  ),
                ),
              )
            ],
          );
        },
      ),
    );
  }
}
