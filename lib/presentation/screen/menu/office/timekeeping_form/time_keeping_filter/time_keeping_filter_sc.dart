import 'package:base_code_project/presentation/screen/menu/office/timekeeping_form/time_keeping_filter/time_keeping_filter_list.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class ListTimeKeepingFilterScreen extends StatefulWidget {
  ListTimeKeepingFilterScreen({Key key, this.title}) : super(key: key);
  final String title;

  @override
  _ListTimeKeepingFilterScreenState createState() => _ListTimeKeepingFilterScreenState();
}

class _ListTimeKeepingFilterScreenState extends State<ListTimeKeepingFilterScreen> {


  @override
  Widget build(BuildContext context) {
    return Container(
      width: MediaQuery.of(context).size.width,
      height: MediaQuery.of(context).size.height/1.5 ,
      child: (
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Expanded(
                child: Padding(
                  padding:
                  const EdgeInsets.symmetric(horizontal: 15, vertical: 5),
                  child:  WidgetFilterTimeKeeping(),
                ),
              ),
            ],
          )),
    );
  }
}
