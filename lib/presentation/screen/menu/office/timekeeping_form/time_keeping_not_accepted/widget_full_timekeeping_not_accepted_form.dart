import 'package:base_code_project/model/date_not_api/data_not_api_timekeeping_not_accepted_form.dart';
import 'package:base_code_project/presentation/screen/menu/monitoring/work_overview/seach_filter_work_overview/sc_seach_filter_work_overview.dart';
import 'package:base_code_project/presentation/screen/menu/office/timekeeping_form/time_keeping_filter/time_keeping_filter_sc.dart';
import 'package:flutter/material.dart';
import 'package:modal_bottom_sheet/modal_bottom_sheet.dart';

import 'list_timekeeping_not_accepted_form.dart';

class WidgetFullTimekeepingNotAccepted extends StatefulWidget {
  const WidgetFullTimekeepingNotAccepted({
    Key key,
  }) : super(key: key);

  @override
  _WidgetFullTimekeepingNotAcceptedState createState() => _WidgetFullTimekeepingNotAcceptedState();
}

class _WidgetFullTimekeepingNotAcceptedState extends State<WidgetFullTimekeepingNotAccepted>
    with SingleTickerProviderStateMixin {
  final controller = PageController();
  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Expanded(
            flex: 9,
            child: Stack(
              children: [
                ListView.builder(
                  scrollDirection: Axis.vertical,
                  // physics:  AlwaysScrollableScrollPhysics(),
                  itemCount: dataTimeKeepingNotAccepted.length,
                  itemBuilder: (context, index) {
                    return Container(
                      width: MediaQuery.of(context).size.width,
                      height: MediaQuery.of(context).size.height / 3,
                      // color: Colors.blue,
                      // margin: EdgeInsets.symmetric(horizontal: 10,vertical: 2),
                      child: ChildListTimekeepingNotAcceptedForm(
                        id: index,
                      ),
                    );
                  },
                ),
                Positioned(
                    right: 10,
                    top: 250,
                    child: Image.asset(
                      "assets/images/image_add.png",
                      width: 30,
                      height: 30,
                    )),
              ],
            ),
          ),
          Expanded(
            flex: 1,
            child: Column(
              children: [
                Container(
                  width: MediaQuery.of(context).size.width,
                  height: 1,
                  color: Colors.grey[400],
                ),
                SizedBox(height: 14,),
                Row(
                  children: [
                    Expanded(
                      flex: 2,
                      // child: Container(),
                      child: Padding(
                        padding: const EdgeInsets.only(bottom: 5, left: 20),
                        child: GestureDetector(
                          onTap: () {
                            showMaterialModalBottomSheet(
                              shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.only(
                                    topLeft: Radius.circular(15.0),
                                    topRight: Radius.circular(15.0)),
                              ),
                              backgroundColor: Colors.white,
                              context: context,
                              builder: (context) =>
                                  ListTimeKeepingFilterScreen(),
                            );
                          },
                          child: Image.asset(
                            "assets/images/ic_fillter.png",
                            width: 25,
                            height: 25,
                            color: Colors.blue,
                          ),
                        ),
                      ),
                    ),
                    Expanded(
                      flex: 8,
                      // child: Padding(
                      //     padding: const EdgeInsets.only(
                      //       bottom: 5,
                      //     ),
                      //     child: Center(
                      //         child: Text(
                      //           "Tất cả trạng thái",
                      //           style: TextStyle(
                      //               fontWeight: FontWeight.bold, fontSize: 14),
                      //         ))),

                      child: Padding(
                        padding: const EdgeInsets.only(bottom: 20, left: 10),
                        // child: Container(
                        // ),
                      ),
                    ),

                  ],
                ),
              ],
            ),
          ),

        ],
      ),
    );

  }
}
