import 'package:base_code_project/app/constants/color/color.dart';
import 'package:base_code_project/app/constants/style/style.dart';
import 'package:base_code_project/app/constants/value/value.dart';
import 'package:base_code_project/presentation/common_widgets/widget_appbar_detail.dart';
import 'package:base_code_project/presentation/common_widgets/widget_appbar_menu_back_two.dart';
import 'package:base_code_project/presentation/screen/menu/office/timekeeping_form/time_keeping_accepted/widget_full_timekeeping_accepted_form.dart';
import 'package:base_code_project/presentation/screen/menu/office/timekeeping_form/time_keeping_not_accepted/widget_full_timekeeping_not_accepted_form.dart';
import 'package:base_code_project/presentation/screen/menu/office/timekeeping_form/time_keeping_waiting/widget_full_timekeeping_waiting_form.dart';
import 'package:base_code_project/utils/locale/app_localization.dart';
import 'package:bubble_tab_indicator/bubble_tab_indicator.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';



class FullTimeKeepingScreen extends StatefulWidget {
  final int id;
  FullTimeKeepingScreen({Key key, this.id}) : super(key: key);

  @override
  _FullTimeKeepingScreenState createState() => _FullTimeKeepingScreenState();
}

class _FullTimeKeepingScreenState extends State<FullTimeKeepingScreen>
    with SingleTickerProviderStateMixin {
  final PageController _pageController = PageController(initialPage: 0);
  TabController _tabController;
  @override
  void initState() {
    super.initState();
    _tabController = new TabController(vsync: this, length: 3);
    _tabController.addListener(() {
      print('my index is' + _tabController.index.toString());
    });
  }

  @override
  void dispose() {
    _tabController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      top : true,
      child: Scaffold(
          body: Container(
        color: Colors.grey[100],
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Stack(
              children: [
                _buildAppbar(),
                _buildTabBarMenu(),
                //_buildTabBar()
              ],
            ),
            Expanded(
                child: Padding(
              padding: const EdgeInsets.all(AppValue.APP_HORIZONTAL_PADDING),
              child: TabBarView(
                controller: _tabController,
                children: [
                  WidgetFullTimekeepingAccepted(),
                  WidgetFullTimekeepingWaiting(
                  ),
                  // BannerPages(
                  //   drawer: _drawerKey,
                  // ),
                  WidgetFullTimekeepingNotAccepted(),
                ],
              ),
            )),
          ],
        ),
      )),
    );
  }

  _buildAppbar() => WidgetAppbarDetailPage(

    title: AppLocalizations.of(context).translate('full_time_keeping_form.appbar').toUpperCase(),
    left: [WidgetAppbarMenuBackTwo()],



  );

  Widget _buildTabBarMenu() {
    return new Container(
      width: MediaQuery.of(context).size.width,
      margin: EdgeInsets.only(top: 50, left: 0, right: 0),
      color: Colors.white,
      height: AppValue.ACTION_BAR_HEIGHT,
      child: new TabBar(
        controller: _tabController,
        labelPadding: EdgeInsets.symmetric(
          horizontal: 18,
        ),
        tabs: [

          Container(
            constraints: BoxConstraints(
                minWidth: MediaQuery.of(context).size.width / 4.3),
            alignment: Alignment.center,
            width: 60,
            child: Text(
              'ĐÃ DUYỆT',
              textAlign: TextAlign.center,
            ),
          ),
          Container(
            constraints: BoxConstraints(
                minWidth: MediaQuery.of(context).size.width / 4.3),
            alignment: Alignment.center,
            width: 60,
            child: Text(
              'CHỜ DUYỆT',
              textAlign: TextAlign.center,
            ),
          ),
          Container(
            constraints: BoxConstraints(
                minWidth: MediaQuery.of(context).size.width / 3.8),
            alignment: Alignment.center,
            width: 60,
            child: Text(
              'KHÔNG DUYỆT',
              textAlign: TextAlign.center,
            ),
          ),
        ],
        labelStyle: AppStyle.DEFAULT_SMALL_BOLD,
        unselectedLabelStyle: AppStyle.DEFAULT_SMALL,
        labelColor: Colors.blue,
        unselectedLabelColor: AppColor.BLACK,
        physics: BouncingScrollPhysics(),
        indicatorWeight: 3,
        isScrollable: true,
        indicatorPadding: EdgeInsets.symmetric(horizontal: 20),
        indicatorColor: AppColor.GREY,
        indicator: new BubbleTabIndicator(
          indicatorHeight: AppValue.ACTION_BAR_HEIGHT - 10,
          indicatorColor: Colors.white,
          tabBarIndicatorSize: TabBarIndicatorSize.tab,
          indicatorRadius: 50,
        ),
      ),
    );
  }
}
