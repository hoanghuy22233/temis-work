import 'package:base_code_project/model/date_not_api/data_not_api_timekeeping_waiting_form.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import '../detail_timekeeping_screen/sc_time_keeping_waiting_detail.dart';

class ChildListTimekeepingWaitingForm extends StatelessWidget {
  final int id;
  ChildListTimekeepingWaitingForm({Key key, this.id}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
        onTap: () => Navigator.push(
          context,
          MaterialPageRoute(builder: (context) => TimeKeepingWaitingDetailScreen(id: id)),
        ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisAlignment: MainAxisAlignment.start,
        children: [
          Container(
            padding: EdgeInsets.only(left: 15,),
            height: 25,
            width: MediaQuery.of(context).size.width,
            color: Colors.grey[300],
            child: Row(
              children: [
                Text("${dataTimeKeepingWaiting[id].name}",
                  style: TextStyle(fontWeight: FontWeight.bold),
                ),
              ],
            ),
          ),
          Container(
            padding: EdgeInsets.symmetric(horizontal:15),

            child: Column(
              children: [
                Row(
                  children: [
                    Image.asset("assets/images/clock.png", height: 15, width: 15,),
                    SizedBox(width: 5,),
                    Text("${dataTimeKeepingWaiting[id].timeBegin}"),
                    Text(" - "),
                    Text("${dataTimeKeepingWaiting[id].timeEnd}")

                  ],
                ),
                Row(
                  children: [
                    Image.asset("assets/images/flag (1).png",height: 15, width: 15, color: Colors.blue,),
                    SizedBox(width: 5,),
                    Text("${dataTimeKeepingWaiting[id].shift} - ${dataTimeKeepingWaiting[id].date}"),

                  ],
                ),
                Row(
                  children: [
                    Image.asset("assets/images/question.png",height: 15, width: 15, color: Colors.green,),
                    SizedBox(width: 5,),
                    Text("${dataTimeKeepingWaiting[id].reason}"),

                  ],
                ),
                Row(
                  children: [
                    Image.asset("assets/images/danger-sign.png", height: 15, width: 15, color: Colors.red,),
                    SizedBox(width:5,),
                    Text("${dataTimeKeepingWaiting[id].description}"),
                  ],
                ),
                Row(
                  children: [
                    Image.asset("assets/images/ic_checked.png", height: 15, width: 15),
                    SizedBox(width:5,),
                    Text("Chờ duyệt".toUpperCase(), style: TextStyle(color: Colors.deepOrange),)
                  ],
                ),
                Row(
                  children: [
                    Image.asset("assets/images/user.png", height: 15, width: 15 , color: Colors.blue,),
                    SizedBox(width:5,),
                    Text("Người duyệt: ${dataTimeKeepingWaiting[id].approvedBy}"),

                  ],
                ),
                Row(
                  children: [
                    Image.asset("assets/images/user.png", height: 15, width: 15 , color: Colors.blue,),
                    SizedBox(width:5,),


                    Text("Người theo dõi : ${dataTimeKeepingWaiting[id].supervisor}"),

                  ],
                ),
                Row(children: [
                  Text("(${dataTimeKeepingWaiting[id].supervisorEmail})")
                ],)

              ],
            ),
          ),

        ],
      )


    );
  }
}
