import 'package:base_code_project/model/date_not_api/data_not_api_timekeeping_waiting_form.dart';
import 'package:base_code_project/presentation/screen/menu/office/timekeeping_form/time_keeping_filter/time_keeping_filter_sc.dart';
import 'package:flutter/material.dart';
import 'package:modal_bottom_sheet/modal_bottom_sheet.dart';

import 'list_timekeeping_waiting_form.dart';

class WidgetFullTimekeepingWaiting extends StatefulWidget {
  const WidgetFullTimekeepingWaiting({
    Key key,
  }) : super(key: key);

  @override
  _WidgetFullTimekeepingWaitingState createState() => _WidgetFullTimekeepingWaitingState();
}

class _WidgetFullTimekeepingWaitingState extends State<WidgetFullTimekeepingWaiting>
    with SingleTickerProviderStateMixin {
  final controller = PageController();
  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Expanded(
            flex: 9,
            child: Stack(
              children: [
                ListView.builder(
                  scrollDirection: Axis.vertical,
                  // physics:  AlwaysScrollableScrollPhysics(),
                  itemCount: dataTimeKeepingWaiting.length,
                  itemBuilder: (context, index) {
                    return Container(
                      width: MediaQuery.of(context).size.width,
                      height: MediaQuery.of(context).size.height / 3,
                      // color: Colors.blue,
                      // margin: EdgeInsets.symmetric(horizontal: 10,vertical: 2),
                      child: ChildListTimekeepingWaitingForm(
                        id: index,
                      ),
                    );
                  },
                ),
                Positioned(
                    right: 10,
                    top: 250,
                    child: Image.asset(
                      "assets/images/image_add.png",
                      width: 30,
                      height: 30,
                    )),
              ],
            ),
          ),
          Expanded(
            flex: 1,
            child: Column(
              children: [
                Container(
                  width: MediaQuery.of(context).size.width,
                  height: 1,
                  color: Colors.grey[400],
                ),
                SizedBox(height: 14,),
                Row(
                  children: [
                    Expanded(
                      flex: 2,
                      // child: Container(),
                      child: Padding(
                        padding: const EdgeInsets.only(bottom: 5, left: 20),
                        child: GestureDetector(
                          onTap: () {
                            showMaterialModalBottomSheet(
                              shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.only(
                                    topLeft: Radius.circular(15.0),
                                    topRight: Radius.circular(15.0)),
                              ),
                              backgroundColor: Colors.white,
                              context: context,
                              builder: (context) =>
                                  ListTimeKeepingFilterScreen(),
                            );
                          },
                          child: Image.asset(
                            "assets/images/ic_fillter.png",
                            width: 25,
                            height: 25,
                            color: Colors.blue,
                          ),
                        ),
                      ),
                    ),
                    Expanded(
                      flex: 8,
                      // child: Padding(
                      //     padding: const EdgeInsets.only(
                      //       bottom: 5,
                      //     ),
                      //     child: Center(
                      //         child: Text(
                      //           "Tất cả trạng thái",
                      //           style: TextStyle(
                      //               fontWeight: FontWeight.bold, fontSize: 14),
                      //         ))),

                      child: Padding(
                        padding: const EdgeInsets.only(bottom: 20, left: 10),
                        // child: Container(
                        // ),
                      ),
                    ),

                  ],
                ),
              ],
            ),
          ),

        ],
      ),
    );
    // return ListView(
    //   // width: double.infinity,
    //   // padding: EdgeInsets.symmetric(vertical: 10, horizontal: 20),
    //   children: [
    //     Container(
    //       margin: EdgeInsets.all(8),
    //       child: Column(
    //         mainAxisAlignment: MainAxisAlignment.start,
    //         crossAxisAlignment: CrossAxisAlignment.start,
    //         children: [
    //           Row(
    //             children: [
    //               Expanded(flex: 5, child: Text("Dự án số 1 ")),
    //               Expanded(
    //                   flex: 2,
    //                   child: GestureDetector(
    //                     child: Row(
    //                       children: [
    //                         Text("100: "),
    //                         Image.asset(
    //                           "assets/images/ic_person.png",
    //                           width: 20,
    //                           height: 20,
    //                         ),
    //                       ],
    //                     ),
    //                   )),
    //               Expanded(
    //                   flex: 3,
    //                   child: GestureDetector(
    //                     child: Row(
    //                       children: [
    //                         Text("Tham gia: "),
    //                         Image.asset(
    //                           "assets/images/add_project.png",
    //                           width: 20,
    //                           height: 20,
    //                         ),
    //                         // Text("Tham gia")
    //                       ],
    //                     ),
    //                   )),
    //             ],
    //           ),
    //           SizedBox(
    //             height: 10,
    //           ),
    //           Row(
    //             children: [
    //               Expanded(flex: 5, child: Text("Dự án số 2 ")),
    //               Expanded(
    //                   flex: 2,
    //                   child: GestureDetector(
    //                     child: Row(
    //                       children: [
    //                         Text("100: "),
    //                         Image.asset(
    //                           "assets/images/ic_person.png",
    //                           width: 20,
    //                           height: 20,
    //                         ),
    //                       ],
    //                     ),
    //                   )),
    //               Expanded(
    //                   flex: 3,
    //                   child: GestureDetector(
    //                     child: Row(
    //                       children: [
    //                         Text("Tham gia: "),
    //                         Image.asset(
    //                           "assets/images/add_project.png",
    //                           width: 20,
    //                           height: 20,
    //                         ),
    //                         // Text("Tham gia")
    //                       ],
    //                     ),
    //                   )),
    //             ],
    //           ),
    //           SizedBox(
    //             height: 10,
    //           ),
    //           Row(
    //             children: [
    //               Expanded(flex: 5, child: Text("Dự án số 3 ")),
    //               Expanded(
    //                   flex: 2,
    //                   child: GestureDetector(
    //                     child: Row(
    //                       children: [
    //                         Text("100: "),
    //                         Image.asset(
    //                           "assets/images/ic_person.png",
    //                           width: 20,
    //                           height: 20,
    //                         ),
    //                       ],
    //                     ),
    //                   )),
    //               Expanded(
    //                   flex: 3,
    //                   child: GestureDetector(
    //                     child: Row(
    //                       children: [
    //                         Text("Tham gia: "),
    //                         Image.asset(
    //                           "assets/images/add_project.png",
    //                           width: 20,
    //                           height: 20,
    //                         ),
    //                         // Text("Tham gia")
    //                       ],
    //                     ),
    //                   )),
    //             ],
    //           ),
    //           SizedBox(
    //             height: 10,
    //           ),
    //           Row(
    //             children: [
    //               Expanded(flex: 5, child: Text("Dự án số 4 ")),
    //               Expanded(
    //                   flex: 2,
    //                   child: GestureDetector(
    //                     child: Row(
    //                       children: [
    //                         Text("100: "),
    //                         Image.asset(
    //                           "assets/images/ic_person.png",
    //                           width: 20,
    //                           height: 20,
    //                         ),
    //                       ],
    //                     ),
    //                   )),
    //               Expanded(
    //                   flex: 3,
    //                   child: GestureDetector(
    //                     child: Row(
    //                       children: [
    //                         Text("Tham gia: "),
    //                         Image.asset(
    //                           "assets/images/add_project.png",
    //                           width: 20,
    //                           height: 20,
    //                         ),
    //                         // Text("Tham gia")
    //                       ],
    //                     ),
    //                   )),
    //             ],
    //           ),
    //           SizedBox(
    //             height: 10,
    //           ),
    //           Row(
    //             children: [
    //               Expanded(flex: 5, child: Text("Dự án số 5 ")),
    //               Expanded(
    //                   flex: 2,
    //                   child: GestureDetector(
    //                     child: Row(
    //                       children: [
    //                         Text("100: "),
    //                         Image.asset(
    //                           "assets/images/ic_person.png",
    //                           width: 20,
    //                           height: 20,
    //                         ),
    //                       ],
    //                     ),
    //                   )),
    //               Expanded(
    //                   flex: 3,
    //                   child: GestureDetector(
    //                     child: Row(
    //                       children: [
    //                         Text("Tham gia: "),
    //                         Image.asset(
    //                           "assets/images/add_project.png",
    //                           width: 20,
    //                           height: 20,
    //                         ),
    //                         // Text("Tham gia")
    //                       ],
    //                     ),
    //                   )),
    //             ],
    //           ),
    //           SizedBox(
    //             height: 10,
    //           ),
    //           Row(
    //             children: [
    //               Expanded(flex: 5, child: Text("Dự án số 6 ")),
    //               Expanded(
    //                   flex: 2,
    //                   child: GestureDetector(
    //                     child: Row(
    //                       children: [
    //                         Text("100: "),
    //                         Image.asset(
    //                           "assets/images/ic_person.png",
    //                           width: 20,
    //                           height: 20,
    //                         ),
    //                       ],
    //                     ),
    //                   )),
    //               Expanded(
    //                   flex: 3,
    //                   child: GestureDetector(
    //                     child: Row(
    //                       children: [
    //                         Text("Tham gia: "),
    //                         Image.asset(
    //                           "assets/images/add_project.png",
    //                           width: 20,
    //                           height: 20,
    //                         ),
    //                         // Text("Tham gia")
    //                       ],
    //                     ),
    //                   )),
    //             ],
    //           ),
    //           SizedBox(
    //             height: 10,
    //           ),
    //           Row(
    //             children: [
    //               Expanded(flex: 5, child: Text("Dự án số 7 ")),
    //               Expanded(
    //                   flex: 2,
    //                   child: GestureDetector(
    //                     child: Row(
    //                       children: [
    //                         Text("100: "),
    //                         Image.asset(
    //                           "assets/images/ic_person.png",
    //                           width: 20,
    //                           height: 20,
    //                         ),
    //                       ],
    //                     ),
    //                   )),
    //               Expanded(
    //                   flex: 3,
    //                   child: GestureDetector(
    //                     child: Row(
    //                       children: [
    //                         Text("Tham gia: "),
    //                         Image.asset(
    //                           "assets/images/add_project.png",
    //                           width: 20,
    //                           height: 20,
    //                         ),
    //                         // Text("Tham gia")
    //                       ],
    //                     ),
    //                   )),
    //             ],
    //           ),
    //           SizedBox(
    //             height: 10,
    //           ),
    //           Row(
    //             children: [
    //               Expanded(flex: 5, child: Text("Dự án số 8 ")),
    //               Expanded(
    //                   flex: 2,
    //                   child: GestureDetector(
    //                     child: Row(
    //                       children: [
    //                         Text("100: "),
    //                         Image.asset(
    //                           "assets/images/ic_person.png",
    //                           width: 20,
    //                           height: 20,
    //                         ),
    //                       ],
    //                     ),
    //                   )),
    //               Expanded(
    //                   flex: 3,
    //                   child: GestureDetector(
    //                     child: Row(
    //                       children: [
    //                         Text("Tham gia: "),
    //                         Image.asset(
    //                           "assets/images/add_project.png",
    //                           width: 20,
    //                           height: 20,
    //                         ),
    //                         // Text("Tham gia")
    //                       ],
    //                     ),
    //                   )),
    //             ],
    //           ),
    //           SizedBox(
    //             height: 10,
    //           ),
    //           Row(
    //             children: [
    //               Expanded(flex: 5, child: Text("Dự án số 9 ")),
    //               Expanded(
    //                   flex: 2,
    //                   child: GestureDetector(
    //                     child: Row(
    //                       children: [
    //                         Text("100: "),
    //                         Image.asset(
    //                           "assets/images/ic_person.png",
    //                           width: 20,
    //                           height: 20,
    //                         ),
    //                       ],
    //                     ),
    //                   )),
    //               Expanded(
    //                   flex: 3,
    //                   child: GestureDetector(
    //                     child: Row(
    //                       children: [
    //                         Text("Tham gia: "),
    //                         Image.asset(
    //                           "assets/images/add_project.png",
    //                           width: 20,
    //                           height: 20,
    //                         ),
    //                         // Text("Tham gia")
    //                       ],
    //                     ),
    //                   )),
    //             ],
    //           ),
    //           SizedBox(
    //             height: 10,
    //           ),
    //         ],
    //       ),
    //     )
    //   ],
    // );
  }
}
