import 'package:base_code_project/model/date_not_api/data_not_api_timekeeping_accepted_form.dart';
import 'package:drop_cap_text/drop_cap_text.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import 'detail_timekeeping_screen/sc_time_keeping_accepted_detail.dart';
class ChildListTimekeepingAcceptedForm extends StatelessWidget {
  final int id;
  ChildListTimekeepingAcceptedForm({Key key, this.id}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
         onTap: () => Navigator.push(
         context,
         MaterialPageRoute(builder: (context) => TimeKeepingDetailScreen(id: id)),
         ),
      child: Container(
        // height: MediaQuery.of(context).size.height/3,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.start,
          children: [
            Container(
              padding: EdgeInsets.only(left: 15,),
              height: 25,
              width: MediaQuery.of(context).size.width,
              color: Colors.grey[300],
              child: Row(
                children: [
                  Text("${dataTimeKeepingAccepted[id].name}",
                    style: TextStyle(fontWeight: FontWeight.bold),
                  ),
                ],
              ),
            ),
            Container(
              padding: EdgeInsets.symmetric(horizontal:15),

              child: Column(
                children: [
                  Row(
                    children: [
                      Image.asset("assets/images/clock.png", height: 15, width: 15,),
                      SizedBox(width: 5,),
                      Text("${dataTimeKeepingAccepted[id].timeBegin}"),
                      Text(" - "),
                      Text("${dataTimeKeepingAccepted[id].timeEnd}")

                    ],
                  ),
                  Row(
                    children: [
                      Image.asset("assets/images/flag (1).png",height: 15, width: 15, color: Colors.blue,),
                      SizedBox(width: 5,),
                      Text("${dataTimeKeepingAccepted[id].shift} - ${dataTimeKeepingAccepted[id].date}"),

                    ],
                  ),
                  Row(
                    children: [
                      Image.asset("assets/images/question.png",height: 15, width: 15, color: Colors.green,),
                      SizedBox(width: 5,),
                      Text("${dataTimeKeepingAccepted[id].reason}"),

                    ],
                  ),
                  Row(
                    children: [
                      Image.asset("assets/images/danger-sign.png", height: 15, width: 15, color: Colors.red,),
                      SizedBox(width:5,),
                      Text("${dataTimeKeepingAccepted[id].description}"),
                    ],
                  ),
                  Row(
                    children: [
                      Image.asset("assets/images/ic_checked.png", height: 15, width: 15),
                      SizedBox(width:5,),
                      Text("Đã duyệt".toUpperCase(), style: TextStyle(color: Colors.green),)
                    ],
                  ),
                  DropCapText(
                    "Người duyệt: ${dataTimeKeepingAccepted[id].approvedBy} - ${dataTimeKeepingAccepted[id].reasonAccepted}",
                    dropCap: DropCap(
                      width: 15, height: 15,
                      child: Image.asset("assets/images/user.png", height: 15, width: 15 , color: Colors.blue,),
                    ),
                  ),
                  Row(
                    children: [
                      Image.asset("assets/images/user.png", height: 15, width: 15 , color: Colors.blue,),
                      SizedBox(width:5,),


                      Text("Người theo dõi : ${dataTimeKeepingAccepted[id].supervisor}"),

                    ],
                  ),
                  Row(children: [
                    Text("(${dataTimeKeepingAccepted[id].supervisorEmail})")
                  ],)

                ],
              ),
            ),

          ],
        ),
      )


    );
  }
}
