import 'package:base_code_project/model/date_not_api/data_not_api_addpicture.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class ChildListAddPicture extends StatelessWidget {
  final int id;
  ChildListAddPicture({Key key, this.id}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                Expanded(
                    flex: 1,
                    child: Image.asset(
                      "${dataAddPicture[id].img}",
                      width: 20,
                      height: 20,
                    )),
                Expanded(
                    flex: 9,
                    child: Padding(
                      padding: const EdgeInsets.only(left: 30),
                      child: Text("${dataAddPicture[id].textForm}"),
                    ))
              ],
            ),
            SizedBox(
              height: 20,
            ),


          ],
        ),


    );
  }
}