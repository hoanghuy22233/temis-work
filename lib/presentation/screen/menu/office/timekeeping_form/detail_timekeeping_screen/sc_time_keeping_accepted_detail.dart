import 'package:base_code_project/model/date_not_api/data_not_api_timekeeping_accepted_form.dart';
import 'package:base_code_project/presentation/screen/menu/office/propose_form/detail_propose_screen/widget_propose_detail_appbar.dart';
import 'package:base_code_project/presentation/screen/menu/office/timekeeping_form/detail_timekeeping_screen/addpicture/sc_addpicture.dart';
import 'package:flutter/material.dart';
import 'package:modal_bottom_sheet/modal_bottom_sheet.dart';

class TimeKeepingDetailScreen extends StatefulWidget {
  final int id;
  TimeKeepingDetailScreen({Key key, this.id}) : super(key: key);

  TimeKeepingDetailScreenState createState() => TimeKeepingDetailScreenState();
}

class TimeKeepingDetailScreenState extends State<TimeKeepingDetailScreen> {
  bool isHD = false;
  bool isShow = true;
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        body: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            _buildAppbar(),
            Expanded(
              child: ListView(
                children: [
                  buildDetail(
                      "Người tạo:", dataTimeKeepingAccepted[widget.id].creator),
                  buildDetail(
                      "Loại đơn:", dataTimeKeepingAccepted[widget.id].name),
                  buildDetail("Ngày nộp đơn:",
                      "${dataTimeKeepingAccepted[widget.id].createdDate} ${dataTimeKeepingAccepted[widget.id].createdTime}"),
                  buildDetail("Thời gian:",
                      "${dataTimeKeepingAccepted[widget.id].date} ${dataTimeKeepingAccepted[widget.id].shift}"),
                  buildDetail("Ghi chú:",
                      "${dataTimeKeepingAccepted[widget.id].description}"),
                  Card(
                    child: Padding(
                      padding: const EdgeInsets.all(5.0),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text("Trạng thái phê duyệt:"),
                          Text(
                            "Đã Duyệt".toUpperCase(),
                            style: TextStyle(color: Colors.green),
                          ),
                        ],
                      ),
                    ),
                  ),
                  buildDetail("Người duyệt:",
                      "${dataTimeKeepingAccepted[widget.id].approvedBy}"),
                  buildDetail("Người theo dõi:",
                      "${dataTimeKeepingAccepted[widget.id].supervisor}"),
                  buildDetail("Người phê duyệt:",
                      "${dataTimeKeepingAccepted[widget.id].approvedBy} - ${dataTimeKeepingAccepted[widget.id].reasonAccepted}"),
                  buildDetail("Ngày phê duyệt:",
                      "${dataTimeKeepingAccepted[widget.id].approvedDate} ${dataTimeKeepingAccepted[widget.id].approvedTime}"),
                  Card(
                    child: Container(
                        height: 150,
                        padding: const EdgeInsets.all(5.0),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Row(children: [
                              Expanded(
                                flex: 1,
                                child: Image.asset(
                                  "assets/images/chatting.png",
                                  width: 40,
                                  height: 40,
                                  color: Colors.blue,
                                ),
                              ),
                              Expanded(flex: 9, child: Text(" Trao đổi")),
                            ]),
                            Row(
                              children: [
                                Expanded(
                                  flex: 8,
                                  child: TextField(
                                    keyboardType: TextInputType.multiline,
                                    maxLines: null,
                                    decoration: InputDecoration(
                                      border: OutlineInputBorder(),
                                      labelText: 'Nhập nội dung...',
                                    ),
                                  ),
                                ),
                                Expanded(
                                  flex: 1,
                                    child: IconButton(
                                        icon: Icon(Icons.camera_alt, color: Colors.blue,),
                                        onPressed: () {
                                          showMaterialModalBottomSheet(
                                              context: context,
                                              builder: (context) =>
                                                  AddPictureScreen());
                                        })),
                                Expanded(
                                    flex: 1,
                                    child: IconButton(
                                        icon: Image.asset("assets/images/direct.png", color:  Colors.blue)
                                        ))
                              ],
                            ),
                          ],
                        )),
                  ),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }

  // Widget _buildContent() {
  //   return _buildCondition();
  // }

  Widget _buildAppbar() => WidgetTimeKeepingDetailAppbar();

  buildDetail(String name, String detail) {
    return Container(
      width: MediaQuery.of(context).size.width,
      child: Card(
        child: Padding(
          padding: const EdgeInsets.all(5.0),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                name,
              ),
              Text(detail),
            ],
          ),
        ),
      ),
    );
  }
}
