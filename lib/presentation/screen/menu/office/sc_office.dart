import 'package:base_code_project/presentation/screen/menu/office/list_office.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class OfficePageScreen extends StatefulWidget {
  OfficePageScreen(
      {Key key, this.title, this.panelController, this.scrollController})
      : super(key: key);
  final String title;
  final panelController;
  final scrollController;

  @override
  OfficePageState createState() => OfficePageState();
}

class OfficePageState extends State<OfficePageScreen> {
  @override
  Widget build(BuildContext context) {
    return Container(
      width: MediaQuery.of(context).size.width,
      height: MediaQuery.of(context).size.height / 1.7,
      child: (Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Expanded(
              flex: 1,
              child: Container(
                padding: EdgeInsets.symmetric(horizontal: 23, vertical: 5),
                child: Text(
                  "OFFICE+",
                  style: TextStyle(
                      fontWeight: FontWeight.bold,
                      fontSize: 16,
                      color: Colors.grey),
                ),
              )),
          Expanded(
            flex: 9,
            child: Padding(
              padding: const EdgeInsets.symmetric(horizontal: 15),
              child: WidgetListOffice(),
            ),
          ),
        ],
      )),
    );
  }
}
