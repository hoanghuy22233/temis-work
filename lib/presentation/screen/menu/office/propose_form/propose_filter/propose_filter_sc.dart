import 'package:base_code_project/presentation/screen/menu/office/propose_form/propose_filter/propose_filter_list.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class ListProposeFilterScreen extends StatefulWidget {
  ListProposeFilterScreen({Key key, this.title}) : super(key: key);
  final String title;

  @override
  _ListProposeFilterScreenState createState() => _ListProposeFilterScreenState();
}

class _ListProposeFilterScreenState extends State<ListProposeFilterScreen> {


  @override
  Widget build(BuildContext context) {
    return Container(
      width: MediaQuery.of(context).size.width,
      height: MediaQuery.of(context).size.height/1.5 ,
      child: (
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Expanded(
                child: Padding(
                  padding:
                  const EdgeInsets.symmetric(horizontal: 15, vertical: 5),
                  child:  WidgetFilterPropose(),
                ),
              ),
            ],
          )),
    );
  }
}
