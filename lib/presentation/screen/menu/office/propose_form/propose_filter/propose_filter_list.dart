
import 'package:base_code_project/model/date_not_api/data_not_api_filter_propose.dart';
import 'package:flutter/material.dart';

class WidgetFilterPropose extends StatefulWidget {
  @override
  _WidgetFilterProposeState createState() => _WidgetFilterProposeState();
}
class _WidgetFilterProposeState extends State<WidgetFilterPropose> {
  int tappedIndex;
  @override
  void initState() {
    super.initState();
    tappedIndex = 0;
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: ListView.builder(
        itemCount: dataFillterPropose.length,
        itemBuilder: (context, index) {
          return Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              GestureDetector(
                onTap: () {
                  setState(() {
                    tappedIndex = index;
                  });
                },
                child: Padding(
                  padding:
                  const EdgeInsets.symmetric(horizontal: 10, vertical: 15),
                  child: Text(
                    "${dataFillterPropose[index].textForm}",
                    style: TextStyle(
                        color: tappedIndex == index ? Colors.blue : Colors.black,
                        fontWeight: FontWeight.bold
                    ),
                    // style: TextStyle(fontWeight: FontWeight.bold),
                  ),
                ),
              )
            ],
          );
        },
      ),
    );
  }
}
