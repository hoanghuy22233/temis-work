import 'package:base_code_project/model/date_not_api/data_not_api_propose_waiting_form.dart';
import 'package:flutter/material.dart';
import 'package:modal_bottom_sheet/modal_bottom_sheet.dart';
import 'list_propose_waiting_form.dart';
import 'package:base_code_project/presentation/screen/menu/office/propose_form/propose_filter/propose_filter_sc.dart';

class WidgetFullProposeWaiting extends StatefulWidget {
  const WidgetFullProposeWaiting({
    Key key,
  }) : super(key: key);

  @override
  _WidgetFullProposeWaitingState createState() => _WidgetFullProposeWaitingState();
}

class _WidgetFullProposeWaitingState extends State<WidgetFullProposeWaiting>
    with SingleTickerProviderStateMixin {
  final controller = PageController();
  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
            width: MediaQuery.of(context).size.width,
            height: MediaQuery.of(context).size.height / 15,
            margin: EdgeInsets.symmetric(horizontal: 17),
            child: Row(
              children: [
                Image.asset(
                  "assets/images/img_search.png",
                  width: 15,
                  height: 15,
                ),
                SizedBox(
                  width: 20,
                ),
                Expanded(
                  flex: 8,
                  child: TextField(
                    decoration: new InputDecoration.collapsed(
                      // border: InputBorder.none,
                        hintText: 'Tìm kiếm'),
                  ),

                )
              ],
            ),
          ),
          Container(
            width: MediaQuery.of(context).size.width,
            height: 3,
            // color: Colors.grey[300],
            decoration: BoxDecoration(
              border: Border(
                bottom: BorderSide(width: 2.0, color: Colors.grey[400]),
              ),
              boxShadow: [
                BoxShadow(
                  color: Colors.grey.withOpacity(0.3),
                  spreadRadius: 1,
                  blurRadius: 7,
                  offset: Offset(0, 2), // changes position of shadow
                ),
              ],
            ),
          ),
          Expanded(
            flex: 9,
            child: Stack(
              children: [
                ListView.builder(
                  scrollDirection: Axis.vertical,
                  // physics:  AlwaysScrollableScrollPhysics(),
                  itemCount: dataProposeWaiting.length,
                  itemBuilder: (context, index) {
                    return Container(
                      width: MediaQuery.of(context).size.width,
                      height: MediaQuery.of(context).size.height / 5,
                      // color: Colors.blue,
                      // margin: EdgeInsets.symmetric(horizontal: 10,vertical: 2),
                      child: ChildListProposeWaitingForm(
                        id: index,
                      ),
                    );
                  },
                ),
                Positioned(
                    right: 10,
                    top: 250,
                    child: Image.asset(
                      "assets/images/image_add.png",
                      width: 30,
                      height: 30,
                    )),
              ],
            ),
          ),
          Expanded(
            flex: 1,
            child: Column(
              children: [
                Container(
                  width: MediaQuery.of(context).size.width,
                  height: 1,
                  color: Colors.grey[400],
                ),
                SizedBox(height: 14,),
                Row(
                  children: [
                    Expanded(
                      flex: 2,
                      // child: Container(),
                      child: Padding(
                        padding: const EdgeInsets.only(bottom: 5, left: 20),
                        child: GestureDetector(
                          onTap: () {
                            showMaterialModalBottomSheet(
                              shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.only(
                                    topLeft: Radius.circular(15.0),
                                    topRight: Radius.circular(15.0)),
                              ),
                              backgroundColor: Colors.white,
                              context: context,
                              builder: (context) =>
                                  ListProposeFilterScreen(),
                            );
                          },
                          child: Image.asset(
                            "assets/images/ic_fillter.png",
                            width: 25,
                            height: 25,
                            color: Colors.blue,
                          ),
                        ),
                      ),
                    ),
                    Expanded(
                      flex: 8,
                      // child: Padding(
                      //     padding: const EdgeInsets.only(
                      //       bottom: 5,
                      //     ),
                      //     child: Center(
                      //         child: Text(
                      //           "Tất cả trạng thái",
                      //           style: TextStyle(
                      //               fontWeight: FontWeight.bold, fontSize: 14),
                      //         ))),

                      child: Padding(
                        padding: const EdgeInsets.only(bottom: 20, left: 10),
                        // child: Container(
                        // ),
                      ),
                    ),

                  ],
                ),
              ],
            ),
          ),

        ],
      ),
    );

  }
}
