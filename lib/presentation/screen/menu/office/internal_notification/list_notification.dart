import 'package:base_code_project/model/date_not_api/data_not_api_internal_notification.dart';
import 'package:base_code_project/presentation/screen/menu/office/internal_notification/notification_detail.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class ListNotificationPage extends StatelessWidget{
  final int id;
  String capitalize(String s) => s[0].toUpperCase() + s.substring(1);
  ListNotificationPage({Key key, this.id}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () => Navigator.push(
        context,
        MaterialPageRoute(builder: (context) => NotificationDetailPage(id: id)),
      ),
      child: Container(
        padding: EdgeInsets.all(5),

        width: MediaQuery.of(context).size.width,
        child: Column(
          children: [
            Row(
              children: [
                Text(capitalize(dataInternalNotification[id].title), style: TextStyle(color: Colors.blue, fontSize: 18),),
              ],
            ),
            Row(
              children: [
                Text(dataInternalNotification[id].content)
              ],
            ),
            Row(
              children: [
                Image.asset("assets/images/user.png",width: 20, height: 20,color: Colors.blue,),
                Text("Ban hành bởi ${dataInternalNotification[id].approvedBy} ( ${dataInternalNotification[id].approvedBy} )")
              ],
            ),
            Divider(
              thickness: 1,
            )
          ],
        ),
      ),
    );
  }
  }
