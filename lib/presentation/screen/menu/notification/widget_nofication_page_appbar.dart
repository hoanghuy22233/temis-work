import 'package:base_code_project/presentation/common_widgets/barrel_common_widgets.dart';
import 'package:base_code_project/presentation/common_widgets/widget_appbar_menu_back_two.dart';
import 'package:base_code_project/presentation/common_widgets/widget_appbar_notification_page.dart';
import 'package:base_code_project/presentation/common_widgets/widget_appbar_notification_seach.dart';
import 'package:base_code_project/utils/locale/app_localization.dart';
import 'package:flutter/material.dart';

class WidgetNotificationPageAppbar extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: WidgetAppbarNotificationPage(
        title: AppLocalizations.of(context).translate('notification_page.appbar').toUpperCase(),
        left: [WidgetAppbarMenuBackTwo()],
        right: [WidgetAppbarNotificationSeach()],
      ),
    );
  }
}
