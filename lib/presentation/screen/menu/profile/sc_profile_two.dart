import 'package:base_code_project/app/constants/navigator/navigator.dart';
import 'package:base_code_project/app/constants/style/style.dart';
import 'package:base_code_project/model/date_not_api/data_not_api_profile_customer.dart';
import 'package:base_code_project/presentation/screen/menu/profile/widget_profile_appbar.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class ProfileTwoPageScreen extends StatefulWidget {
  @override
  _ProfileTwoPageScreenState createState() => _ProfileTwoPageScreenState();
}

class _ProfileTwoPageScreenState extends State<ProfileTwoPageScreen> {
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      top: true,
      child: Scaffold(
          body: Column(
            children: [
              _buildAppbar(),
              SizedBox(height: 10,),
   Padding(padding: EdgeInsets.symmetric(horizontal: 20,vertical: 20),
   child: Column(
     children: [
       Row(
         mainAxisAlignment: MainAxisAlignment.start,
         children: <Widget>[
           Container(
             width: 70,
             height: 70,
             child: ClipRRect(
               borderRadius: BorderRadius.circular(300.0),
               child: Image.asset(dataProfileCustomer[0].img,
                   fit: BoxFit.cover
                 // width: 30,
                 // height: 30,
               ),
             ),
           ),
           SizedBox(
             width: 10,
           ),
           Expanded(
             flex: 7,
             child: Column(
               crossAxisAlignment: CrossAxisAlignment.start,
               mainAxisAlignment: MainAxisAlignment.start,
               children: <Widget>[
                 Text(
                   dataProfileCustomer[0].textForm,
                   style: const TextStyle(
                       fontSize: 16, fontWeight: FontWeight.bold),
                 ),
                 SizedBox(
                   height: 5,
                 ),
                 Text(
                   "${dataProfileCustomer[0].textPhone}",
                   style: const TextStyle(
                     fontSize: 14,fontWeight: FontWeight.bold,
                     color: Colors.black ,
                   ),
                 ),


               ],
             ),
           ),
         ],
       ),
       SizedBox(
         height: 15,
       ),
       GestureDetector(
         onTap: () {
           AppNavigator.navigateProfileDetail();
         },
         child: Column(
           children: [
             Container(
               child: Padding(
                 padding: const EdgeInsets.symmetric(vertical: 20),
                 child: Row(
                   // mainAxisAlignment: MainAxisAlignment.start,
                   children: [
                     Image.asset("assets/images/ic_person_two.png",width: 25,height: 25,),
                     SizedBox(
                       width: 10,
                     ),
                     Expanded(
                       flex: 3,
                       child: Container(
                         child: Text(
                           "Thông tin tài khoản",
                           style: TextStyle(
                               fontSize: 15, color: Colors.black, height: 1.2),
                         ),
                       ),
                     ),
                     Expanded(
                       flex: 1,
                       child: Container(
                         alignment: Alignment.centerRight,
                         child: Text(
                           '',
                           style: AppStyle.DEFAULT_MEDIUM_BOLD,
                         ),
                       ),
                     ),
                     SizedBox(
                       width: 5,
                     ),
                     Icon(
                       Icons.chevron_right,
                       color: Colors.grey,
                     )
                   ],
                 ),
               ),
             ),
             Divider(
               height: 0.1,
               thickness: 0.5,
               color: Colors.grey,
             ),
             SizedBox(
               height: 5,
             ),
           ],
         ),
       ),
       GestureDetector(
         onTap: () {
           AppNavigator.navigateContact();

         },
         child: Column(
           children: [
             Container(
               child: Padding(
                 padding: const EdgeInsets.symmetric(vertical: 20),
                 child: Row(
                   // mainAxisAlignment: MainAxisAlignment.start,
                   children: [
                     Image.asset("assets/images/ic_contact.png",width: 25,height: 25,),
                     SizedBox(
                       width: 10,
                     ),
                     Expanded(
                       flex: 3,
                       child: Container(
                         child: Text(
                           "Liên hệ",
                           style: TextStyle(
                               fontSize: 15, color: Colors.black, height: 1.2),
                         ),
                       ),
                     ),
                     Expanded(
                       flex: 1,
                       child: Container(
                         alignment: Alignment.centerRight,
                         child: Text(
                           '',
                           style: AppStyle.DEFAULT_MEDIUM_BOLD,
                         ),
                       ),
                     ),
                     SizedBox(
                       width: 5,
                     ),
                     Icon(
                       Icons.chevron_right,
                       color: Colors.grey,
                     )
                   ],
                 ),
               ),
             ),
             Divider(
               height: 0.1,
               thickness: 0.5,
               color: Colors.grey,
             ),
             SizedBox(
               height: 5,
             ),
           ],
         ),
       ),
       GestureDetector(
         onTap: () {
           AppNavigator.navigateTerm();

         },
         child: Column(
           children: [
             Container(
               child: Padding(
                 padding: const EdgeInsets.symmetric(vertical: 20),
                 child: Row(
                   // mainAxisAlignment: MainAxisAlignment.start,
                   children: [
                     Image.asset("assets/images/ic_rules.png",width: 25,height: 25,),
                     SizedBox(
                       width: 10,
                     ),
                     Expanded(
                       flex: 3,
                       child: Container(
                         child: Text(
                           "Nội quy và chính sách",
                           style: TextStyle(
                               fontSize: 15, color: Colors.black, height: 1.2),
                         ),
                       ),
                     ),
                     Expanded(
                       flex: 1,
                       child: Container(
                         alignment: Alignment.centerRight,
                         child: Text(
                           '',
                           style: AppStyle.DEFAULT_MEDIUM_BOLD,
                         ),
                       ),
                     ),
                     SizedBox(
                       width: 5,
                     ),
                     Icon(
                       Icons.chevron_right,
                       color: Colors.grey,
                     )
                   ],
                 ),
               ),
             ),
             Divider(
               height: 0.1,
               thickness: 0.5,
               color: Colors.grey,
             ),
             SizedBox(
               height: 5,
             ),
           ],
         ),
       ),
       GestureDetector(
         onTap: () {
           AppNavigator.navigateLogin();

         },
         child: Column(
           children: [
             Container(
               child: Padding(
                 padding: const EdgeInsets.symmetric(vertical: 20),
                 child: Row(
                   // mainAxisAlignment: MainAxisAlignment.start,
                   children: [
                     Image.asset("assets/images/ic_install.png",width: 25,height: 25,),
                     SizedBox(
                       width: 10,
                     ),
                     Expanded(
                       flex: 3,
                       child: Container(
                         child: Text(
                           "Đăng xuất",
                           style: TextStyle(
                               fontSize: 15, color: Colors.black, height: 1.2),
                         ),
                       ),
                     ),
                     Expanded(
                       flex: 1,
                       child: Container(
                         alignment: Alignment.centerRight,
                         child: Text(
                           '',
                           style: AppStyle.DEFAULT_MEDIUM_BOLD,
                         ),
                       ),
                     ),
                     SizedBox(
                       width: 5,
                     ),
                     Icon(
                       Icons.chevron_right,
                       color: Colors.grey,
                     )
                   ],
                 ),
               ),
             ),
             Divider(
               height: 0.1,
               thickness: 0.5,
               color: Colors.grey,
             ),
             SizedBox(
               height: 5,
             ),
           ],
         ),
       ),




     ],
   ),)
            ],
          )),
    );
  }
}
Widget _buildAppbar() => WidgetProfileAppbar();

