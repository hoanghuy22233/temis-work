
import 'package:base_code_project/presentation/screen/menu/crm/require_supported/seach_require/list_seach_require_supported.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';


class SeachRequireSupportedPageScreen extends StatefulWidget {
  SeachRequireSupportedPageScreen({Key key, this.title}) : super(key: key);
  final String title;

  @override
  SeachRequireSupportedPageScreenState createState() => SeachRequireSupportedPageScreenState();
}

class SeachRequireSupportedPageScreenState extends State<SeachRequireSupportedPageScreen> {
  @override
  Widget build(BuildContext context) {
    return Container(
      width: MediaQuery.of(context).size.width,
      height: MediaQuery.of(context).size.height/2 ,
      child: (
          Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Expanded(
            child: Padding(
              padding:
              const EdgeInsets.symmetric(horizontal: 15, vertical: 5),
              child:  WidgetSeachRequireSupported(),
            ),
          ),
        ],
      )),
    );
  }
}
