
import 'package:base_code_project/presentation/screen/menu/crm/require_supported/seach_date_require_supported/sc_seach_date_require_supported.dart';
import 'package:base_code_project/presentation/screen/menu/crm/require_supported/seach_require/sc_seach_require_supported.dart';
import 'package:flutter/material.dart';
import 'package:modal_bottom_sheet/modal_bottom_sheet.dart';


class WidgetRequireSupported extends StatefulWidget {
  const WidgetRequireSupported({
    Key key,
  }) : super(key: key);

  @override
  _WidgetRequireSupportedState createState() =>
      _WidgetRequireSupportedState();
}

class _WidgetRequireSupportedState extends State<WidgetRequireSupported>
    with SingleTickerProviderStateMixin {
  final controller = PageController();
  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        width: MediaQuery.of(context).size.width,
        height: MediaQuery.of(context).size.height,
        child: Column(
          children: [
            Expanded(flex: 1,
              //   child:  Container(
              //   width: MediaQuery.of(context).size.width,
              //   height: MediaQuery.of(context).size.height / 14,
              //   decoration: BoxDecoration
              //     (
              //     border: Border(
              //       top: BorderSide( //                   <--- left side
              //         color: Colors.grey[400],
              //         width: 2.0,
              //       ),
              //       bottom: BorderSide( //                    <--- top side
              //         color: Colors.grey[400],
              //         width: 2.0,
              //       ),
              //     ),
              //   ),
              //   // margin: EdgeInsets.symmetric(horizontal: 17),
              //   child: Row(
              //     children: [
              //       SizedBox(width: 15,),
              //       Image.asset(
              //         "assets/images/img_search.png",
              //         width: 15,
              //         height: 15,
              //       ),
              //       SizedBox(
              //         width: 20,
              //       ),
              //       Expanded(
              //         flex: 8,
              //         child: TextField(
              //           decoration: new InputDecoration.collapsed(
              //             // border: InputBorder.none,
              //               hintText: 'Tìm kiếm'),
              //         ),
              //       )
              //     ],
              //   ),
              // ),
              child: Container(),
            ),
            Expanded(flex: 8,child: Container()),
            Expanded(
              flex: 1,
              child: Column(
                children: [
                  Container(
                    width: MediaQuery.of(context).size.width,
                    height: 2,
                    color: Colors.grey[400],
                  ),
                  Row(
                    children: [
                      Expanded(
                        flex: 2,
                        // child: Container(),
                        child:  Padding(
                          padding: const EdgeInsets.only(top: 10,left: 10),
                          child: GestureDetector(
                            onTap: () {
                              showMaterialModalBottomSheet(
                                shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(10.0),
                                ),
                                backgroundColor: Colors.white,
                                context: context,
                                builder: (context) => SeachRequireSupportedPageScreen(),
                              );
                            },
                            child: Image.asset(
                              "assets/images/ic_fillter.png",
                              width: 25,
                              height: 25,
                              color: Colors.blue,
                            ),
                          ),
                        ),

                      ),

                      Expanded(
                        flex: 6,
                        child:  Padding(
                          padding: const EdgeInsets.only(top: 10,),
                          // child: Container(),
                          child: Center(child: Text("Tất cả YC hỗ trợ",style: TextStyle(fontWeight: FontWeight.bold,fontSize: 14),))
                        ),
                      ),
                      Expanded(
                        flex: 2,
                        child:  Padding(
                          padding: const EdgeInsets.only(top: 10,right: 10),
                          child: GestureDetector(
                            onTap: () {
                              showMaterialModalBottomSheet(
                                shape: RoundedRectangleBorder(
                                  // borderRadius: BorderRadius.circular(10.0),
                                  borderRadius: BorderRadius.only(
                                      topLeft: Radius.circular(15.0),
                                      topRight: Radius.circular(15.0)),
                                ),
                                backgroundColor: Colors.white,
                                context: context,
                                builder: (context) => SeachDateRequireSupportedPageScreen(),
                              );
                            },
                            child: Image.asset(
                              "assets/images/ic_list.png",
                              width: 25,
                              height: 25,
                              color: Colors.blue,
                            ),
                          ),
                        ),

                      )

                    ],
                  ),
                ],
              ),
            ),




            // Expanded(
            //   flex: 1,
            //   child: Column(
            //    children: [
            //      Container(
            //        width: MediaQuery.of(context).size.width,
            //        height: 2,
            //        color: Colors.grey[400],
            //      ),
            //      Row(
            //        children: [
            //          Expanded(
            //            flex: 6,
            //            child:  Padding(
            //                padding: const EdgeInsets.only(top: 10,),
            //                child: Center(child: Text("Tất cả YC hỗ trợ",style: TextStyle(fontWeight: FontWeight.bold,fontSize: 14),))
            //            ),
            //
            //          ),
            //
            //        ],
            //      ),
            //    ],
            //   ),
            // ),
          ],
        ),
      ),
    );
  }
}
