import 'package:base_code_project/model/date_not_api/data_not_api_crm.dart';
import 'package:base_code_project/presentation/screen/menu/crm/crm_customer/sc_crm_customer.dart';
import 'package:base_code_project/presentation/screen/menu/crm/require_supported/sc_require_supported.dart';
import 'package:base_code_project/presentation/screen/menu/crm/sales_opportunities/sc_sales_opportunities.dart';
import 'customer_care/sc_customer_care.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class ChildListCrm extends StatelessWidget {
  final int id;
  ChildListCrm({Key key, this.id}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        switch (dataCrm[id].id)
        {
          case 0:
            // return(
            //     showMaterialModalBottomSheet(
            //       context: context,
            //       builder: (context) => MonitoringPageScreen(),
            //     )
            // );
            return (Navigator.push(
              context,
              MaterialPageRoute(builder: (context) => CustomerCarePageScreen()),
            ));
            break;
          case 1:
            return (Navigator.push(
              context,
              MaterialPageRoute(builder: (context) => RequireSupportedPageScreen()),
            ));
            break;
          case 2:
            return (Navigator.push(
              context,
              MaterialPageRoute(builder: (context) => SalesOpportunitiesPageScreen()),
            ));
            break;
          case 3:
            return (Navigator.push(
              context,
              MaterialPageRoute(builder: (context) => CrmCustomerPageScreen()),
            ));
            break;

        }
      },
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              Expanded(
                  flex: 1,
                  child: Image.asset(
                    "${dataCrm[id].img}",
                    width: 20,
                    height: 20,
                  )),
              Expanded(
                  flex: 9,
                  child: Padding(
                    padding: const EdgeInsets.only(left: 30),
                    child: Text("${dataCrm[id].textForm}"),
                  ))
            ],
          ),
          SizedBox(
            height: 20,
          ),
        ],
      ),

    );
  }
}
