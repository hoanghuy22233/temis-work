import 'package:base_code_project/model/date_not_api/data_not_api_customer_care_contact.dart';
import 'package:flutter/material.dart';

class WidgetSeachContact extends StatefulWidget {
  @override
  _WidgetSeachContactState createState() => _WidgetSeachContactState();
}

class _WidgetSeachContactState extends State<WidgetSeachContact> {
  int tappedIndex;
  @override
  void initState() {
    super.initState();
    tappedIndex = 0;
  }
  @override
  Widget build(BuildContext context) {
    return Container(
      // height: MediaQuery.of(context).size.height / 2,
      child: ListView.builder(
        itemCount: dataCustomerCareContact.length,
        itemBuilder: (context, index) {
          return Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              // GestureDetector(
              // onTap: () {
              //   if (dataCustomerCareContact[index].id == 0) {
              //     Text(
              //       "${dataCustomerCareContact[index].textForm}",
              //       style: TextStyle(color: Colors.blue),
              //     );
              //   }
              // }),
              GestureDetector(
                onTap: () {
                  setState(() {
                    tappedIndex = index;
                  });
                },
                child: Padding(
                  padding:
                      const EdgeInsets.symmetric(horizontal: 10, vertical: 15),
                  child: Text("${dataCustomerCareContact[index].textForm}",style: TextStyle(fontWeight: FontWeight.bold,color: tappedIndex == index ? Colors.blue : Colors.black,),),
                ),
              )
            ],
          );
        },
      ),
    );
  }
}
