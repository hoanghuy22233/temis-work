import 'package:base_code_project/model/date_not_api/data_not_api_customer_care_overview.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class ChildCustomerCareOverview extends StatelessWidget {
  final int id;
  ChildCustomerCareOverview({Key key, this.id}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Row(
          children: [
            Expanded(
                flex: 1,
                child: Image.asset(
                  "${dataOverview[id].img}",
                  width: 10,
                  height: 8,
                )),
            Expanded(flex: 9, child: Text("${dataOverview[id].textForm}")),
            Expanded(
                flex: 1,
                child: Text(
                  "${dataOverview[id].number.toString()}",
                  style: TextStyle(fontWeight: FontWeight.bold),
                )),
          ],
        ),
        SizedBox(
          height: 5,
        )
      ],
    );
  }
}
