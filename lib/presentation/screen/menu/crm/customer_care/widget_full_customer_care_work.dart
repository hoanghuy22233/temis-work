
import 'package:base_code_project/presentation/screen/menu/crm/customer_care/seach_care_work/sc_seach_care_work.dart';
import 'package:base_code_project/presentation/screen/menu/crm/customer_care/seach_date_overview/sc_seach_date_overview.dart';
import 'package:flutter/material.dart';
import 'package:modal_bottom_sheet/modal_bottom_sheet.dart';

import 'list_customer_care_overview_two.dart';

class WidgetCustomerCareWork extends StatefulWidget {
  const WidgetCustomerCareWork({
    Key key,
  }) : super(key: key);

  @override
  _WidgetCustomerCareWorkState createState() =>
      _WidgetCustomerCareWorkState();
}

class _WidgetCustomerCareWorkState extends State<WidgetCustomerCareWork>
    with SingleTickerProviderStateMixin {
  final controller = PageController();
  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        width: MediaQuery.of(context).size.width,
        height: MediaQuery.of(context).size.height,
        child: Column(
          // crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Expanded(flex: 1,child:  Container(
              width: MediaQuery.of(context).size.width,
              height: MediaQuery.of(context).size.height / 14,
              decoration: BoxDecoration
                (
                border: Border(
                  top: BorderSide( //                   <--- left side
                    color: Colors.grey[400],
                    width: 2.0,
                  ),
                  bottom: BorderSide( //                    <--- top side
                    color: Colors.grey[400],
                    width: 2.0,
                  ),
                ),
              ),
              // margin: EdgeInsets.symmetric(horizontal: 17),
              child: Row(
                children: [
                  SizedBox(width: 15,),
                  Image.asset(
                    "assets/images/img_search.png",
                    width: 15,
                    height: 15,
                  ),
                  SizedBox(
                    width: 20,
                  ),
                  Expanded(
                    flex: 8,
                    child: TextField(
                      decoration: new InputDecoration.collapsed(
                        // border: InputBorder.none,
                          hintText: 'Tìm kiếm'),
                    ),
                  )
                ],
              ),
            ),),

            Expanded(flex: 8,child: Container()),
            Expanded(
              flex: 1,
              child: Column(
                children: [
                  Container(
                    width: MediaQuery.of(context).size.width,
                    height: 2,
                    color: Colors.grey[400],
                  ),
                  Row(
                    children: [
                      Expanded(
                        flex: 2,
                        // child: Container(),
                        child:  Padding(
                          padding: const EdgeInsets.only(top: 10,left: 20),
                          child: GestureDetector(
                            onTap: () {
                              showMaterialModalBottomSheet(
                                shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.only(
                                      topLeft: Radius.circular(15.0),
                                      topRight: Radius.circular(15.0)),
                                  // borderRadius: BorderRadius.circular(10.0),
                                ),
                                backgroundColor: Colors.white,
                                context: context,
                                builder: (context) => SeachCareWorkPageScreen(),
                              );
                            },
                            child: Image.asset(
                              "assets/images/ic_fillter.png",
                              width: 25,
                              height: 25,
                              color: Colors.blue,
                            ),
                          ),
                        ),

                      ),
                      Expanded(
                        flex: 6,
                        child:  Padding(
                            padding: const EdgeInsets.only(top: 10,),
                            child: Center(child: Text("Tất cả trạng thái",style: TextStyle(fontWeight: FontWeight.bold,fontSize: 14),))
                        ),
                      ),
                      Expanded(
                        flex: 2,
                        child:  Padding(
                          padding: const EdgeInsets.only(top: 10,right: 20),
                          child: GestureDetector(
                            onTap: () {
                              showMaterialModalBottomSheet(
                                shape: RoundedRectangleBorder(
                                  // borderRadius: BorderRadius.circular(10.0),
                                  borderRadius: BorderRadius.only(
                                      topLeft: Radius.circular(15.0),
                                      topRight: Radius.circular(15.0)),
                                ),
                                backgroundColor: Colors.white,
                                context: context,
                                builder: (context) => SeachDateOverViewPageScreen(),
                              );
                            },
                            child: Image.asset(
                              "assets/images/ic_list.png",
                              width: 25,
                              height: 25,
                              color: Colors.blue,
                            ),
                          ),
                        ),
                      )
                    ],
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
