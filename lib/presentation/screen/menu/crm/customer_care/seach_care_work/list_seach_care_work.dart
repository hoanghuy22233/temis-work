import 'package:base_code_project/model/date_not_api/data_not_api_customer_care_work.dart';
import 'package:flutter/material.dart';

class WidgetSeachCareWork extends StatefulWidget {
  @override
  _WidgetSeachCareWorkState createState() => _WidgetSeachCareWorkState();
}

class _WidgetSeachCareWorkState extends State<WidgetSeachCareWork> {
  int tappedIndex;
  @override
  void initState() {
    super.initState();
    tappedIndex = 0;
  }
  @override
  Widget build(BuildContext context) {
    return Container(
      // height: MediaQuery.of(context).size.height / 2,
      child: ListView.builder(
        itemCount: dataCustomerCareWork.length,
        itemBuilder: (context, index) {
          return Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              GestureDetector(
                onTap: () {
                  setState(() {
                    tappedIndex = index;
                  });
                },
                child: Padding(
                  padding:
                      const EdgeInsets.symmetric(horizontal: 10, vertical: 15),
                  child: Text("${dataCustomerCareWork[index].textForm}",style: TextStyle(fontWeight: FontWeight.bold,color: tappedIndex == index ? Colors.blue : Colors.black,),),
                ),
              )
            ],
          );
        },
      ),
    );
  }
}
