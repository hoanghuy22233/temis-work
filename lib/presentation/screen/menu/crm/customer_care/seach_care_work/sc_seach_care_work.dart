
import 'package:base_code_project/presentation/screen/menu/crm/customer_care/seach_care_work/list_seach_care_work.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';


class SeachCareWorkPageScreen extends StatefulWidget {
  SeachCareWorkPageScreen({Key key, this.title}) : super(key: key);
  final String title;

  @override
  SeachCareWorkPageScreenState createState() => SeachCareWorkPageScreenState();
}

class SeachCareWorkPageScreenState extends State<SeachCareWorkPageScreen> {
  @override
  Widget build(BuildContext context) {
    return Container(
      width: MediaQuery.of(context).size.width,
      height: MediaQuery.of(context).size.height/3 ,
      child: (
          Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Expanded(
            child: Padding(
              padding:
              const EdgeInsets.symmetric(horizontal: 15, vertical: 5),
              child:  WidgetSeachCareWork(),
            ),
          ),
        ],
      )),
    );
  }
}
