import 'package:base_code_project/presentation/screen/menu/crm/customer_care/seach_status/list_seach_status.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';


class SeachStatusPageScreen extends StatefulWidget {
  SeachStatusPageScreen({Key key, this.title}) : super(key: key);
  final String title;

  @override
  SeachStatusPageScreenState createState() => SeachStatusPageScreenState();
}

class SeachStatusPageScreenState extends State<SeachStatusPageScreen> {
  @override
  Widget build(BuildContext context) {
    return Container(
      width: MediaQuery.of(context).size.width,
      height: MediaQuery.of(context).size.height/3 ,
      child: (
          Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Expanded(
            child: Padding(
              padding:
              const EdgeInsets.symmetric(horizontal: 15, vertical: 5),
              child:  WidgetSeachStatus(),
            ),
          ),
        ],
      )),
    );
  }
}
