import 'package:base_code_project/model/date_not_api/data_not_api_customer_care_full_status.dart';
import 'package:flutter/material.dart';

class WidgetSeachStatus extends StatefulWidget {
  @override
  _WidgetSeachStatusState createState() => _WidgetSeachStatusState();
}

class _WidgetSeachStatusState extends State<WidgetSeachStatus> {
  int tappedIndex;
  @override
  void initState() {
    super.initState();
    tappedIndex = 0;
  }
  @override
  Widget build(BuildContext context) {
    return Container(
      // height: MediaQuery.of(context).size.height / 2,
      child: ListView.builder(
        itemCount: dataStatusFull.length,
        itemBuilder: (context, index) {
          return Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              // GestureDetector(
              // onTap: () {
              //   if (dataStatusFull[index].id == 0) {
              //     Text(
              //       "${dataStatusFull[index].textForm}",
              //       style: TextStyle(color: Colors.blue),
              //     );
              //   }
              // }),
              GestureDetector(
                onTap: () {
                  setState(() {
                    tappedIndex = index;
                  });
                },
                child: Padding(
                  padding:
                      const EdgeInsets.symmetric(horizontal: 10, vertical: 15),
                  child: Text("${dataStatusFull[index].textForm}",style: TextStyle(fontWeight: FontWeight.bold,color: tappedIndex == index ? Colors.blue : Colors.black,),),
                ),
              )
            ],
          );
        },
      ),
    );
  }
}
