import 'package:base_code_project/app/constants/navigator/navigator.dart';
import 'package:base_code_project/presentation/common_widgets/widget_appbar_customer_care.dart';
import 'package:base_code_project/presentation/screen/menu/crm/customer_care/widget_full_customer_care_contact.dart';
import 'package:base_code_project/presentation/screen/menu/crm/customer_care/widget_full_customer_care_overview.dart';
import 'package:base_code_project/presentation/screen/menu/crm/customer_care/widget_full_customer_care_visit.dart';
import 'package:base_code_project/presentation/screen/menu/crm/customer_care/widget_full_customer_care_work.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:base_code_project/app/constants/color/color.dart';
import 'package:base_code_project/app/constants/style/style.dart';
import 'package:base_code_project/app/constants/value/value.dart';

class CustomerCarePageScreen extends StatefulWidget {
  final int id;
  CustomerCarePageScreen({Key key, this.id}) : super(key: key);

  @override
  _CustomerCarePageScreenState createState() => _CustomerCarePageScreenState();
}

class _CustomerCarePageScreenState extends State<CustomerCarePageScreen>
    with SingleTickerProviderStateMixin {
  final PageController _pageController = PageController(initialPage: 0);
  TabController _tabController;
  @override
  void initState() {
    super.initState();
    _tabController = new TabController(vsync: this, length: 4);
    _tabController.addListener(() {
      print('my index is' + _tabController.index.toString());
    });
  }

  @override
  void dispose() {
    _tabController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
        top: true,
        child: Scaffold(
          body: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Stack(
                children: [
                  _buildAppbar(),
                  _buildTabBarMenu(),
                  //_buildTabBar()
                ],
              ),
              Expanded(
                  child: Padding(
                padding: const EdgeInsets.all(AppValue.APP_HORIZONTAL_PADDING),
                child: TabBarView(
                  controller: _tabController,
                  children: [
                    WidgetCustomerCareOverview(),
                    WidgetCustomerCareVisit(),
                    // BannerPages(
                    //   drawer: _drawerKey,
                    // ),
                    WidgetCustomerCareContact(),
                    WidgetCustomerCareWork(),
                  ],
                ),
              )),
            ],
          ),
        ));
  }

  _buildAppbar() => WidgetAppbarCustomerCare(
        backgroundColor: Colors.blue,
        textColor: Colors.white,
        title: "Chăm sóc khách hàng",
        titleDate: "Tháng này",
        right: [
          // Padding(padding: EdgeInsets.only(top:7,right: 10),
          //   child: GestureDetector(
          //     onTap: () {
          //       showMaterialModalBottomSheet(
          //         shape: RoundedRectangleBorder(
          //           borderRadius: BorderRadius.circular(10.0),
          //         ),
          //         backgroundColor: Colors.white,
          //         context: context,
          //         builder: (context) => SeachWorkPageScreen(),
          //
          //       );
          //     },
          //     child: Image.asset(
          //       "assets/images/ic_fillter.png",
          //       width: 25,
          //       height: 25,
          //       color: Colors.white,
          //     ),
          //   ),
          // )
        ],
        left: [
          Padding(
            padding: const EdgeInsets.only( left: 10),
            child: GestureDetector(
              onTap: () {
                AppNavigator.navigateBack();
              },
              child: Image.asset(
                "assets/images/ic_back_two.png",
                width: 20,
                height: 20,
                color: Colors.white,
              ),
            ),
          )
        ],
      );

  Widget _buildTabBarMenu() {
    return new Container(
      width: MediaQuery.of(context).size.width,
      margin: EdgeInsets.only(top: 45, left: 0, right: 0),
      color: Colors.white,
      height: AppValue.ACTION_BAR_HEIGHT,
      child: new TabBar(
        controller: _tabController,
        labelPadding: EdgeInsets.symmetric(
          horizontal: 10,
        ),
        tabs: [
          Container(
            constraints:
                BoxConstraints(minWidth: MediaQuery.of(context).size.width / 5),
            alignment: Alignment.center,
            child: Text(
              'TỔNG QUAN',
              textAlign: TextAlign.center,
            ),
          ),
          Container(
            constraints:
                BoxConstraints(minWidth: MediaQuery.of(context).size.width / 5),
            alignment: Alignment.center,
            child: Text(
              'VIẾNG THĂM',
              textAlign: TextAlign.center,
            ),
          ),
          Container(
            constraints:
                BoxConstraints(minWidth: MediaQuery.of(context).size.width / 5),
            alignment: Alignment.center,
            child: Text(
              'LIÊN LẠC',
              textAlign: TextAlign.center,
            ),
          ),
          Container(
            constraints:
                BoxConstraints(minWidth: MediaQuery.of(context).size.width / 5),
            alignment: Alignment.center,
            child: Text(
              'CÔNG VIỆC',
              textAlign: TextAlign.center,
            ),
          ),
        ],
        labelStyle: AppStyle.DEFAULT_SMALL_BOLD,
        unselectedLabelStyle: AppStyle.DEFAULT_SMALL,
        labelColor: Colors.blue,
        unselectedLabelColor: AppColor.BLACK,
        physics: BouncingScrollPhysics(),
        indicatorWeight: 3,
        isScrollable: true,
        indicatorPadding: EdgeInsets.symmetric(horizontal: 10),
        indicatorColor: Colors.blue,
        // indicator: new BubbleTabIndicator(
        //   indicatorHeight: AppValue.ACTION_BAR_HEIGHT - 10,
        //   indicatorColor: Colors.white,
        //   tabBarIndicatorSize: TabBarIndicatorSize.tab,
        //   indicatorRadius: 50,
        // ),
      ),
    );
  }
}
