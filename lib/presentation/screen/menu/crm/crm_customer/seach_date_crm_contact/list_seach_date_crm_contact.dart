import 'package:base_code_project/model/date_not_api/data_not_api_list_crm_contact.dart';
import 'package:flutter/material.dart';

class WidgetListSeachCrmContact extends StatefulWidget {
  @override
  _WidgetListSeachCrmContactState createState() => _WidgetListSeachCrmContactState();
}

class _WidgetListSeachCrmContactState extends State<WidgetListSeachCrmContact> {
  int tappedIndex;
  @override
  void initState() {
    super.initState();
    tappedIndex = 0;
  }
  @override
  Widget build(BuildContext context) {
    return Container(
      // height: MediaQuery.of(context).size.height / 2,
      child: ListView.builder(
        itemCount: dataListCrmContact.length,
        itemBuilder: (context, index) {
          return Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              GestureDetector(
                onTap: () {
                  setState(() {
                    tappedIndex = index;
                  });
                },
                child: Padding(
                  padding:
                      const EdgeInsets.symmetric(horizontal: 10, vertical: 15),
                  child: Text("${dataListCrmContact[index].textForm}",style: TextStyle(fontWeight: FontWeight.bold,color: tappedIndex == index ? Colors.blue : Colors.black,),),
                ),
              )
            ],
          );
        },
      ),
    );
  }
}
