import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:base_code_project/model/date_not_api/data_not_api_fillter_crm_contact.dart';

import 'list_seach_crm_customer.dart';


class SeachCrmCustomerPageScreen extends StatefulWidget {
  SeachCrmCustomerPageScreen({Key key, this.title}) : super(key: key);
  final String title;

  @override
  _SeachCrnCustomerPageScreenState createState() => _SeachCrnCustomerPageScreenState();
}

class _SeachCrnCustomerPageScreenState extends State<SeachCrmCustomerPageScreen> {


  @override
  Widget build(BuildContext context) {
    return Container(
      width: MediaQuery.of(context).size.width,
      height: MediaQuery.of(context).size.height/3.8 ,
      child: (
          Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Expanded(
            child: Padding(
              padding:
              const EdgeInsets.symmetric(horizontal: 15, vertical: 5),
              child:  WidgetSeachCrmCustomer(),
            ),
          ),
        ],
      )),
    );
  }
}
