import 'package:base_code_project/model/date_not_api/data_not_api_fillter_crm_contact.dart';
import 'package:base_code_project/model/date_not_api/data_not_api_seach_require_fillter.dart';
import 'package:flutter/material.dart';

class WidgetSeachCrmCustomer extends StatefulWidget {
  @override
  _WidgetSeachCrmCustomerState createState() => _WidgetSeachCrmCustomerState();
}

class _WidgetSeachCrmCustomerState extends State<WidgetSeachCrmCustomer> {
  int tappedIndex;
  @override
  void initState() {
    super.initState();
    tappedIndex = 0;
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      // height: MediaQuery.of(context).size.height / 2,
      // child: ListView.builder(
      //     shrinkWrap: true,
      //     itemCount: dataFillterCrmContact.length,
      //     itemBuilder: (context, index) {
      //       return Container(
      //           // color: tappedIndex == index ? Colors.blue : Colors.grey,
      //           child: ListTile(
      //               title: Text(
      //                 '${dataFillterCrmContact[index].textForm}',
      //                 style: TextStyle(
      //                   color:
      //                       tappedIndex == index ? Colors.blue : Colors.grey,
      //                 ),
      //               ),
      //               onTap: () {
      //                 setState(() {
      //                   tappedIndex = index;
      //                 });
      //               }));
      //     })
      child: ListView.builder(
        itemCount: dataFillterCrmContact.length,
        itemBuilder: (context, index) {
          return Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              // Text(
              //     "${dataFillterCrmContact[index].textForm}",
              //     style: pressed
              //         ? TextStyle(color: Colors.black,fontWeight: FontWeight.bold)
              //         : TextStyle(color:Colors.green,fontWeight: FontWeight.bold),
              //     // style: TextStyle(fontWeight: FontWeight.bold),
              //   ),
              //   RaisedButton(
              //     child: new Text(
              //         'Change color'),
              //     onPressed: () {
              //       setState(() {
              //         pressed = !pressed;
              //       });
              //     },
              //   )
              GestureDetector(
                onTap: () {
                  setState(() {
                    tappedIndex = index;
                  });
                },
                child: Padding(
                  padding:
                      const EdgeInsets.symmetric(horizontal: 10, vertical: 15),
                  child: Text(
                    "${dataFillterCrmContact[index].textForm}",
                    style: TextStyle(
                      color: tappedIndex == index ? Colors.blue : Colors.black,
                      fontWeight: FontWeight.bold
                    ),
                    // style: TextStyle(fontWeight: FontWeight.bold),
                  ),
                ),
              )
            ],
          );
        },
      ),
    );
  }
}
