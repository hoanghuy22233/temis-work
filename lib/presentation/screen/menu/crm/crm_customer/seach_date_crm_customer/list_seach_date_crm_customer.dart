import 'package:base_code_project/model/date_not_api/data_not_api_list_crm_customer.dart';
import 'package:flutter/material.dart';

class WidgetListSeachCrmCustomer extends StatefulWidget {
  @override
  _WidgetListSeachCrmCustomerState createState() => _WidgetListSeachCrmCustomerState();
}

class _WidgetListSeachCrmCustomerState extends State<WidgetListSeachCrmCustomer> {
  int tappedIndex;
  @override
  void initState() {
    super.initState();
    tappedIndex = 0;
  }
  @override
  Widget build(BuildContext context) {
    return Container(
      // height: MediaQuery.of(context).size.height / 2,
      child: ListView.builder(
        itemCount: dataListCrmCustomer.length,
        itemBuilder: (context, index) {
          return Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              // GestureDetector(
              // onTap: () {
              //   if (dataListCrmCustomer[index].id == 0) {
              //     Text(
              //       "${dataListCrmCustomer[index].textForm}",
              //       style: TextStyle(color: Colors.blue),
              //     );
              //   }
              // }),
              GestureDetector(
                onTap: () {
                  setState(() {
                    tappedIndex = index;
                  });
                },
                child: Padding(
                  padding:
                      const EdgeInsets.symmetric(horizontal: 10, vertical: 15),
                  child: Text("${dataListCrmCustomer[index].textForm}",style: TextStyle(fontWeight: FontWeight.bold,color: tappedIndex == index ? Colors.blue : Colors.black,),),
                ),
              )
            ],
          );
        },
      ),
    );
  }
}
