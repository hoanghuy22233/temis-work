import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import 'list_seach_date_crm_customer.dart';

class SeachDateCrmCustomerPageScreen extends StatefulWidget {
  SeachDateCrmCustomerPageScreen({Key key, this.title}) : super(key: key);
  final String title;

  @override
  _SeachDateCrmCustomerPageScreenState createState() => _SeachDateCrmCustomerPageScreenState();
}

class _SeachDateCrmCustomerPageScreenState extends State<SeachDateCrmCustomerPageScreen> {
  @override
  Widget build(BuildContext context) {
    return Container(
        width: MediaQuery.of(context).size.width,
        height: MediaQuery.of(context).size.height / 1.6,
        // decoration: new BoxDecoration(
        //     color: Colors.white,
        //     borderRadius: new BorderRadius.only(
        //         topLeft: const Radius.circular(100.0),
        //         topRight: const Radius.circular(100.0))),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Expanded(
              child: Padding(
                padding:
                    const EdgeInsets.symmetric(horizontal: 15, ),
                child: WidgetListSeachCrmCustomer(),
              ),
            ),
          ],
        ));
  }
}
