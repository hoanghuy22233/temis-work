import 'package:base_code_project/model/date_not_api/data_not_api_seach_date_require.dart';
import 'package:flutter/material.dart';

class WidgetListSeachDateRequireSupported extends StatefulWidget {
  @override
  _WidgetListSeachDateSalesOpportunitiesState createState() => _WidgetListSeachDateSalesOpportunitiesState();
}

class _WidgetListSeachDateSalesOpportunitiesState extends State<WidgetListSeachDateRequireSupported> {
  int tappedIndex;
  @override
  void initState() {
    super.initState();
    tappedIndex = 0;
  }
  @override
  Widget build(BuildContext context) {
    return Container(
      // height: MediaQuery.of(context).size.height / 2,
      child: ListView.builder(
        itemCount: dataSeachDateRequire.length,
        itemBuilder: (context, index) {
          return Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              GestureDetector(
                onTap: () {
                  setState(() {
                    tappedIndex = index;
                  });
                },
                child: Padding(
                  padding:
                      const EdgeInsets.symmetric(horizontal: 10, vertical: 15),
                  child: Text("${dataSeachDateRequire[index].textForm}",style: TextStyle(fontWeight: FontWeight.bold,color: tappedIndex == index ? Colors.blue : Colors.black,),),
                ),
              )
            ],
          );
        },
      ),
    );
  }
}
