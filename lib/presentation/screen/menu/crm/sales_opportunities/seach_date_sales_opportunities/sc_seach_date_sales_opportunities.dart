import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import 'list_seach_date_sales_opportunities.dart';

class SeachDateSalesOpportunitiesPageScreen extends StatefulWidget {
  SeachDateSalesOpportunitiesPageScreen({Key key, this.title}) : super(key: key);
  final String title;

  @override
  _SeachDateSalesOpportunitiesPageScreenState createState() => _SeachDateSalesOpportunitiesPageScreenState();
}

class _SeachDateSalesOpportunitiesPageScreenState extends State<SeachDateSalesOpportunitiesPageScreen> {
  @override
  Widget build(BuildContext context) {
    return Container(
        width: MediaQuery.of(context).size.width,
        height: MediaQuery.of(context).size.height / 1.6,
        // decoration: new BoxDecoration(
        //     color: Colors.white,
        //     borderRadius: new BorderRadius.only(
        //         topLeft: const Radius.circular(100.0),
        //         topRight: const Radius.circular(100.0))),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Expanded(
              child: Padding(
                padding:
                    const EdgeInsets.symmetric(horizontal: 15, ),
                child: WidgetListSeachDateRequireSupported(),
              ),
            ),
          ],
        ));
  }
}
