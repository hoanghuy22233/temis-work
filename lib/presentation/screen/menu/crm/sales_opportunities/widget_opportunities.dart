import 'package:base_code_project/presentation/screen/menu/crm/require_supported/seach_require/sc_seach_require_supported.dart';
import 'package:base_code_project/presentation/screen/menu/crm/sales_opportunities/seach_date_sales_opportunities/sc_seach_date_sales_opportunities.dart';
import 'package:flutter/material.dart';
import 'package:modal_bottom_sheet/modal_bottom_sheet.dart';


class WidgetOpportunities extends StatefulWidget {
  const WidgetOpportunities({
    Key key,
  }) : super(key: key);

  @override
  _WidgetOpportunitiesState createState() =>
      _WidgetOpportunitiesState();
}

class _WidgetOpportunitiesState extends State<WidgetOpportunities>
    with SingleTickerProviderStateMixin {
  final controller = PageController();
  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        width: MediaQuery.of(context).size.width,
        height: MediaQuery.of(context).size.height,
        child: Column(
          // crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            // Expanded(
            //   flex: 3,
            //   child: ListView.builder(
            //     itemCount: dataOverview.length + 1,
            //     itemBuilder: (context, index) {
            //       if (index == 0) {
            //         return Padding(
            //           padding: const EdgeInsets.only(left: 20),
            //           child: Text(
            //             "Theo khách hàng cần chăm sóc",
            //             style: TextStyle(
            //                 fontSize: 16, fontWeight: FontWeight.bold),
            //           ),
            //         );
            //       }
            //       return Container(
            //         margin: EdgeInsets.symmetric(horizontal: 10),
            //         child: ChildCustomerCareOverview(
            //           id: index - 1,
            //         ),
            //       );
            //     },
            //   ),
            // ),
            // Container(
            //   width: MediaQuery.of(context).size.width,
            //   height: 2,
            //   color: Colors.grey[400],
            // ),
            // // Expanded(
            // //   flex: 1,
            // //   child: Container(
            // //     width: MediaQuery.of(context).size.width,
            // //     height: 2,
            // //     color: Colors.grey,
            // //   ),
            // // ),
            // Expanded(
            //   flex: 4,
            //   child: ListView.builder(
            //     itemCount: dataOverviewTwo.length + 1,
            //     itemBuilder: (context, index) {
            //       if (index == 0) {
            //         return Padding(
            //           padding: const EdgeInsets.only(left: 20),
            //           child: Text(
            //             "Theo khách hàng cần chăm sóc",
            //             style: TextStyle(
            //                 fontSize: 16, fontWeight: FontWeight.bold),
            //           ),
            //         );
            //       }
            //       return Container(
            //         margin: EdgeInsets.symmetric(horizontal: 10),
            //         child: ChildCustomerCareOverviewTwo(
            //           id: index - 1,
            //         ),
            //       );
            //     },
            //   ),
            // ),
            // Container(
            //   width: MediaQuery.of(context).size.width,
            //   height: 2,
            //   color: Colors.grey[400],
            // ),
            // Container(
            //   width: MediaQuery.of(context).size.width,
            //   height: 3,
            //   // color: Colors.grey[300],
            //   decoration: BoxDecoration(
            //     border: Border(
            //       bottom: BorderSide(width: 2.0, color: Colors.grey[300]),
            //     ),
            //     boxShadow: [
            //       BoxShadow(
            //         color: Colors.grey.withOpacity(0.1),
            //         spreadRadius: 1,
            //         blurRadius: 7,
            //         offset: Offset(0, 2), // changes position of shadow
            //       ),
            //     ],
            //   ),
            // ),
            //
            // Container(
            //   width: MediaQuery.of(context).size.width,
            //   height: MediaQuery.of(context).size.height / 15,
            //   margin: EdgeInsets.symmetric(horizontal: 17),
            //   child: Row(
            //     children: [
            //       Image.asset(
            //         "assets/images/img_search.png",
            //         width: 15,
            //         height: 15,
            //       ),
            //       SizedBox(
            //         width: 20,
            //       ),
            //       Expanded(
            //         flex: 8,
            //         child: TextField(
            //           decoration: new InputDecoration.collapsed(
            //             // border: InputBorder.none,
            //               hintText: 'Tìm kiếm'),
            //         ),
            //       )
            //     ],
            //   ),
            // ),
            // Container(
            //   width: MediaQuery.of(context).size.width,
            //   height: 3,
            //   // color: Colors.grey[300],
            //   decoration: BoxDecoration(
            //     border: Border(
            //       bottom: BorderSide(width: 2.0, color: Colors.grey[300]),
            //     ),
            //     boxShadow: [
            //       BoxShadow(
            //         color: Colors.grey.withOpacity(0.1),
            //         spreadRadius: 1,
            //         blurRadius: 7,
            //         offset: Offset(0, 2), // changes position of shadow
            //       ),
            //     ],
            //   ),
            // ),


            Expanded(flex: 9,child: Container()),
            Expanded(
              flex: 1,
              child: Column(
                children: [  Container(
                  width: MediaQuery.of(context).size.width,
                  height: 2,
                  color: Colors.grey[400],
                ),
               Row(
                    children: [
                      Expanded(
                        flex: 2,
                        // child: Container(),
                        child:  Padding(
                          padding: const EdgeInsets.only(top: 10,left: 10),
                          child: GestureDetector(
                            onTap: () {
                              showMaterialModalBottomSheet(
                                shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(10.0),
                                ),
                                backgroundColor: Colors.white,
                                context: context,
                                builder: (context) => SeachRequireSupportedPageScreen(),
                              );
                            },
                            child: Image.asset(
                              "assets/images/ic_fillter.png",
                              width: 25,
                              height: 25,
                              color: Colors.blue,
                            ),
                          ),
                        ),
                      ),
                      Expanded(
                        flex: 6,
                        child:  Padding(
                            padding: const EdgeInsets.only(top: 10,),
                            child: Center(child: Text("Tất cả trạng thái",style: TextStyle(fontWeight: FontWeight.bold,fontSize: 14),))
                        ),

                      ),
                      Expanded(
                        flex: 2,
                        child:  Padding(
                          padding: const EdgeInsets.only(top: 10,right: 10),
                          child: GestureDetector(
                            onTap: () {
                              showMaterialModalBottomSheet(
                                shape: RoundedRectangleBorder(
                                  // borderRadius: BorderRadius.circular(10.0),
                                  borderRadius: BorderRadius.only(
                                      topLeft: Radius.circular(15.0),
                                      topRight: Radius.circular(15.0)),
                                ),
                                backgroundColor: Colors.white,
                                context: context,
                                builder: (context) => SeachDateSalesOpportunitiesPageScreen(),
                              );
                            },
                            child: Image.asset(
                              "assets/images/ic_list.png",
                              width: 25,
                              height: 25,
                              color: Colors.blue,
                            ),
                          ),
                        ),
                      )
                    ],
                  ),

                ],

              ),
            ),


          ],
        ),
      ),
    );
  }
}
