import 'package:base_code_project/presentation/screen/menu/crm/sales_opportunities/seach_opportunities/list_seach_opportunities.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';


class SeachOpportunitiesPageScreen extends StatefulWidget {
  SeachOpportunitiesPageScreen({Key key, this.title}) : super(key: key);
  final String title;

  @override
  SeachOpportunitiesPageScreenState createState() => SeachOpportunitiesPageScreenState();
}

class SeachOpportunitiesPageScreenState extends State<SeachOpportunitiesPageScreen> {
  @override
  Widget build(BuildContext context) {
    return Container(
      width: MediaQuery.of(context).size.width,
      height: MediaQuery.of(context).size.height/2 ,
      child: (
          Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Expanded(
            child: Padding(
              padding:
              const EdgeInsets.symmetric(horizontal: 15, vertical: 5),
              child:  WidgetSeachOpportunities(),
            ),
          ),
        ],
      )),
    );
  }
}
