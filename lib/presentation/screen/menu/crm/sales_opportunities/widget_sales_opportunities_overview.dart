import 'package:base_code_project/presentation/screen/menu/crm/sales_opportunities/seach_date_sales_opportunities/sc_seach_date_sales_opportunities.dart';
import 'package:flutter/material.dart';
import 'package:modal_bottom_sheet/modal_bottom_sheet.dart';


class WidgetSalesOpportunitiesOverview extends StatefulWidget {
  const WidgetSalesOpportunitiesOverview({
    Key key,
  }) : super(key: key);

  @override
  _WidgetSalesOpportunitiesOverviewState createState() =>
      _WidgetSalesOpportunitiesOverviewState();
}

class _WidgetSalesOpportunitiesOverviewState extends State<WidgetSalesOpportunitiesOverview>
    with SingleTickerProviderStateMixin {
  final controller = PageController();
  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        width: MediaQuery.of(context).size.width,
        height: MediaQuery.of(context).size.height,
        child: Column(
          // crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            // Expanded(
            //   flex: 3,
            //   child: ListView.builder(
            //     itemCount: dataOverview.length + 1,
            //     itemBuilder: (context, index) {
            //       if (index == 0) {
            //         return Padding(
            //           padding: const EdgeInsets.only(left: 20),
            //           child: Text(
            //             "Theo khách hàng cần chăm sóc",
            //             style: TextStyle(
            //                 fontSize: 16, fontWeight: FontWeight.bold),
            //           ),
            //         );
            //       }
            //       return Container(
            //         margin: EdgeInsets.symmetric(horizontal: 10),
            //         child: ChildCustomerCareOverview(
            //           id: index - 1,
            //         ),
            //       );
            //     },
            //   ),
            // ),
            // Container(
            //   width: MediaQuery.of(context).size.width,
            //   height: 2,
            //   color: Colors.grey[400],
            // ),
            // // Expanded(
            // //   flex: 1,
            // //   child: Container(
            // //     width: MediaQuery.of(context).size.width,
            // //     height: 2,
            // //     color: Colors.grey,
            // //   ),
            // // ),
            // Expanded(
            //   flex: 4,
            //   child: ListView.builder(
            //     itemCount: dataOverviewTwo.length + 1,
            //     itemBuilder: (context, index) {
            //       if (index == 0) {
            //         return Padding(
            //           padding: const EdgeInsets.only(left: 20),
            //           child: Text(
            //             "Theo khách hàng cần chăm sóc",
            //             style: TextStyle(
            //                 fontSize: 16, fontWeight: FontWeight.bold),
            //           ),
            //         );
            //       }
            //       return Container(
            //         margin: EdgeInsets.symmetric(horizontal: 10),
            //         child: ChildCustomerCareOverviewTwo(
            //           id: index - 1,
            //         ),
            //       );
            //     },
            //   ),
            // ),
            // Container(
            //   width: MediaQuery.of(context).size.width,
            //   height: 2,
            //   color: Colors.grey[400],
            // ),
            Expanded(flex: 9,child: Container()),
            Expanded(
              flex: 1,
              child: Column(
                  children: [  Container(
                    width: MediaQuery.of(context).size.width,
                    height: 2,
                    color: Colors.grey[400],
                  ),
                    Row(
                      children: [
                        Expanded(
                          flex: 2,
                          child: Container(),
                          // child:  Padding(
                          //   padding: const EdgeInsets.only(bottom: 5,left: 20),
                          //   child: GestureDetector(
                          //     onTap: () {
                          //       showMaterialModalBottomSheet(
                          //         shape: RoundedRectangleBorder(
                          //           borderRadius: BorderRadius.circular(10.0),
                          //         ),
                          //         backgroundColor: Colors.white,
                          //         context: context,
                          //         builder: (context) => SeachDateWorkPageScreen(),
                          //       );
                          //     },
                          //     child: Image.asset(
                          //       "assets/images/ic_fillter.png",
                          //       width: 25,
                          //       height: 25,
                          //       color: Colors.blue,
                          //     ),
                          //   ),
                          // ),
                        ),
                        Expanded(
                          flex: 6,
                          child:  Padding(
                            padding: const EdgeInsets.only(bottom: 20,left: 10),
                            child: Container(
                            ),
                          ),
                        ),
                        Expanded(
                          flex: 2,
                          child:  Padding(
                            padding: const EdgeInsets.only(top: 10,right: 20),
                            child: GestureDetector(
                              onTap: () {
                                showMaterialModalBottomSheet(
                                  shape: RoundedRectangleBorder(
                                    // borderRadius: BorderRadius.circular(10.0),
                                    borderRadius: BorderRadius.only(
                                        topLeft: Radius.circular(15.0),
                                        topRight: Radius.circular(15.0)),
                                  ),
                                  backgroundColor: Colors.white,
                                  context: context,
                                  builder: (context) => SeachDateSalesOpportunitiesPageScreen(),
                                );
                              },
                              child: Image.asset(
                                "assets/images/ic_list.png",
                                width: 25,
                                height: 25,
                                color: Colors.blue,
                              ),
                            ),
                          ),
                        )
                      ],
                    ),

                  ],

              ),
            ),
          ],
        ),
      ),
    );
  }
}
