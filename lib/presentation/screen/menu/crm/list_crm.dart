import 'package:base_code_project/model/date_not_api/data_not_api_crm.dart';
import 'package:base_code_project/presentation/screen/menu/crm/child_list_crm.dart';
import 'package:flutter/material.dart';
class WidgetListCrm extends StatefulWidget {
  @override
  _WidgetListCrmState createState() => _WidgetListCrmState();
}

class _WidgetListCrmState extends State<WidgetListCrm> {
  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      itemCount: dataCrm.length+1,
      itemBuilder: (context, index) {
        if( index == dataCrm.length){
          return GestureDetector
            (
            onTap: () {
              Navigator.pop(context);
            },
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    Expanded(
                        flex: 1,
                        child: Image.asset(
                          "assets/images/delete_red.png",
                          width: 20,
                          height: 20,
                        )),
                    Expanded(
                        flex: 9,
                        child: Padding(
                          padding: const EdgeInsets.only(left: 30),
                          child: Text("Hủy"),
                        ))
                  ],
                ),
                SizedBox(
                  height: 20,
                ),
              ],
            ),
          );
        }else{
          return Container(
              // margin: EdgeInsets.symmetric(horizontal: 10),
        child: ChildListCrm(
        id: index
          ,
        ));
        }

      },
    );
  }
}


