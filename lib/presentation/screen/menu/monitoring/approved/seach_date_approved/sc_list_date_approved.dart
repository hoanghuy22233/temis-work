import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'list_date_approved.dart';

class ListDateApprovedPageScreen extends StatefulWidget {
  ListDateApprovedPageScreen({Key key, this.title}) : super(key: key);
  final String title;

  @override
  _ListDateApprovedPageScreenState createState() => _ListDateApprovedPageScreenState();
}

class _ListDateApprovedPageScreenState extends State<ListDateApprovedPageScreen> {
  @override
  Widget build(BuildContext context) {
    return Container(
        width: MediaQuery.of(context).size.width,
        height: MediaQuery.of(context).size.height / 1.8,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Expanded(
              child: Padding(
                padding:
                    const EdgeInsets.symmetric(horizontal: 15, ),
                child: WidgetListDateApproved(),
              ),
            ),
          ],
        ));
  }
}
