import 'package:base_code_project/presentation/screen/menu/monitoring/approved/detail_letter/widget_detail_letter_appbar.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:base_code_project/model/date_not_api/data_not_api_list_detail_letter.dart';

class DetailLetterPageScreen extends StatefulWidget {
  final int id;
  DetailLetterPageScreen({Key key, this.id}) : super(key: key);
  @override
  _DetailLetterPageScreenState createState() => _DetailLetterPageScreenState();
}

class _DetailLetterPageScreenState extends State<DetailLetterPageScreen> {
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      top: true,
      child: Scaffold(
        body: Column(
          children: [
            _buildAppbar(),
            Expanded(
                child: ListView(
              padding: EdgeInsets.symmetric(horizontal: 20, vertical: 10),
              children: [
                Text(
                  "Người tạo đơn:",
                  style: TextStyle(fontWeight: FontWeight.bold, fontSize: 16),
                ),
                Text(
                  "${dataListLetter[widget.id].textNameSend}",
                  style: TextStyle(fontWeight: FontWeight.bold, fontSize: 16),
                ),
                SizedBox(
                  height: 10,
                ),
                Container(
                  width: MediaQuery.of(context).size.width,
                  height: 2,
                  color: Colors.grey[400],
                ),
                SizedBox(
                  height: 10,
                ),
                Text(
                  "Loại đơn:",
                  style: TextStyle(fontWeight: FontWeight.bold, fontSize: 16),
                ),
                Text(
                  "${dataListLetter[widget.id].textTitle}",
                  style: TextStyle(fontWeight: FontWeight.bold, fontSize: 16),
                ),
                SizedBox(
                  height: 10,
                ),
                Container(
                  width: MediaQuery.of(context).size.width,
                  height: 2,
                  color: Colors.grey[400],
                ),
                SizedBox(
                  height: 10,
                ),
                Text(
                  "Ngày nộp đơn:",
                  style: TextStyle(fontWeight: FontWeight.bold, fontSize: 16),
                ),
                Text(
                  "${dataListLetter[widget.id].textFullDate}",
                  style: TextStyle(fontWeight: FontWeight.bold, fontSize: 16),
                ),
                SizedBox(
                  height: 10,
                ),
                Container(
                  width: MediaQuery.of(context).size.width,
                  height: 2,
                  color: Colors.grey[400],
                ),
                SizedBox(
                  height: 10,
                ),
                Text(
                  "Thời gian:",
                  style: TextStyle(fontWeight: FontWeight.bold, fontSize: 16),
                ),
                Text(
                  dataListLetter[widget.id].textDate +
                      ": " +
                      "Ca hành chính ${dataListLetter[widget.id].textFullDate}" +
                      " lý do: \n${dataListLetter[widget.id].textReason}",
                  style: TextStyle(fontWeight: FontWeight.bold, fontSize: 16),
                ),
                SizedBox(
                  height: 10,
                ),
                Container(
                  width: MediaQuery.of(context).size.width,
                  height: 2,
                  color: Colors.grey[400],
                ),
                SizedBox(
                  height: 10,
                ),
                Text(
                  "Ghi chú:",
                  style: TextStyle(fontWeight: FontWeight.bold, fontSize: 16),
                ),
                Text(
                  "${dataListLetter[widget.id].textNameLetter}",
                  style: TextStyle(fontWeight: FontWeight.bold, fontSize: 16),
                ),
                SizedBox(
                  height: 10,
                ),
                Container(
                  width: MediaQuery.of(context).size.width,
                  height: 2,
                  color: Colors.grey[400],
                ),
                SizedBox(
                  height: 10,
                ),
                Text(
                  "Trạng thái phê duyệt:",
                  style: TextStyle(fontWeight: FontWeight.bold, fontSize: 16),
                ),
                Text(
                  "${dataListLetter[widget.id].textStatusWait}",
                  style: TextStyle(
                      fontWeight: FontWeight.bold,
                      fontSize: 16,
                      color: Colors.orange),
                ),
                SizedBox(
                  height: 10,
                ),
                Container(
                  width: MediaQuery.of(context).size.width,
                  height: 2,
                  color: Colors.grey[400],
                ),
                SizedBox(
                  height: 10,
                ),
                Text(
                  "Người duyệt:",
                  style: TextStyle(fontWeight: FontWeight.bold, fontSize: 16),
                ),
                Text(
                  "${dataListLetter[widget.id].textApproved}",
                  style: TextStyle(fontWeight: FontWeight.bold, fontSize: 16),
                ),
                SizedBox(
                  height: 10,
                ),
                Container(
                  width: MediaQuery.of(context).size.width,
                  height: 2,
                  color: Colors.grey[400],
                ),
                SizedBox(
                  height: 10,
                ),
                Text(
                  "Người theo dõi:",
                  style: TextStyle(fontWeight: FontWeight.bold, fontSize: 16),
                ),
                Text(
                  "${dataListLetter[widget.id].textFollow}",
                  style: TextStyle(fontWeight: FontWeight.bold, fontSize: 16),
                ),
                SizedBox(
                  height: 10,
                ),
                Container(
                  width: MediaQuery.of(context).size.width,
                  height: 2,
                  color: Colors.grey[400],
                ),
              ],
            ))
          ],
        ),
      ),
    );
  }
}

Widget _buildAppbar() => WidgetDetailLetterPageAppbar();
