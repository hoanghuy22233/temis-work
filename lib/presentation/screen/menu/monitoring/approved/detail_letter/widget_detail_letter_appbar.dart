import 'package:base_code_project/presentation/common_widgets/barrel_common_widgets.dart';
import 'package:base_code_project/presentation/common_widgets/widget_appbar_detail_letter_page.dart';
import 'package:base_code_project/utils/locale/app_localization.dart';
import 'package:flutter/material.dart';

class WidgetDetailLetterPageAppbar extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: WidgetAppbarDetailLetterPage(
        title: AppLocalizations.of(context).translate('detail_letter_page.appbar').toUpperCase(),
        left: [WidgetAppbarMenuBack()],
        // right: [WidgetAppbarHomeBell()],
      ),
    );
  }
}
