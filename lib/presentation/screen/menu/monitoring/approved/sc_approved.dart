
import 'package:base_code_project/presentation/screen/menu/monitoring/approved/widget_approved_page_appbar.dart';
import 'package:base_code_project/presentation/screen/menu/monitoring/approved/widget_finish_approved.dart';
import 'package:base_code_project/presentation/screen/menu/monitoring/approved/widget_wait_approved.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:base_code_project/app/constants/color/color.dart';
import 'package:base_code_project/app/constants/style/style.dart';
import 'package:base_code_project/app/constants/value/value.dart';

class ApprovedPageScreen extends StatefulWidget {
  final int id;
  ApprovedPageScreen({Key key, this.id}) : super(key: key);

  @override
  _ApprovedPageScreenState createState() => _ApprovedPageScreenState();
}

class _ApprovedPageScreenState extends State<ApprovedPageScreen>
    with SingleTickerProviderStateMixin {
  final PageController _pageController = PageController(initialPage: 0);
  TabController _tabController;
  @override
  void initState() {
    super.initState();
    _tabController = new TabController(vsync: this, length: 2);
    _tabController.addListener(() {
      print('my index is' + _tabController.index.toString());
    });
  }

  @override
  void dispose() {
    _tabController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      top: true,
      child: Scaffold(
          body: Container(
        color: Colors.grey[100],
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Stack(
              children: [
                // Container(
                //   color: Colors.grey[100],
                //   // height: MediaQuery.of(context).size.height * 0.1,
                //   child: _buildAppbar(),
                // ),
                _buildAppbar(),
                _buildTabBarMenu(),
                //_buildTabBar()
              ],
            ),
            Expanded(
                child: Padding(
              padding: const EdgeInsets.all(AppValue.APP_HORIZONTAL_PADDING),
              child: TabBarView(
                controller: _tabController,
                children: [
                  WidgetWaitApproved(
                  ),
                  WidgetFinishApproved(),


                ],
              ),
            )),

          ],
        ),
      )),
    );
  }

  Widget _buildAppbar() => WidgetApprovedpPageAppbar();

  Widget _buildTabBarMenu() {
    return new Container(
      width: MediaQuery.of(context).size.width,
      margin: EdgeInsets.only(top: 45, left:0, right:0),
      color: Colors.white,
      height: AppValue.ACTION_BAR_HEIGHT,
      child: new TabBar(
        controller: _tabController,
        labelPadding: EdgeInsets.symmetric(
          horizontal: 10,
        ),
        tabs: [
          Container(
            constraints: BoxConstraints(
                minWidth: MediaQuery.of(context).size.width / 2),
            alignment: Alignment.center,
            child: Text(
              'ĐƠN CHỜ DUYỆT',
              textAlign: TextAlign.center,
            ),
          ),
          Container(
            constraints: BoxConstraints(
                minWidth: MediaQuery.of(context).size.width / 2),
            alignment: Alignment.center,
            child: Text(
              'ĐƠN ĐÃ DUYỆT',
              textAlign: TextAlign.center,
            ),
          ),


        ],
        labelStyle: AppStyle.DEFAULT_SMALL_BOLD,
        unselectedLabelStyle: AppStyle.DEFAULT_SMALL,
        labelColor: Colors.blue,
        unselectedLabelColor: AppColor.BLACK,
        physics: BouncingScrollPhysics(),
        indicatorWeight: 3,
        isScrollable: true,
        indicatorPadding: EdgeInsets.symmetric(horizontal: 10),
        indicatorColor: Colors.blue,
        // indicator: new BubbleTabIndicator(
        //   indicatorHeight: AppValue.ACTION_BAR_HEIGHT - 10,
        //   indicatorColor: Colors.white,
        //   tabBarIndicatorSize: TabBarIndicatorSize.tab,
        //   indicatorRadius: 50,
        // ),

      ),
    );
  }
}
