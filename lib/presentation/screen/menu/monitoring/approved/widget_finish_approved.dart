import 'package:base_code_project/presentation/screen/menu/monitoring/approved/seach_date_approved/sc_list_date_approved.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:modal_bottom_sheet/modal_bottom_sheet.dart';
import 'confirm_dialog_wait_approved/sc_confirm_dialog_wait_approved.dart';
import 'list_finish_approved/sc_list_finish_approved.dart';

class WidgetFinishApproved extends StatefulWidget {
  const WidgetFinishApproved({
    Key key,
  }) : super(key: key);

  @override
  _WidgetFinishApprovedState createState() => _WidgetFinishApprovedState();
}

class _WidgetFinishApprovedState extends State<WidgetFinishApproved>
    with SingleTickerProviderStateMixin {
  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        width: MediaQuery.of(context).size.width,
        height: MediaQuery.of(context).size.height,
        child: Column(
          // crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            // SizedBox(
            //   height: 10,
            // ),
            // Center(
            //   child: Text(
            //     "Không có thông tin đơn!",
            //     style: TextStyle(fontWeight: FontWeight.bold),
            //   ),
            // ),
            Expanded(
                flex: 9,
                child: ListFinishApprovedScreen()),
            Expanded(
              flex: 1,
              child: Column(
                children: [
                  Container(
                    width: MediaQuery.of(context).size.width,
                    height: 2,
                    color: Colors.grey[400],
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Row(
                    children: [
                      Expanded(
                        flex: 2,
                        // child: Container(),
                        child: Padding(
                          padding: const EdgeInsets.only(bottom: 5, left: 20),
                          child: GestureDetector(
                            onTap: () {
                              return showDialog(
                                  context: context,
                                  builder: (BuildContext context) {
                                    return FilterDialogWaitApprovedPageScreen();
                                  });
                            },
                            child: Image.asset(
                              "assets/images/ic_fillter.png",
                              width: 25,
                              height: 25,
                              color: Colors.blue,
                            ),
                          ),
                        ),
                      ),
                      Expanded(
                        flex: 6,
                        child: Padding(
                            padding: const EdgeInsets.only(
                              bottom: 5,
                            ),
                            child: Center(
                                child: Text(
                              "Tháng này",
                              style: TextStyle(
                                  fontWeight: FontWeight.bold, fontSize: 14),
                            ))),

                        // child: Padding(
                        //   padding: const EdgeInsets.only(bottom: 20, left: 10),
                        //   // child: Container(
                        //   // ),
                        // ),
                      ),
                      Expanded(
                        flex: 2,
                        child: Padding(
                          padding: const EdgeInsets.only(bottom: 5, right: 20),
                          child: GestureDetector(
                            onTap: () {
                              showMaterialModalBottomSheet(
                                shape: RoundedRectangleBorder(
                                  // borderRadius: BorderRadius.circular(10.0),
                                  borderRadius: BorderRadius.only(
                                      topLeft: Radius.circular(15.0),
                                      topRight: Radius.circular(15.0)),
                                ),
                                backgroundColor: Colors.white,
                                context: context,
                                builder: (context) =>
                                    ListDateApprovedPageScreen(),
                              );
                            },
                            child: Image.asset(
                              "assets/images/ic_list.png",
                              width: 25,
                              height: 25,
                              color: Colors.blue,
                            ),
                          ),
                        ),
                      )
                    ],
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
  // _openRingtoneDialog() {
  //   showDialog(
  //       context: context,
  //       builder: (context) => SingleChoiceConfirmationDialog<String>(
  //           title: Text('Phone ringtone'),
  //           initialValue: _ringTone,
  //           items: _ringTones,
  //           onSelected: _onSelected,
  //           onSubmitted: _onSubmitted)
  //   );
  // }
  // _openColorDialog() {
  //   showDialog(
  //     context: context,
  //     builder: (context) => SingleChoiceConfirmationDialog<Color>(
  //       title: Text('Color'),
  //       initialValue: _color,
  //       items: _colors,
  //       contentPadding: EdgeInsets.symmetric(vertical: 16.0),
  //       divider: Container(
  //         height: 1.0,
  //         color: Colors.blue,
  //       ),
  //       onSubmitted: (Color color) {
  //         setState(() => _color = color);
  //       },
  //       itemBuilder: (Color color) =>
  //           Container(height: 100.0, color: color),
  //     ),
  //   );
  // }

}
