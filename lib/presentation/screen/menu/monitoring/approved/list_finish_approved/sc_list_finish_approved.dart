import 'package:base_code_project/model/date_not_api/data_not_api_list_detail_letter.dart';
import 'package:flutter/material.dart';

class ListFinishApprovedScreen extends StatefulWidget {
  @override
  _ListFinishApprovedScreenState createState() =>
      _ListFinishApprovedScreenState();
}

class _ListFinishApprovedScreenState extends State<ListFinishApprovedScreen> {
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Expanded(
            child: ListView.builder(
          itemCount: dataListLetter.length,
          itemBuilder: (context, index) {
            return Padding(
              padding: const EdgeInsets.symmetric(horizontal: 10, vertical: 2),
              child: Container(
                width: MediaQuery.of(context).size.width,
                height: MediaQuery.of(context).size.height / 3,
                decoration: BoxDecoration(
                  border: Border.all(color: Colors.grey[300]),
                  borderRadius: BorderRadius.all(Radius.circular(
                          5.0) //                 <--- border radius here
                      ),
                ),
                // color: Colors.blue,
                child: (
                    Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    Container(
                        height: MediaQuery.of(context).size.height / 18,
                        width: MediaQuery.of(context).size.width,
                        color: Colors.grey[200],
                        child: Padding(
                          padding: const EdgeInsets.symmetric(
                              horizontal: 10, vertical: 8),
                          child: Text(
                            "${dataListLetter[index].textTitle}",
                            style: TextStyle(fontWeight: FontWeight.bold),
                          ),
                        )),
                    Padding(
                      padding: EdgeInsets.only(left: 10, top: 15),
                      child: Row(
                        children: [
                          Image.asset(
                            "assets/images/ic_user.png",
                            width: 15,
                            height: 15,
                          ),
                          SizedBox(
                            width: 10,
                          ),
                          Text("Người gửi: ${dataListLetter[index].textNameSend}"),
                        ],
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.only(left: 10, top: 5),
                      child: Row(
                        children: [
                          Image.asset(
                            "assets/images/ic_clock.png",
                            width: 15,
                            height: 15,
                          ),
                          SizedBox(
                            width: 10,
                          ),
                          Text("Thời gian:"),
                        ],
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.only(
                        left: 40,
                      ),
                      child: Text("${dataListLetter[index].textDate} ${dataListLetter[index].textFullDate} lý do : ${dataListLetter[index].textReason}"),
                    ),
                    Padding(
                      padding: EdgeInsets.only(left: 10, top: 5),
                      child: Row(
                        children: [
                          Image.asset(
                            "assets/images/ic_user.png",
                            width: 15,
                            height: 15,
                          ),
                          SizedBox(
                            width: 10,
                          ),
                          Text("Người duyệt: ${dataListLetter[index].textApproved}"),
                        ],
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.only(left: 10, top: 5),
                      child: Row(
                        children: [
                          Image.asset(
                            "assets/images/ic_exclamation.png",
                            width: 15,
                            height: 15,
                          ),
                          SizedBox(
                            width: 10,
                          ),
                          Text("${dataListLetter[index].textNameLetter}"),
                        ],
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.only(left: 10, top: 5),
                      child: Row(
                        children: [
                          Image.asset(
                            "assets/images/ic_checked.png",
                            width: 15,
                            height: 15 ,
                          ),
                          SizedBox(
                            width: 10,
                          ),
                          Text("Trạng thái: "),
                          Text(
                            "${dataListLetter[index].textStatusRemove}",
                            style: TextStyle(color: Colors.red),
                          ),
                        ],
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.only(left: 10, top: 10),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.end,
                        children: [
                          Image.asset(
                            "assets/images/delete_red.png",
                            width: 20,
                            height: 20,
                          ),
                          SizedBox(
                            width: 5,
                          ),
                          Text(
                            "HỦY DUYỆT",
                          ),
                          SizedBox(
                            width: 10,
                          ),
                        ],
                      ),
                    ),
                  ],
                )),
              ),
            );
          },
          // child: Text(""),
        ))
      ],
    );
  }
}
