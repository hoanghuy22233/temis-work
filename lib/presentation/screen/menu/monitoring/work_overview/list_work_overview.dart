import 'package:base_code_project/model/date_not_api/data_not_api_work_overview.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:modal_bottom_sheet/modal_bottom_sheet.dart';

class ChildListWorkOverview extends StatefulWidget {
  final int id;
  ChildListWorkOverview({Key key, this.id}) : super(key: key);

  @override
  _ChildListWorkOverviewState createState() => _ChildListWorkOverviewState();
}

class _ChildListWorkOverviewState extends State<ChildListWorkOverview> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      // backgroundColor: Colors.white,
      body: GestureDetector(
        // onTap: () {
        //   if (dataWorkOverView[widget.id].id == 0 && widget.id == 0) {
        //     showMaterialModalBottomSheet(
        //       shape: RoundedRectangleBorder(
        //         // borderRadius: BorderRadius.circular(10.0),
        //         borderRadius: BorderRadius.only(
        //             topLeft: Radius.circular(15.0),
        //             topRight: Radius.circular(15.0)),
        //       ),
        //       backgroundColor: Colors.white,
        //       context: context,
        //       builder: (context) => MonitoringPageScreen(),
        //     );
        //   } else if (dataWorkOverView[widget.id].id == 1 && widget.id == 1) {
        //     return (Navigator.push(
        //       context,
        //       MaterialPageRoute(builder: (context) => ScheduleTwoPageScreen()),
        //     ));
        //   } else if (dataWorkOverView[widget.id].id == 2 && widget.id == 2) {
        //     return (showMaterialModalBottomSheet(
        //       shape: RoundedRectangleBorder(
        //         // borderRadius: BorderRadius.circular(10.0),
        //         borderRadius: BorderRadius.only(
        //             topLeft: Radius.circular(15.0),
        //             topRight: Radius.circular(15.0)),
        //       ),
        //       backgroundColor: Colors.white,
        //       context: context,
        //       builder: (context) => WorkPageScreen(),
        //     ));
        //   } else if (dataWorkOverView[widget.id].id == 3 && widget.id == 3) {
        //     return (showMaterialModalBottomSheet(
        //       shape: RoundedRectangleBorder(
        //         // borderRadius: BorderRadius.circular(10.0),
        //         borderRadius: BorderRadius.only(
        //             topLeft: Radius.circular(15.0),
        //             topRight: Radius.circular(15.0)),
        //       ),
        //       backgroundColor: Colors.white,
        //       context: context,
        //       builder: (context) => OfficePageScreen(),
        //     ));
        //   } else if (dataWorkOverView[widget.id].id == 4 && widget.id == 4) {
        //     return (showMaterialModalBottomSheet(
        //       shape: RoundedRectangleBorder(
        //         // borderRadius: BorderRadius.circular(10.0),
        //         borderRadius: BorderRadius.only(
        //             topLeft: Radius.circular(15.0),
        //             topRight: Radius.circular(15.0)),
        //       ),
        //       backgroundColor: Colors.white,
        //       context: context,
        //       builder: (context) => CrmPageScreen(),
        //     ));
        //   }
          // else if (dataWorkOverView[widget.id].id == 5 && widget.id == 5) {
          //   return (
          //       Navigator.push(
          //     context,
          //     MaterialPageRoute(builder: (context) => HrmPageScreen()),
          //   ));
          // }
        // },
        child: Stack(
          children: [
            Container(
              height: MediaQuery.of(context).size.height / 3,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.all(Radius.circular(10.0)),
                color: dataWorkOverView[widget.id].backgroundColor,

                // color: Colors.white,
                // boxShadow: [
                //   BoxShadow(
                //       color: Colors.black.withAlpha(100), blurRadius: 10.0),
                // ]
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(left: 20, top: 10),
              child: Column(
                children: [
                  Row(
                    children: [
                      Text(
                        "${dataWorkOverView[widget.id].title}",
                        style: TextStyle(fontWeight: FontWeight.bold, fontSize: 16),
                      ),
                    ],
                  ),
                  SizedBox(height: 15,),
                  Row(
                    children: [
                      Expanded(flex: 2,child: Text("${dataWorkOverView[widget.id].textDivide} ${dataWorkOverView[widget.id].textWork}")),
                    ],
                  ),
                  SizedBox(height: 5,),
                  Row(
                    children: [
                      Expanded(flex: 2,child: Text("${dataWorkOverView[widget.id].textPercent} ${dataWorkOverView[widget.id].textWork}")),
                    ],
                  )
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
