import 'package:base_code_project/presentation/screen/menu/monitoring/work_overview/seach_filter_work_overview/list_seach_filter_work_overview.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class ListWorkOverviewPageScreen extends StatefulWidget {
  ListWorkOverviewPageScreen({Key key, this.title}) : super(key: key);
  final String title;

  @override
  _ListWorkOverviewPageScreenState createState() => _ListWorkOverviewPageScreenState();
}

class _ListWorkOverviewPageScreenState extends State<ListWorkOverviewPageScreen> {


  @override
  Widget build(BuildContext context) {
    return Container(
      width: MediaQuery.of(context).size.width,
      height: MediaQuery.of(context).size.height/5 ,
      child: (
          Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Expanded(
            child: Padding(
              padding:
              const EdgeInsets.symmetric(horizontal: 15, vertical: 5),
              child:  WidgetFilterWorkOverView(),
            ),
          ),
        ],
      )),
    );
  }
}
