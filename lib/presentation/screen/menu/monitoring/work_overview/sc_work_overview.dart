import 'package:base_code_project/app/constants/navigator/navigator.dart';
import 'package:base_code_project/model/date_not_api/data_not_api_work_overview.dart';
import 'package:base_code_project/presentation/common_widgets/widget_appbar_work_overview.dart';
import 'package:base_code_project/presentation/screen/menu/monitoring/work_overview/list_work_overview.dart';
import 'package:base_code_project/presentation/screen/menu/monitoring/work_overview/seach_filter_work_overview/sc_seach_filter_work_overview.dart';
import 'package:base_code_project/presentation/screen/menu/monitoring/work_overview/seach_list_work_overview/sc_seach_list_work_overview.dart';
import 'package:flutter/material.dart';
import 'package:modal_bottom_sheet/modal_bottom_sheet.dart';

class WorkOverviewPageScreen extends StatefulWidget {
  @override
  WorkOverviewPageScreenState createState() => WorkOverviewPageScreenState();
}

class WorkOverviewPageScreenState extends State<WorkOverviewPageScreen> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      top: true,
      child: Scaffold(
        body: Column(
          children: [
            _buildAppbar(),
            Expanded(
              flex: 9,
                child: ListView.builder(
                  itemCount: dataWorkOverView.length,
                  itemBuilder: (context, index) {
                    return Container(
                      width: MediaQuery.of(context).size.width,
                      height: MediaQuery.of(context).size.height/6,
                      // color: Colors.blue,
                      margin: EdgeInsets.symmetric(horizontal: 10,vertical: 3),
                      child: ChildListWorkOverview(
                        id: index,
                      ),
                    );
                  },
                )
            ),
            Expanded(
              flex: 1,
              child: Column(
                children: [
                  Container(
                    width: MediaQuery.of(context).size.width,
                    height: 1,
                    color: Colors.grey[400],
                  ),
                  SizedBox(height: 14,),
                  Row(
                    children: [
                      Expanded(
                        flex: 2,
                        // child: Container(),
                        child: Padding(
                          padding: const EdgeInsets.only(bottom: 5, left: 20),
                          child: GestureDetector(
                            onTap: () {
                              showMaterialModalBottomSheet(
                                shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.only(
                                      topLeft: Radius.circular(15.0),
                                      topRight: Radius.circular(15.0)),
                                ),
                                backgroundColor: Colors.white,
                                context: context,
                                builder: (context) =>
                                    ListWorkOverviewPageScreen(),
                              );
                            },
                            child: Image.asset(
                              "assets/images/ic_fillter.png",
                              width: 25,
                              height: 25,
                              color: Colors.blue,
                            ),
                          ),
                        ),
                      ),
                      Expanded(
                        flex: 6,
                        // child: Padding(
                        //     padding: const EdgeInsets.only(
                        //       bottom: 5,
                        //     ),
                        //     child: Center(
                        //         child: Text(
                        //           "Tất cả trạng thái",
                        //           style: TextStyle(
                        //               fontWeight: FontWeight.bold, fontSize: 14),
                        //         ))),

                        child: Padding(
                          padding: const EdgeInsets.only(bottom: 20, left: 10),
                          // child: Container(
                          // ),
                        ),
                      ),
                      Expanded(
                        flex: 2,
                        child: Padding(
                          padding: const EdgeInsets.only(bottom: 5, right: 20),
                          child: GestureDetector(
                            onTap: () {
                              showMaterialModalBottomSheet(
                                shape: RoundedRectangleBorder(
                                  // borderRadius: BorderRadius.circular(10.0),
                                  borderRadius: BorderRadius.only(
                                      topLeft: Radius.circular(15.0),
                                      topRight: Radius.circular(15.0)),
                                ),
                                backgroundColor: Colors.white,
                                context: context,
                                builder: (context) =>
                                    DateListWorkOverviewPageScreen(),
                              );
                            },
                            child: Image.asset(
                              "assets/images/ic_list.png",
                              width: 25,
                              height: 25,
                              color: Colors.blue,
                            ),
                          ),
                        ),
                      )
                    ],
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }

  // Widget _buildContent() {
  //   return _buildCondition();
  // }

  _buildAppbar() => WidgetAppbarWorkOverview(
    backgroundColor: Colors.blue,
    textColor: Colors.white,
    title: "Tổng quan công việc",
    titleDate: "Hoàn thành tháng này",
    right: [
      // Padding(padding: EdgeInsets.only(top:7,right: 10),
      //   child: GestureDetector(
      //     onTap: () {
      //       showMaterialModalBottomSheet(
      //         shape: RoundedRectangleBorder(
      //           borderRadius: BorderRadius.circular(10.0),
      //         ),
      //         backgroundColor: Colors.white,
      //         context: context,
      //         builder: (context) => SeachWorkPageScreen(),
      //
      //       );
      //     },
      //     child: Image.asset(
      //       "assets/images/ic_fillter.png",
      //       width: 25,
      //       height: 25,
      //       color: Colors.white,
      //     ),
      //   ),
      // )
    ],
    left: [
      Padding(
        padding: const EdgeInsets.only(bottom: 0,left: 10),
        child: GestureDetector(
          onTap: () {
            AppNavigator.navigateBack();
          },
          child: Image.asset(
            "assets/images/ic_back_two.png",
            width: 20,
            height: 20,
            color: Colors.white,
          ),
        ),
      )
    ],
  );

}
