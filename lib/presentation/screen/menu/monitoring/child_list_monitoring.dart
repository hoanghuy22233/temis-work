import 'package:base_code_project/app/constants/navigator/navigator.dart';
import 'package:base_code_project/model/date_not_api/data_not_api_monitorings.dart';
import 'package:base_code_project/presentation/screen/menu/monitoring/suggestion/sc_suggestion.dart';
import 'package:base_code_project/presentation/screen/menu/monitoring/work_overview/sc_work_overview.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import 'approved/sc_approved.dart';

class ChildListMonitoring extends StatefulWidget {
  final int id;
  ChildListMonitoring({Key key, this.id}) : super(key: key);

  @override
  _ChildListMonitoringState createState() => _ChildListMonitoringState();
}

class _ChildListMonitoringState extends State<ChildListMonitoring> {
  int tappedIndex;
  @override
  void initState() {
    super.initState();
    tappedIndex = 1;
  }
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        switch (dataMonitoring[widget.id].id)
        {
          case 0:
            return AppNavigator.navigateTimeKeeping();
            break;
          // case 1:
          //   return (Navigator.push(
          //     context,
          //     MaterialPageRoute(builder: (context) => SchedulePageScreen()),
          //   ));
          //   break;
          case 2:
            return (Navigator.push(
              context,
              MaterialPageRoute(builder: (context) => WorkOverviewPageScreen()),
            ));
            break;
          case 3:
            return (Navigator.push(
              context,
              MaterialPageRoute(builder: (context) => ApprovedPageScreen()),
            ));
            break;
          case 4:
            return (Navigator.push(
              context,
              MaterialPageRoute(builder: (context) => SuggestionPageScreen()),
            ));
            break;

        }
      },
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              Expanded(
                  flex: 1,
                  child: Image.asset(
                    "${dataMonitoring[widget.id].img}",
                    width: 20,
                    height: 20,
                  )),
              Expanded(
                  flex: 9,
                  child: Padding(
                    padding: const EdgeInsets.only(left: 30),
                    child: tappedIndex==widget.id?Text("${dataMonitoring[tappedIndex].textForm}",style: TextStyle(color: Colors.grey),):Text("${dataMonitoring[widget.id].textForm}"),
                  ))
            ],
          ),
          SizedBox(
            height: 20,
          ),
        ],
      ),

    );
  }
}
