import 'package:base_code_project/model/date_not_api/data_not_api_filter_finish_approved.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
class FilterDialogWaitSuggestionPageScreen extends StatefulWidget {
  FilterDialogWaitSuggestionPageScreen({Key key, this.title}) : super(key: key);
  final String title;

  @override
  _FilterDialogWaitSuggestionPageScreenState createState() => _FilterDialogWaitSuggestionPageScreenState();
}

class _FilterDialogWaitSuggestionPageScreenState extends State<FilterDialogWaitSuggestionPageScreen> {
  int _currentIndex;
  String _currText = '';
  @override
  void initState() {
    super.initState();
    _currentIndex = 0;
  }
  @override
  Widget build(BuildContext context) {
    return StatefulBuilder(
      builder: (context, setState2) {
        return AlertDialog(
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.all(
                    Radius.circular(20.0))),
            backgroundColor: Colors.white,
            content: Container(
              height: 370,
              width: double.minPositive,
              child: Column(
                children: [
                  Center(
                    child:Text("Chọn phòng ban",style: TextStyle(fontWeight: FontWeight.bold,fontSize: 20),) ,
                  ),
                  SizedBox(height: 10,),
                  Container(
                    width: 300,
                    height: 1,
                    color: Colors.black,
                  ),
                  ListView.builder(
                    shrinkWrap: true,
                    itemCount: dataFilterFinishApproved.length,
                    itemBuilder: (BuildContext context, int index) {
                      return RadioListTile(
                        activeColor: Colors.blue,
                        value: index,
                        groupValue: _currentIndex,
                        title: Text(dataFilterFinishApproved[index].textForm,style: TextStyle(color: _currentIndex == index ? Colors.blue : Colors.black,),),
                        onChanged: (val) {
                          setState2(() {
                            _currentIndex = val;
                            _currText = dataFilterFinishApproved[index].textForm;
                            print("Radio Tile pressed $_currText");
                          });

                        },
                      );
                    },
                  ),
                  SizedBox(height: 5,),
                  Container(
                    width: 300,
                    height: 1,
                    color: Colors.black,
                  ),
                  Row(
                    children: [
                      Expanded(
                        flex: 5,
                        child: FlatButton(
                          onPressed: () {
                            Navigator.pop(context, null);
                          },
                          child: Text('Không',style: TextStyle(color: Colors.blue,fontSize: 18),),
                        ),
                      ),
                      Expanded(
                        flex: 5,
                        child: FlatButton(
                          onPressed: () {
                            Navigator.pop(context, _currentIndex);
                          },
                          child: Text('Chọn',style: TextStyle(color: Colors.blue,fontSize: 18),),
                        ),
                      ),
                    ],
                  )
                ],
              ),
            ));
      },
    );
  }
}
