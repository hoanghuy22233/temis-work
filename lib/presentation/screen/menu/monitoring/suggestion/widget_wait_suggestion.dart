import 'package:base_code_project/presentation/screen/menu/monitoring/suggestion/seach_date_suggestion/sc_list_date_suggestion.dart';
import 'package:flutter/material.dart';
import 'package:modal_bottom_sheet/modal_bottom_sheet.dart';

import 'confirm_dialog_wait_suggestion/sc_confirm_dialog_wait_suggestion.dart';

class WidgetWaitSuggestion extends StatefulWidget {
  const WidgetWaitSuggestion({
    Key key,
  }) : super(key: key);

  @override
  _WidgetWaitSuggestionState createState() => _WidgetWaitSuggestionState();
}

class _WidgetWaitSuggestionState extends State<WidgetWaitSuggestion>
    with SingleTickerProviderStateMixin {
  final controller = PageController();
  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        width: MediaQuery.of(context).size.width,
        height: MediaQuery.of(context).size.height,
        child: Column(
          children: [
            SizedBox(height: 10,),
            Center(
              child: Text(
                "Không có thông tin đơn!",
                style: TextStyle(fontWeight: FontWeight.bold),
              ),
            ),

            Expanded(
              flex: 9,
              child: Container(),
            ),
            // Container(
            //   width: MediaQuery.of(context).size.width,
            //   height: 2,
            //   color: Colors.grey[400],
            // ),
            Expanded(
              flex: 1,
              child: Column(
                children: [
                  Container(
                    width: MediaQuery.of(context).size.width,
                    height: 2,
                    color: Colors.grey[400],
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Row(
                    children: [
                      Expanded(
                        flex: 2,
                        // child: Container(),
                        child:  Padding(
                          padding: const EdgeInsets.only(bottom: 5,left: 20),
                          child: GestureDetector(
                            onTap: () {
                              return showDialog(
                                  context: context,
                                  builder: (BuildContext context) {
                                    return FilterDialogWaitSuggestionPageScreen();
                                  });
                            },
                            child: Image.asset(
                              "assets/images/ic_fillter.png",
                              width: 25,
                              height: 25,
                              color: Colors.blue,
                            ),
                          ),
                        ),
                      ),
                      Expanded(
                        flex: 6,
                        // child: Container(),
                        child:  Padding(
                          padding: const EdgeInsets.only(bottom: 5,),
                          child: Center(child: Text("Tháng này",style: TextStyle(fontWeight: FontWeight.bold,fontSize: 14),))
                        ),
                      ),
                      Expanded(
                        flex: 2,
                        child: Padding(
                          padding: const EdgeInsets.only(bottom: 5, right: 20),
                          child: GestureDetector(
                            onTap: () {
                              showMaterialModalBottomSheet(
                                shape: RoundedRectangleBorder(
                                  // borderRadius: BorderRadius.circular(10.0),
                                  borderRadius: BorderRadius.only(
                                      topLeft: Radius.circular(15.0),
                                      topRight: Radius.circular(15.0)),
                                ),
                                backgroundColor: Colors.white,
                                context: context,
                                builder: (context) =>
                                    ListDateSuggestionPageScreen(),
                              );
                            },
                            child: Image.asset(
                              "assets/images/ic_list.png",
                              width: 25,
                              height: 25,
                              color: Colors.blue,
                            ),
                          ),
                        ),
                      )
                    ],
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
