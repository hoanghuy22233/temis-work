import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'list_date_suggestion.dart';

class ListDateSuggestionPageScreen extends StatefulWidget {
  ListDateSuggestionPageScreen({Key key, this.title}) : super(key: key);
  final String title;

  @override
  _ListDateSuggestionPageScreenState createState() => _ListDateSuggestionPageScreenState();
}

class _ListDateSuggestionPageScreenState extends State<ListDateSuggestionPageScreen> {
  @override
  Widget build(BuildContext context) {
    return Container(
        width: MediaQuery.of(context).size.width,
        height: MediaQuery.of(context).size.height / 1.8,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Expanded(
              child: Padding(
                padding:
                    const EdgeInsets.symmetric(horizontal: 15, ),
                child: WidgetListDateSuggestion(),
              ),
            ),
          ],
        ));
  }
}
