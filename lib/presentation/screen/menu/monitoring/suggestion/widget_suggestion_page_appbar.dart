import 'package:base_code_project/presentation/common_widgets/barrel_common_widgets.dart';
import 'package:base_code_project/presentation/common_widgets/widget_appbar_approved_page.dart';
import 'package:base_code_project/presentation/common_widgets/widget_appbar_suggestion_page.dart';
import 'package:base_code_project/utils/locale/app_localization.dart';
import 'package:flutter/material.dart';

class WidgetSuggestionPageAppbar extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: WidgetAppbarSuggestionPage(
        title: AppLocalizations.of(context).translate('suggestion_page.appbar').toUpperCase(),
        left: [WidgetAppbarMenuBack()],
        // right: [WidgetAppbarHomeBell()],
      ),
    );
  }
}
