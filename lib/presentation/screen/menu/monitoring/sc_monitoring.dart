import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'list_monitoring.dart';

class MonitoringPageScreen extends StatefulWidget {
  MonitoringPageScreen({Key key, this.title}) : super(key: key);
  final String title;

  @override
  MonitoringPageScreenState createState() => MonitoringPageScreenState();
}

class MonitoringPageScreenState extends State<MonitoringPageScreen> {
  @override
  Widget build(BuildContext context) {
    return Container(
      width: MediaQuery.of(context).size.width,
      height: MediaQuery.of(context).size.height/2.2 ,
      child: (Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Expanded(
              flex: 1,
              child: Container(
                padding: EdgeInsets.only(left: 23, top: 15),
                child: Text(
                  "GIÁM SÁT",
                  style: TextStyle(
                      fontWeight: FontWeight.bold,
                      fontSize: 16,
                      color: Colors.grey),
                ),
              )),
          Expanded(
            flex: 8,
            child: Padding(
              padding: const EdgeInsets.symmetric(horizontal: 15, ),
              child: WidgetListMonitoring(),
            ),
          ),
        ],
      )),
    );
  }
}
