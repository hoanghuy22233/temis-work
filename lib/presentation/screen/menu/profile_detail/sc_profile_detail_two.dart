import 'package:base_code_project/app/constants/endpoint/app_endpoint.dart';
import 'package:base_code_project/app/constants/navigator/navigator.dart';
import 'package:base_code_project/model/date_not_api/data_not_api_profile_customer.dart';
import 'package:base_code_project/presentation/common_widgets/widget_circle_avatar.dart';
import 'package:base_code_project/presentation/screen/menu/profile_detail/avatar_two/ic_avatar_two.dart';
import 'package:base_code_project/presentation/screen/menu/profile_detail/widget_profile_detail_appbar.dart';
import 'package:flutter/material.dart';

import 'background_image_two/ic_background_image_two.dart';
class ProfileDetailTwoPageScreen extends StatefulWidget {
  @override
  _ProfileDetailTwoPageScreenState createState() => _ProfileDetailTwoPageScreenState();
}

class _ProfileDetailTwoPageScreenState extends State<ProfileDetailTwoPageScreen> {
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      top: true,
      child: Scaffold(
          body: Column(
            children: [
              // _buildAppbar(),
              Container(
                child:   Stack(
                  children: [
                    Column(
                      children: [
                        Stack(
                          children: [
                            Container(
                                width: MediaQuery.of(context).size.width,
                                height: MediaQuery.of(context).size.height/5,
                                // color: Colors.grey,
                                child:  Image.asset("assets/images/anhnen.jpg", fit: BoxFit.cover)
                            ),
                            Positioned(
                              bottom: 10,
                              right: 10,
                              child: GestureDetector(
                                onTap: () {
                                  Navigator.push(
                                    context,
                                    MaterialPageRoute(builder: (context) => BackgroundImagePageScreen()),
                                  );
                                },
                                child: Container(
                                  width: 40,
                                  height: 40,
                                  child:  ClipRRect(
                                    borderRadius: BorderRadius.circular(500),
                                    child: Container(
                                      decoration: BoxDecoration(
                                          color: Color(0xFFD6D5D5),
                                          borderRadius:BorderRadius.circular(500),
                                          border: Border.all(color: Colors.white, width: 3)
                                      ),
                                      child: Padding(
                                        padding:  EdgeInsets.all(6.0),
                                        child: AspectRatio(
                                          aspectRatio: 1,
                                          child: Image.asset(
                                            'assets/images/img_camera2.png',
                                            fit: BoxFit.fitWidth,
                                          ),
                                        ),
                                      ),
                                    ),
                                  ),
                                ),
                              ),
                            ),

                            Positioned(top: 50,left: 5,child: GestureDetector(onTap: () {
                              AppNavigator.navigateBack();
                            },child: Image.asset('assets/images/ic_back_two.png', color: Colors.white,width: 20,height: 20,)),)
                          ],
                        ),
                      ],
                    ),
                    Padding(
                      padding: EdgeInsets.only(top: 80,left: 120),
                      child: Stack(
                        children: [
                          Container(
                            width: 90,
                            height: 90,
                            child: ClipRRect(
                              borderRadius: BorderRadius.circular(300.0),
                              child: Image.asset(dataProfileCustomer[0].img,
                                  fit: BoxFit.cover
                                // width: 30,
                                // height: 30,
                              ),
                            ),
                          ),
                          Padding(
                            padding: EdgeInsets.only(top: 60,left: 60),
                            child: GestureDetector(
                              onTap: () {
                                Navigator.push(
                                  context,
                                  MaterialPageRoute(builder: (context) => AvatarTwoPageScreen()),
                                );
                              },
                              child: Container(
                                width: 35,
                                height: 35,
                                child:  ClipRRect(
                                  borderRadius: BorderRadius.circular(500),
                                  child: Container(
                                    decoration: BoxDecoration(
                                        color: Color(0xFFD6D5D5),
                                        borderRadius:BorderRadius.circular(500),
                                        border: Border.all(color: Colors.white, width: 3)
                                    ),
                                    child: Padding(
                                      padding:  EdgeInsets.all(6.0),
                                      child: AspectRatio(
                                        aspectRatio: 1,
                                        child: Image.asset(
                                          'assets/images/img_camera2.png',
                                          fit: BoxFit.fitWidth,
                                        ),
                                      ),
                                    ),
                                  ),
                                ),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),

              SizedBox(height: 30,),

              Padding(padding: EdgeInsets.symmetric(horizontal: 10),
                child: Row(
                   mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    Text("Họ và tên",style: TextStyle(fontSize: 15,fontWeight: FontWeight.bold),),
                  ],
                ),
              ),
              Padding(padding: EdgeInsets.symmetric(horizontal: 10,vertical: 5),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    Text("huyle",style: TextStyle(fontSize: 15,),),
                  ],
                ),
              ),
              SizedBox(height: 15,),
              Padding(padding: EdgeInsets.symmetric(horizontal: 10),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    Text("Số điện thoại",style: TextStyle(fontSize: 15,fontWeight: FontWeight.bold),),
                  ],
                ),
              ),
              Padding(padding: EdgeInsets.symmetric(horizontal: 10,vertical: 5),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    Text("098317376473",style: TextStyle(fontSize: 15,),),
                  ],
                ),
              ),
              SizedBox(height: 15,),
              Padding(padding: EdgeInsets.symmetric(horizontal: 10),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    Text("Email",style: TextStyle(fontSize: 15,fontWeight: FontWeight.bold),),
                  ],
                ),
              ),
              Padding(padding: EdgeInsets.symmetric(horizontal: 10,vertical: 5),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    Text("letuanlong98@gmail.com",style: TextStyle(fontSize: 15,),),
                  ],
                ),
              ),
              SizedBox(height: 15,),
              Padding(padding: EdgeInsets.symmetric(horizontal: 10),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    Text("Ngày sinh",style: TextStyle(fontSize: 15,fontWeight: FontWeight.bold),),
                  ],
                ),
              ),
              Padding(padding: EdgeInsets.symmetric(horizontal: 10,vertical: 5),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    Text("25/5/1998",style: TextStyle(fontSize: 15,),),
                  ],
                ),
              ),


              // Container(
              //   child: Text("test"),
              // )
              // Column(
              //   crossAxisAlignment: CrossAxisAlignment.start,
              //   mainAxisAlignment: MainAxisAlignment.start,
              //   children: [
              //     Text("Họ và tên",style: TextStyle(fontWeight: FontWeight.bold,fontSize: 15),)
              //   ],
              // )
            ],
          )),
    );
  }
}
Widget _buildAppbar() => WidgetProfileDetailAppbar();

