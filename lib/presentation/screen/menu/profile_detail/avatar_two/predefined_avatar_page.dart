import 'dart:io';

// import 'package:base_code_project/presentation/screen/menu/profile_detail/background_image_two/utils.dart';
// import 'package:base_code_project/presentation/screen/menu/profile_detail/background_image_two/widget/floating_button.dart';
// import 'package:base_code_project/presentation/screen/menu/profile_detail/background_image_two/widget/image_avatar_list_widget.dart';
import 'package:base_code_project/presentation/screen/menu/profile_detail/avatar_two/utils.dart';
import 'package:base_code_project/presentation/screen/menu/profile_detail/avatar_two/widget/floating_button.dart';
import 'package:base_code_project/presentation/screen/menu/profile_detail/avatar_two/widget/image_avatar_list_widget.dart';
import 'package:flutter/material.dart';
import 'package:image_cropper/image_cropper.dart';
// import 'package:image_cropper/image_cropper.dart';
// import 'package:image_cropper_example/utils.dart';
// import 'package:image_cropper_example/widget/floating_button.dart';
// import 'package:image_cropper_example/widget/image_avatar_list_widget.dart';

class PredefinedAvatarPage extends StatefulWidget {
  final bool isGallery;

  const PredefinedAvatarPage({
    Key key,
    @required this.isGallery,
  }) : super(key: key);

  @override
  _PredefinedAvatarPageState createState() => _PredefinedAvatarPageState();
}

class _PredefinedAvatarPageState extends State<PredefinedAvatarPage> {
  List<File> imageFiles = [];

  // @override
  // void initState() {
  //   super.initState(
  //       setState((){
  //
  //       })
  //   );
  // }
  @override
  Widget build(BuildContext context) => SafeArea(
    top: true,
    child: Scaffold(
      body: ImageAvatarListWidget(imageFiles: imageFiles),
      floatingActionButton: FloatingButtonWidget(onClicked: onClickedButton),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,
        ),
  );

  Future onClickedButton() async {
    final file = await Utils.pickMedia(
      isGallery: widget.isGallery,
      cropImage: cropPredefinedImage,
    );

    if (file == null) return;
    setState(() => imageFiles.add(file));
  }

  Future<File> cropPredefinedImage(File imageFile) async =>
      await ImageCropper.cropImage(
        sourcePath: imageFile.path,
        aspectRatioPresets: [
          CropAspectRatioPreset.original,
          CropAspectRatioPreset.square,
          CropAspectRatioPreset.ratio3x2,
          CropAspectRatioPreset.ratio4x3,
        ],
        // androidUiSettings: androidUiSettingsLocked(),
        // iosUiSettings: iosUiSettingsLocked(),
      );

  // IOSUiSettings iosUiSettingsLocked() => IOSUiSettings(
  //       aspectRatioLockEnabled: false,
  //       resetAspectRatioEnabled: false,
  //     );
  //
  // AndroidUiSettings androidUiSettingsLocked() => AndroidUiSettings(
  //       toolbarTitle: 'Crop Image',
  //       toolbarColor: Colors.red,
  //       toolbarWidgetColor: Colors.white,
  //       initAspectRatio: CropAspectRatioPreset.original,
  //       lockAspectRatio: false,
  //     );
}
