import 'dart:io';

import 'package:flutter/material.dart';

class ImageAvatarListWidget extends StatelessWidget {
  final List<File> imageFiles;

  const ImageAvatarListWidget({
    Key key,
    @required this.imageFiles,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) => GridView.count(
        physics: BouncingScrollPhysics(),
        padding: EdgeInsets.all(12),
        crossAxisCount: 2,
        crossAxisSpacing: 12,
        mainAxisSpacing: 12,
        children: imageFiles
            .map((imageFile) => ClipRRect(
                  borderRadius: BorderRadius.circular(500),
                  child: Image.file(imageFile,fit: BoxFit.cover),

                ))
            .toList(),
    // children: imageFiles
    //     .map((imageFile) => Container(
    //   child: Image.file(imageFile),
    // ))
    //     .toList(),
      );
}
