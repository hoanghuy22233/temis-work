import 'package:base_code_project/app/constants/barrel_constants.dart';
import 'package:base_code_project/presentation/screen/menu/profile_detail/avatar_two/predefined_avatar_page.dart';
import 'package:base_code_project/presentation/screen/menu/profile_detail/background_image_two/predefined_page.dart';
import 'package:flutter/material.dart';

class AvatarTwoPageScreen extends StatefulWidget {
  @override
  _AvatarTwoPageScreenState createState() =>
      _AvatarTwoPageScreenState();
}

class _AvatarTwoPageScreenState extends State<AvatarTwoPageScreen>  with SingleTickerProviderStateMixin {
  TabController controller;
  bool isGallery = true;
  int index = 2;
  final PageStorageBucket bucket = PageStorageBucket();

  @override
  void initState() {
    super.initState();
    controller = TabController(length: 1, vsync: this);
  }
  @override
  Widget build(BuildContext context) {
    return SafeArea(top: true,child: Scaffold(
      appBar: AppBar(

        leading: GestureDetector(
            onTap: () {
              AppNavigator.navigateBack();
            },child: Icon(Icons.arrow_back,color: Colors.white,)),
        backgroundColor: Colors.blue,
        title: Text("Ảnh đại diện",style: TextStyle(color: Colors.white,fontSize: 14),),
        centerTitle: false,
        actions: [
          Row(
            children: [
              Text(
                isGallery ? 'Thư viện ảnh' : 'Camera',
                style: TextStyle(fontWeight: FontWeight.bold,color: Colors.white),
              ),
              Switch(
                value: isGallery,
                onChanged: (value) => setState(() => isGallery = value),
              ),
            ],
          ),
        ],
      ),
      body:PredefinedAvatarPage(isGallery: isGallery),
    ));
  }
}
