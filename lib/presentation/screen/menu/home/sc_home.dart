
import 'package:base_code_project/model/date_not_api/data_not_api_home.dart';
import 'package:base_code_project/presentation/screen/menu/home/list_home.dart';
import 'package:base_code_project/presentation/screen/menu/home/sc_profile_customer.dart';
import 'package:base_code_project/presentation/screen/menu/home/widget_home_page_appbar.dart';
import 'package:flutter/material.dart';

class HomeFourPageScreen extends StatefulWidget {
  @override
  HomeFourPageScreenState createState() => HomeFourPageScreenState();
}

class HomeFourPageScreenState extends State<HomeFourPageScreen> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        body: Column(
          children: [
            _buildAppbar(),
            Expanded(
                child: ListView.builder(
                  itemCount: dataHome.length+1,
                  itemBuilder: (context, index) {
                    if (index == 0) {
                      return ProfileCustomerScreen();
                    }
                    return Container(
                      width: MediaQuery.of(context).size.width,
                      height: MediaQuery.of(context).size.height/8,
                      // color: Colors.blue,
                      margin: EdgeInsets.symmetric(horizontal: 10,vertical: 2),
                      child: ChildListHomeTwo(
                        id: index-1,
                      ),
                    );
                  },
                )


              // child: SingleChildScrollView(
              //   child: Container(
              //     width: MediaQuery.of(context).size.width,
              //     height: MediaQuery.of(context).size.height,
              //     child: _buildCondition(),
              //   ),
              // ),
            ),
          ],
        ),
      ),
    );
  }

  // Widget _buildContent() {
  //   return _buildCondition();
  // }

  Widget _buildAppbar() => WidgetHomePageAppbar();

}
