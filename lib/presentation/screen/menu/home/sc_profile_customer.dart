import 'package:base_code_project/model/date_not_api/data_not_api_notification.dart';
import 'package:base_code_project/model/date_not_api/data_not_api_profile_customer.dart';
import 'package:flutter/material.dart';

class ProfileCustomerScreen extends StatefulWidget {
  const ProfileCustomerScreen();

  @override
  _ProfileCustomerScreenState createState() => _ProfileCustomerScreenState();
}

class _ProfileCustomerScreenState extends State<ProfileCustomerScreen> {
  @override
  Widget build(BuildContext context) {
    // final double categoryHeight = MediaQuery.of(context).size.height * 0.30 - 50;
    return Container(
      // height: MediaQuery.of(context).size.height / 5,
      // color: Colors.redAccent,
      // margin: const EdgeInsets.symmetric(horizontal: 20, vertical: 5),
      child: Column(
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.symmetric(vertical: 20,horizontal: 10),
                child: Container(
                  width: 50,
                  height: 50,
                  child: ClipRRect(
                    borderRadius: BorderRadius.circular(300.0),
                    child: Image.asset(dataProfileCustomer[0].img,
                        fit: BoxFit.cover
                      // width: 30,
                      // height: 30,
                    ),
                  ),
                ),
              ),
              SizedBox(
                width: 10,
              ),
              Container(),
              // Padding(
              //   padding: const EdgeInsets.symmetric(vertical: 20),
              //   child: Container(
              //     width: 2,
              //     height: 130,
              //     color: Colors.grey,
              //   ),
              // ),
              SizedBox(
                width: 10,
              ),
              Expanded(
                flex: 7,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: <Widget>[
                    Text(
                      // post["textRemid"],
                      "Xin chào, ${dataProfileCustomer[0].textForm} "
                      ,
                      style: const TextStyle(
                          fontSize: 16, fontWeight: FontWeight.bold),
                    ),
                    SizedBox(
                      height: 5,
                    ),
                    Row(
                      children: [
                        Image.asset(
                          "assets/images/ic_located.png",
                          width: 20,
                          height: 20,
                          color: Colors.blue,
                        ),
                        SizedBox(
                          width: 5,
                        ),
                        Text(
                          dataProfileCustomer[0].textLocated,
                          style: const TextStyle(
                            fontSize: 14,
                          ),
                        ),
                      ],
                    ),
                    SizedBox(
                      height: 5,
                    ),
                    Row(
                      children: [
                        Image.asset(
                          "assets/images/date_off_three.png",
                          width: 20,
                          height: 20,
                        ),
                        SizedBox(
                          width: 5,
                        ),
                        Text(
                          "${dataProfileCustomer[0].textDate} ${dataProfileCustomer[0].textDateFull}",
                          style: const TextStyle(
                            fontSize: 14,
                            color: Colors.grey,
                          ),
                        ),
                      ],
                    )
                  ],
                ),
              ),
            ],
          ),
          Text(dataProfileCustomer[0].textBottom,
              style: TextStyle(
                  fontSize: 16,
                  fontWeight: FontWeight.bold,
                  color: Colors.lightGreen))
        ],
      ),
    );

  }
}
