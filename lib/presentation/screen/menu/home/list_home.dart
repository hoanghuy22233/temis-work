import 'package:base_code_project/model/date_not_api/data_not_api_home.dart';
import 'package:base_code_project/presentation/screen/menu/crm/sc_crm.dart';
import 'package:base_code_project/presentation/screen/menu/monitoring/sc_monitoring.dart';
import 'package:base_code_project/presentation/screen/menu/office/sc_office.dart';
import 'package:base_code_project/presentation/screen/menu/schedule/sc_schedule_two.dart';
import 'package:base_code_project/presentation/screen/menu/work/sc_work.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:modal_bottom_sheet/modal_bottom_sheet.dart';

class ChildListHomeTwo extends StatefulWidget {
  final int id;
  ChildListHomeTwo({Key key, this.id}) : super(key: key);

  @override
  _ChildListHomeTwoState createState() => _ChildListHomeTwoState();
}

class _ChildListHomeTwoState extends State<ChildListHomeTwo> {
  // final panelController = PanelController();
  // PanelController _panelController = new PanelController();
  // ScrollController _scrollController = new ScrollController();
  final double tabBarHeight = 80;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      // backgroundColor: Colors.white,
      body: GestureDetector(
        onTap: () {
          if (dataHome[widget.id].id == 0 && widget.id == 0) {
            showMaterialModalBottomSheet(
              shape: RoundedRectangleBorder(
                // borderRadius: BorderRadius.circular(10.0),
                borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(15.0),
                    topRight: Radius.circular(15.0)),
              ),
              backgroundColor: Colors.white,
              context: context,
              builder: (context) => MonitoringPageScreen(),
            );
          } else if (dataHome[widget.id].id == 1 && widget.id == 1) {
            return (Navigator.push(
              context,
              MaterialPageRoute(builder: (context) => ScheduleTwoPageScreen()),
            ));
          } else if (dataHome[widget.id].id == 2 && widget.id == 2) {
            return (
                showMaterialModalBottomSheet(
                  shape: RoundedRectangleBorder(
                    // borderRadius: BorderRadius.circular(10.0),
                    borderRadius: BorderRadius.only(
                        topLeft: Radius.circular(15.0),
                        topRight: Radius.circular(15.0)),
                  ),
                  backgroundColor: Colors.white,
                  context: context,
                  builder: (context) => WorkPageScreen(),

            ));
          } else if (dataHome[widget.id].id == 3 && widget.id == 3) {
            return(
                showMaterialModalBottomSheet(
                  shape: RoundedRectangleBorder(
                    // borderRadius: BorderRadius.circular(10.0),
                    borderRadius: BorderRadius.only(
                        topLeft: Radius.circular(15.0),
                        topRight: Radius.circular(15.0)),
                  ),
                  backgroundColor: Colors.white,
                  context: context,
                  builder: (context) => OfficePageScreen(),
                )
            );
            // return (Navigator.push(
            //   context,
            //   MaterialPageRoute(
            //       builder: (context) => SlidingUpPanel(
            //             controller: _panelController,
            //             maxHeight:
            //                 MediaQuery.of(context).size.height - tabBarHeight,
            //             body: HomePageScreen(),
            //             panelBuilder: (_scrollController) => OfficePageScreen(
            //               scrollController: _scrollController,
            //               // panelController: _panelController,
            //             ),
            //           )),
            // ));
          } else if (dataHome[widget.id].id == 4 && widget.id == 4) {
            return (
                showMaterialModalBottomSheet(
                  shape: RoundedRectangleBorder(
                    // borderRadius: BorderRadius.circular(10.0),
                    borderRadius: BorderRadius.only(
                        topLeft: Radius.circular(15.0),
                        topRight: Radius.circular(15.0)),
                  ),
                  backgroundColor: Colors.white,
                    context: context,
                    builder: (context) => CrmPageScreen(),
            ));
          }
          // else if (dataHome[widget.id].id == 5 && widget.id == 5) {
          //   return (
          //       Navigator.push(
          //     context,
          //     MaterialPageRoute(builder: (context) => HrmPageScreen()),
          //   ));
          // }
        },
        child: Stack(
          children: [
            Container(
              height: MediaQuery.of(context).size.height / 8,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.all(Radius.circular(10.0)),
                color: dataHome[widget.id].backgroundColor,

                // color: Colors.white,
                  // boxShadow: [
                  //   BoxShadow(
                  //       color: Colors.black.withAlpha(100), blurRadius: 10.0),
                  // ]
              ),
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Expanded(flex: 3, child: Container()),
                Expanded(
                  flex: 2,
                  child: Image.asset(
                    "${dataHome[widget.id].img}",
                    width: 45,
                    height: 45,
                    // fit: BoxFit.cover,
                  ),
                ),
                // SizedBox(height: 5,),
                SizedBox(
                  width: 5,
                ),
                Expanded(
                  flex: 3,
                  child: Text(
                    "${dataHome[widget.id].title}",
                    style: TextStyle(
                        color: Colors.white,
                        fontWeight: FontWeight.bold,
                        fontSize: 16),
                  ),
                ),
                Expanded(flex: 2, child: Container()),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
