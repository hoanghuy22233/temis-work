import 'package:base_code_project/app/constants/navigator/navigator.dart';
import 'package:base_code_project/presentation/common_widgets/widget_appbar_project.dart';
import 'package:base_code_project/presentation/screen/full_project_two/seach_date_work/sc_seach_date_work.dart';
import 'package:base_code_project/presentation/screen/menu/work/work_flow/widget_mission.dart';
import 'package:base_code_project/presentation/screen/menu/work/work_flow/widget_procedure.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:base_code_project/app/constants/color/color.dart';
import 'package:base_code_project/app/constants/style/style.dart';
import 'package:base_code_project/app/constants/value/value.dart';
import 'package:modal_bottom_sheet/modal_bottom_sheet.dart';


class WorkFlowScreen extends StatefulWidget {
  final int id;
  WorkFlowScreen({Key key, this.id}) : super(key: key);

  @override
  _WorkFlowScreenState createState() => _WorkFlowScreenState();
}

class _WorkFlowScreenState extends State<WorkFlowScreen>
    with SingleTickerProviderStateMixin {
  // final PageController _pageController = PageController(initialPage: 0);
  TabController _tabController;
  @override
  void initState() {
    super.initState();
    _tabController = new TabController(vsync: this, length: 2);
    _tabController.addListener(() {
      print('my index is' + _tabController.index.toString());
    });
  }

  @override
  void dispose() {
    _tabController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      top: true,
      child: Scaffold(
          body: Container(
        color: Colors.grey[100],
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Stack(
              children: [
                // Container(
                //   color: Colors.grey[100],
                //   // height: MediaQuery.of(context).size.height * 0.1,
                //   child: _buildAppbar(),
                // ),
                _buildAppbar(),
                _buildTabBarMenu(),
                //_buildTabBar()
              ],
            ),
            Expanded(
                child: Padding(
              padding: const EdgeInsets.all(AppValue.APP_HORIZONTAL_PADDING),
              child: TabBarView(
                controller: _tabController,
                children: [
                  WidgetProcedure(),
                  WidgetMission(
                  ),
                  // BannerPages(
                  //   drawer: _drawerKey,
                  // ),
                  // WidgetFinish(),
                ],
              ),
            )),
          ],
        ),
      )),
    );
  }
  _buildAppbar() => WidgetAppbarProject(
    backgroundColor: Colors.blue,
    textColor: Colors.white,
    title: "Quản lý quy trình",
    right: [
      Padding(padding: EdgeInsets.only(top:4,right: 10),
        child: GestureDetector(
          onTap: () {
            showMaterialModalBottomSheet(
                shape: RoundedRectangleBorder(
                  // borderRadius: BorderRadius.circular(10.0),
                  borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(15.0),
                      topRight: Radius.circular(15.0)),
                ),
                backgroundColor: Colors.white,
                context: context,
                builder: (context) => SeachDateWorkPageScreen());
          },
          child: Image.asset(
            "assets/images/ic_list.png",
            width: 25,
            height: 25,
            color: Colors.white,
          ),
        ),
      )
    ],
    left: [
      Padding(
        padding: const EdgeInsets.only(left: 10),
        child: GestureDetector(
          onTap: () {
            AppNavigator.navigateBack();
            // showMaterialModalBottomSheet(
            //   shape: RoundedRectangleBorder(
            //     borderRadius: BorderRadius.circular(10.0),
            //   ),
            //   backgroundColor: Colors.white,
            //   context: context,
            //   builder: (context) => SeachDateWorkPageScreen(),
            // );
          },
          child: Image.asset(
            "assets/images/ic_back_two.png",
            width: 20,
            height: 20,
            color: Colors.white,
          ),
        ),
      )


    ],
  );

  // _buildAppbar() => WidgetAppbarProject(
  //       backgroundColor: Colors.blue,
  //       textColor: Colors.white,
  //       title: "Quản lý quy trình",
  //         right: [
  //           Padding(padding: EdgeInsets.only(top:4,right: 10),
  //             child: GestureDetector(
  //               onTap: () {
  //                 showMaterialModalBottomSheet(
  //                     shape: RoundedRectangleBorder(
  //                     // borderRadius: BorderRadius.circular(10.0),
  //                       borderRadius: BorderRadius.only(
  //                           topLeft: Radius.circular(15.0),
  //                           topRight: Radius.circular(15.0)),
  //                 ),
  //                 backgroundColor: Colors.white,
  //                 context: context,
  //                 builder: (context) => SeachDateWorkPageScreen());
  //               },
  //               child: Image.asset(
  //                 "assets/images/ic_list.png",
  //                 width: 25,
  //                 height: 25,
  //                 color: Colors.white,
  //               ),
  //             ),
  //           )
  //         ],
  //       left: [
  //         Padding(
  //           padding: const EdgeInsets.only(bottom: 20,left: 10),
  //           child: GestureDetector(
  //             onTap: () {
  //               AppNavigator.navigateBack();
  //               // showMaterialModalBottomSheet(
  //               //   shape: RoundedRectangleBorder(
  //               //     borderRadius: BorderRadius.circular(10.0),
  //               //   ),
  //               //   backgroundColor: Colors.white,
  //               //   context: context,
  //               //   builder: (context) => SeachDateWorkPageScreen(),
  //               // );
  //             },
  //             child: Image.asset(
  //               "assets/images/ic_back_two.png",
  //               width: 20,
  //               height: 20,
  //               color: Colors.white,
  //             ),
  //           ),
  //         )
  //       ],
  //     );

  // Widget _buildTabBarMenu() {
  //   return new Container(
  //     width: MediaQuery.of(context).size.width,
  //     margin: EdgeInsets.only(top: 45, left:0, right:0),
  //     color: Colors.white,
  //     height: AppValue.ACTION_BAR_HEIGHT,
  //     child: new TabBar(
  //       controller: _tabController,
  //       labelPadding: EdgeInsets.symmetric(
  //         horizontal: 20,
  //       ),
  //       tabs: [
  //         Container(
  //           constraints: BoxConstraints(
  //               minWidth: MediaQuery.of(context).size.width /3),
  //           alignment: Alignment.center,
  //           width: 100,
  //           child: Text(
  //             'QUY TRÌNH',
  //             textAlign: TextAlign.center,
  //           ),
  //         ),
  //         Container(
  //           constraints: BoxConstraints(
  //               minWidth: MediaQuery.of(context).size.width / 3),
  //           alignment: Alignment.center,
  //           width: 100,
  //           child: Text(
  //             'NHIỆM VỤ',
  //             textAlign: TextAlign.center,
  //           ),
  //         ),
  //         // Container(
  //         //   constraints: BoxConstraints(
  //         //       minWidth: MediaQuery.of(context).size.width / 4.3),
  //         //   alignment: Alignment.center,
  //         //   width: 60,
  //         //   child: Text(
  //         //     'HOÀN THÀNH',
  //         //     textAlign: TextAlign.center,
  //         //   ),
  //         // ),
  //       ],
  //       labelStyle: AppStyle.DEFAULT_SMALL_BOLD,
  //       unselectedLabelStyle: AppStyle.DEFAULT_SMALL,
  //       labelColor: Colors.blue,
  //       unselectedLabelColor: AppColor.BLACK,
  //       physics: BouncingScrollPhysics(),
  //       indicatorWeight: 3,
  //       isScrollable: true,
  //       indicatorPadding: EdgeInsets.symmetric(horizontal: 20),
  //       indicatorColor: Colors.blue,
  //       // indicator: new BubbleTabIndicator(
  //       //   indicatorHeight: AppValue.ACTION_BAR_HEIGHT - 10,
  //       //   indicatorColor: Colors.white,
  //       //   tabBarIndicatorSize: TabBarIndicatorSize.tab,
  //       //   indicatorRadius: 50,
  //       // ),
  //
  //     ),
  //   );
  // }
  Widget _buildTabBarMenu() {
    return new Container(
      width: MediaQuery.of(context).size.width,
      margin: EdgeInsets.only(top: 45, left:0, right:0),
      color: Colors.white,
      height: AppValue.ACTION_BAR_HEIGHT,
      child: new TabBar(
        controller: _tabController,
        labelPadding: EdgeInsets.symmetric(
          horizontal: 10,
        ),
        tabs: [
          Container(
            constraints: BoxConstraints(
                minWidth: MediaQuery.of(context).size.width / 2),
            alignment: Alignment.center,
            child: Text(
              'QUY TRÌNH',
              textAlign: TextAlign.center,
            ),
          ),
          Container(
            constraints: BoxConstraints(
                minWidth: MediaQuery.of(context).size.width / 2),
            alignment: Alignment.center,
            child: Text(
              'NHIỆM VỤ',
              textAlign: TextAlign.center,
            ),
          ),


        ],
        labelStyle: AppStyle.DEFAULT_SMALL_BOLD,
        unselectedLabelStyle: AppStyle.DEFAULT_SMALL,
        labelColor: Colors.blue,
        unselectedLabelColor: AppColor.BLACK,
        physics: BouncingScrollPhysics(),
        indicatorWeight: 3,
        isScrollable: true,
        indicatorPadding: EdgeInsets.symmetric(horizontal: 10),
        indicatorColor: Colors.blue,
        // indicator: new BubbleTabIndicator(
        //   indicatorHeight: AppValue.ACTION_BAR_HEIGHT - 10,
        //   indicatorColor: Colors.white,
        //   tabBarIndicatorSize: TabBarIndicatorSize.tab,
        //   indicatorRadius: 50,
        // ),

      ),
    );
  }

}
