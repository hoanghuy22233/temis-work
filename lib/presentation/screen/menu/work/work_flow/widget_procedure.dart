
import 'package:flutter/material.dart';

class WidgetProcedure extends StatefulWidget {
  const WidgetProcedure({
    Key key,
  }) : super(key: key);

  @override
  _WidgetProcedureState createState() => _WidgetProcedureState();
}

class _WidgetProcedureState extends State<WidgetProcedure>
    with SingleTickerProviderStateMixin {
  final controller = PageController();
  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        width: MediaQuery.of(context).size.width,
        height: MediaQuery.of(context).size.height,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Container(
              width: MediaQuery.of(context).size.width,
              height: MediaQuery.of(context).size.height / 15,
              margin: EdgeInsets.symmetric(horizontal: 17),
              child: Row(
                children: [
                  Image.asset(
                    "assets/images/img_search.png",
                    width: 15,
                    height: 15,
                  ),
                  SizedBox(
                    width: 20,
                  ),
                  Expanded(
                    flex: 8,
                    child: TextField(
                      decoration: new InputDecoration.collapsed(
                          // border: InputBorder.none,
                          hintText: 'Tìm kiếm'),
                    ),
                  )
                ],
              ),
            ),
            Container(
              width: MediaQuery.of(context).size.width,
              height: 3,
              // color: Colors.grey[300],
              decoration: BoxDecoration(
                border: Border(
                  bottom: BorderSide(width: 2.0, color: Colors.grey[400]),
                ),
                boxShadow: [
                  BoxShadow(
                    color: Colors.grey.withOpacity(0.3),
                    spreadRadius: 1,
                    blurRadius: 7,
                    offset: Offset(0, 2), // changes position of shadow
                  ),
                ],
              ),
            ),
            // Center(
            //   child: Text("Không có dự án!"),
            // ),
            // Container(
            //   color: Colors.grey[350],
            //   width: MediaQuery.of(context).size.width,
            //   height: MediaQuery.of(context).size.height / 22,
            //   child: Row(
            //     children: [
            //       Container(
            //           margin: EdgeInsets.symmetric(horizontal: 17),
            //           child: Text(
            //             "Hạn hoàn thành thứ 5, 04/03/2021",
            //             style: TextStyle(fontWeight: FontWeight.bold),
            //           )),
            //     ],
            //   ),
            // ),
            // Container(
            //   width: MediaQuery.of(context).size.width,
            //   height: 1,
            //   color: Colors.grey[400],
            // ),
            Container(
              // height: MediaQuery.of(context).size.height / 2,
              child: Expanded(
                child: Stack(
                  children: [
                    Container(
                      // child: Text("test"),
                    ),
                    // ListView.builder(
                    //   scrollDirection: Axis.vertical,
                    //   // physics:  AlwaysScrollableScrollPhysics(),
                    //   itemCount: dataProjectFull.length,
                    //   itemBuilder: (context, index) {
                    //     return Container(
                    //       width: MediaQuery.of(context).size.width,
                    //       height: MediaQuery.of(context).size.height / 6,
                    //       // color: Colors.blue,
                    //       // margin: EdgeInsets.symmetric(horizontal: 10,vertical: 2),
                    //       child: ChildListProjectFullTwo(
                    //         id: index,
                    //       ),
                    //     );
                    //   },
                    // ),
                    Positioned(
                        right: 10,
                        top: 350,
                        child: Image.asset(
                          "assets/images/image_add.png",
                          width: 50,
                          height: 50,
                        )),
                  ],
                ),
              ),
            ),
            // Row(
            //   children: [
            //     Expanded(
            //       flex: 1,
            //       child: GestureDetector(
            //         onTap: () {
            //           showMaterialModalBottomSheet(
            //             shape: RoundedRectangleBorder(
            //               borderRadius: BorderRadius.circular(10.0),
            //             ),
            //             backgroundColor: Colors.white,
            //             context: context,
            //             builder: (context) => SeachWorkPageScreen(),
            //
            //           );
            //         },
            //         child: Image.asset(
            //           "assets/images/ic_fillter.png",
            //           width: 25,
            //           height: 25,
            //         ),
            //       ),
            //     ),
            //     Expanded(flex: 6, child: Container()),
            //     Expanded(
            //         flex: 1,
            //         child: GestureDetector(
            //           onTap: () {
            //             showMaterialModalBottomSheet(
            //               shape: RoundedRectangleBorder(
            //                 borderRadius: BorderRadius.circular(10.0),
            //               ),
            //               backgroundColor: Colors.white,
            //               context: context,
            //               builder: (context) => SeachDateWorkPageScreen(),
            //             );
            //           },
            //           child: Image.asset(
            //             "assets/images/ic_list.png",
            //             width: 25,
            //             height: 25,
            //           ),
            //         )),
            //   ],
            // )
          ],
        ),
      ),
    );

  }
}
