
import 'package:flutter/material.dart';

class WidgetPerform extends StatefulWidget {
  const WidgetPerform({
    Key key,
  }) : super(key: key);

  @override
  _WidgetPerform createState() => _WidgetPerform();
}

class _WidgetPerform extends State<WidgetPerform>
    with SingleTickerProviderStateMixin {
  final controller = PageController();
  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        width: MediaQuery.of(context).size.width,
        height: MediaQuery.of(context).size.height,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Container(
              width: MediaQuery.of(context).size.width,
              height: MediaQuery.of(context).size.height / 15,
              margin: EdgeInsets.symmetric(horizontal: 17),
              child: Row(
                children: [
                  Image.asset(
                    "assets/images/img_search.png",
                    width: 15,
                    height: 15,
                  ),
                  SizedBox(
                    width: 20,
                  ),
                  Expanded(
                    flex: 8,
                    child: TextField(
                      decoration: new InputDecoration.collapsed(
                          // border: InputBorder.none,
                          hintText: 'Tìm kiếm'),
                    ),

                  )
                ],
              ),
            ),
            Container(
              width: MediaQuery.of(context).size.width,
              height: 3,
              // color: Colors.grey[300],
              decoration: BoxDecoration(
                border: Border(
                  bottom: BorderSide(width: 2.0, color: Colors.grey[400]),
                ),
                boxShadow: [
                  BoxShadow(
                    color: Colors.grey.withOpacity(0.3),
                    spreadRadius: 1,
                    blurRadius: 7,
                    offset: Offset(0, 2), // changes position of shadow
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
