import 'package:base_code_project/model/date_not_api/data_not_api_work.dart';
import 'package:base_code_project/presentation/screen/menu/work/child_list_work.dart';
import 'package:flutter/material.dart';
class WidgetListWork extends StatefulWidget {
  @override
  _WidgetListWorkState createState() => _WidgetListWorkState();
}

class _WidgetListWorkState extends State<WidgetListWork> {
  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      itemCount: dataWork.length+1,
      itemBuilder: (context, index) {
        if( index == dataWork.length){
          return GestureDetector
            (
            onTap: () {
              Navigator.pop(context);
            },
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    Expanded(
                        flex: 1,
                        child: Image.asset(
                          "assets/images/delete_red.png",
                          width: 20,
                          height: 20,
                        )),
                    Expanded(
                        flex: 9,
                        child: Padding(
                          padding: const EdgeInsets.only(left: 30),
                          child: Text("Hủy"),
                        ))
                  ],
                ),
                SizedBox(
                  height: 20,
                ),
              ],
            ),
          );
        }else{
          return Container(
              // margin: EdgeInsets.symmetric(horizontal: 10),
        child: ChildListWork(
        id: index
          ,
        ));
        }

      },
    );
  }
}


