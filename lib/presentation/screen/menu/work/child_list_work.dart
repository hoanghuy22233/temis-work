import 'package:base_code_project/model/date_not_api/data_not_api_work.dart';
import 'package:base_code_project/presentation/screen/full_project_two/sc_full_project_two.dart';
import 'package:base_code_project/presentation/screen/menu/work/project/sc_project.dart';
import 'package:base_code_project/presentation/screen/menu/work/work_flow/sc_work_flow.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class ChildListWork extends StatelessWidget {
  final int id;
  ChildListWork({Key key, this.id}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        switch (dataWork[id].id)
        {
          case 0:
            return (Navigator.push(
              context,
              MaterialPageRoute(builder: (context) => FullProjectTwoScreen()),
            ));
            break;
          case 1:
            return (Navigator.push(
              context,
              MaterialPageRoute(builder: (context) => ProjectScreen()),
            ));
            break;
          case 2:
            return (Navigator.push(
              context,
              MaterialPageRoute(builder: (context) => WorkFlowScreen()),
            ));
            break;

        }
      },
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              Expanded(
                  flex: 1,
                  child: Image.asset(
                    "${dataWork[id].img}",
                    width: 20,
                    height: 20,
                  )),
              Expanded(
                  flex: 9,
                  child: Padding(
                    padding: const EdgeInsets.only(left: 30),
                    child: Text("${dataWork[id].textForm}"),
                  ))
            ],
          ),
          SizedBox(
            height: 20,
          ),
        ],
      ),

    );
  }
}
