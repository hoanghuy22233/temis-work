
import 'package:base_code_project/app/constants/barrel_constants.dart';
import 'package:base_code_project/app/constants/color/color.dart';
import 'package:base_code_project/presentation/common_widgets/barrel_common_widgets.dart';
import 'package:flutter/material.dart';

class FollowTimekeepingScreen extends StatefulWidget {
  @override
  FollowTimekeepingScreenState createState() => FollowTimekeepingScreenState();
}

class FollowTimekeepingScreenState extends State<FollowTimekeepingScreen> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.grey[100],
      body: SafeArea(
        top: true,
        child: RefreshIndicator(
          onRefresh: () async {
            await Future.delayed(Duration(seconds: 2));
            return true;
          },
          color: AppColor.PRIMARY_COLOR,
          backgroundColor: Colors.blue,
          child: Column(
                children: [
                  _buildAppbar('Theo dõi chấm công'),
                  Expanded(
                    child: _buildContent()
                  )
                ],
              ),
          ),
      ),
      );
  }

  // Widget _buildContent() {
  //   return _buildCondition();
  // }

  Widget _buildAppbar(String title) => WidgetAppbarWithColor(
    backgroundColor: Colors.blue,
    textColor: Colors.white,
    left: [
      WidgetAppbarMenuBack(),
    ],
    title: title,
  );

  Widget _buildContent(){
    return Container(
      child: SingleChildScrollView(
        child: Column(
          children: [
            _buildNameCard(),
            _buildDetail()
          ],
        ),
      ),
    );
  }

  _buildNameCard() {
    return Container(
      padding: EdgeInsets.all(10),
      child: Column(
        children: [
          Container(
            height: 40,
            width: MediaQuery.of(context).size.width,
            color: Colors.red[400],
            child: Center(child: Text('24 NHÂN VIÊN', style: AppStyle.DEFAULT_MEDIUM_BOLD.copyWith(color: Colors.white, fontSize: 18),)),
          ),
          Container(
            height: 90,
            width: MediaQuery.of(context).size.width,
            color: Colors.red[300],
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Expanded(
                  flex: 2,
                  child: _itemTotal(number: 9, title: 'đã chấm công', ontap: (){AppNavigator.navigateDetailUserAndMap();}),
                ),
                Expanded(
                  child: _itemTotal(number: 0, title: 'sai vị trí'),
                ),
                Expanded(
                  flex: 2,
                  child: _itemTotal(number: 15, title: 'chưa chấm công', ontap: (){AppNavigator.navigateDetailUserAndMap();}),
                ),
              ],
            ),
          )
        ],
      ),
    );
  }

  _buildDetail(){
    return Container(
      color: Colors.white,
      margin: EdgeInsets.symmetric(horizontal: 10),
      child: Column(
        children: [
          _itemDetail(icon: 'assets/icons/plane.png',title: 'Công tác', number: 0),
          _itemDetail(icon:'assets/icons/alarm.png', title: 'Đi muộn', number: 5, ontap: (){AppNavigator.navigateDetailUserAndMap();}),
          _itemDetail(icon: 'assets/icons/alarm.png', title: 'Về sớm',number: 0),
          _itemDetail(icon: 'assets/icons/user.png', title:'Sai khuôn mặt', number:0, ontap: (){AppNavigator.navigateDetailUserAndMap();}),
          _itemDetail(icon: 'assets/icons/wifi.png', title:'Sai wifi',number: 0),
        _itemDetail(icon: 'assets/icons/plane.png',title: 'Công tác', number: 0),
          _itemDetail(icon:'assets/icons/alarm.png', title: 'Đi muộn', number: 5, ontap: (){AppNavigator.navigateDetailUserAndMap();}),
          _itemDetail(icon: 'assets/icons/alarm.png', title: 'Về sớm',number: 0),
          _itemDetail(icon: 'assets/icons/user.png', title:'Sai khuôn mặt', number:0, ontap: (){AppNavigator.navigateDetailUserAndMap();}),
          _itemDetail(icon: 'assets/icons/wifi.png', title:'Sai wifi',number: 0),
        ],
      ),
    );
  }

  _itemTotal({int number, String title, Function ontap}){
    return GestureDetector(
      onTap: ontap,
      child: Container(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Text(number.toString(), style: TextStyle(color: Colors.white, fontSize: 20),),
            SizedBox(height: 10,),
            Center(child: Text(title, style: TextStyle(color: Colors.white, fontSize: 12),)),
          ],
        ),
      ),
    );
  }

  _itemDetail({String icon, String title, int number, Function ontap}){
    return GestureDetector(
      onTap: ontap,
      child: Container(
        height: 60,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Expanded(
              flex: 2,
              child: Container(
                height: 30,
                width: 30,
                child: Image.asset(icon),
              ),
            ),
            Expanded(
                flex: 5,
                child: Text(title, style: TextStyle(color: Colors.black, fontSize: 16),)
            ),
            Expanded(
                flex: 1,
                child: Text(number.toString(), style: TextStyle(color: Colors.black, fontSize: 20),)
            ),
            Expanded(
              flex: 1,
              child: Container(
                height: 20,
                width: 20,
                child: Icon(
                  Icons.chevron_right,
                  color: Colors.grey[400],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
