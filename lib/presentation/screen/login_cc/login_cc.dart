import 'package:base_code_project/app/auth_bloc/authentication_bloc.dart';
import 'package:base_code_project/app/constants/navigator/navigator.dart';
import 'package:base_code_project/model/repo/user_repository.dart';
import 'package:base_code_project/presentation/common_widgets/widget_touch_hide_keyboard.dart';
import 'package:base_code_project/presentation/screen/login/widget_login_form.dart';
import 'package:base_code_project/presentation/screen/login_cc/widget_login_cc_form.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';


class LoginCCScreen extends StatefulWidget {
  @override
  LoginScreenState createState() => LoginScreenState();
}

class LoginScreenState extends State<LoginCCScreen> {
  @override
  Widget build(BuildContext context) {
    var userRepository = RepositoryProvider.of<UserRepository>(context);
    return WidgetTouchHideKeyBoard(
      child: Scaffold(
        body: SafeArea(
          top: true,
          child: Column(
            children: [
              Expanded(
                  child: ListView(
                    padding: EdgeInsets.zero,
                    scrollDirection: Axis.vertical,
                    children: [
                      SizedBox(
                        height: 30,
                      ),
                      Container(
                        margin: EdgeInsets.only(right: 50),
                        child: Image.asset(
                          "assets/images/logoTemis2.png",
                          height: MediaQuery.of(context).size.height/4,
                          width: MediaQuery.of(context).size.width/2,
                        ),
                      ),
                      SizedBox(
                        height: 20,
                      ),
                      _buildLoginForm(),
                    ],
                  )),
              Container(
                padding: EdgeInsets.only(bottom: 15),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    GestureDetector(
                      onTap: () {
                        // AppNavigator.navigateForgotPassword();
                      },
                      child: Text(
                        'Chưa có tài khoản?',
                        style: TextStyle(
                          fontSize: 16,
                          color: Colors.black,
                        ),
                      ),
                    ),
                    SizedBox(
                      width: 1,
                    ),

                    GestureDetector(
                      onTap: () {
                        AppNavigator.navigateRegister();
                      },
                      child: Text(
                        'Đăng ký',
                        style: TextStyle(
                            fontSize: 16,
                            decoration: TextDecoration.underline, color: Colors.blue),
                      ),
                    ),

                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  _buildLoginForm() => WidgetLoginCCForm();
}
