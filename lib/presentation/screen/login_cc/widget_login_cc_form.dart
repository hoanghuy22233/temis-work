import 'package:base_code_project/app/constants/color/color.dart';
import 'package:base_code_project/app/constants/navigator/navigator.dart';
import 'package:base_code_project/app/constants/string/validator.dart';
import 'package:base_code_project/app/constants/style/style.dart';
import 'package:base_code_project/presentation/common_widgets/widget_spacer.dart';
import 'package:base_code_project/presentation/screen/login/bloc/login_event.dart';
import 'package:base_code_project/utils/snackbar/get_snack_bar_utils.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_icons/flutter_icons.dart';


class WidgetLoginCCForm extends StatefulWidget {
  @override
  _WidgetLoginCCFormState createState() => _WidgetLoginCCFormState();
}

class _WidgetLoginCCFormState extends State<WidgetLoginCCForm> {

  final TextEditingController _usernameController = TextEditingController();
  final TextEditingController _passwordController = TextEditingController();

  bool obscurePassword = true;
  bool autoValidate = false;

  bool get isPopulated =>
      _usernameController.text.isNotEmpty &&
          _passwordController.text.isNotEmpty;

  @override
  void initState() {
    super.initState();
    _usernameController.text = 'letuanhuy98@gmail.com';
    _passwordController.text = '123456789';
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.symmetric(horizontal: 15, vertical: 10),
      child: Form(
        child: Column(
          children: [
            Container(
              width: MediaQuery.of(context).size.width / 1.2,
              child: Card(
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(50.0),
                ),
                elevation: 3,
                child: Container(
                  margin: EdgeInsets.only(left: 20),
                  child: TextFormField(
                    enableInteractiveSelection: false,
                    controller: _usernameController,
                    onChanged: (value) {
                      // _loginBloc.add(LoginUsernameChanged(email: value));
                    },
                    validator: AppValidation.validateUserName(
                        "Vui lòng điền tài khoản"),
                    decoration: InputDecoration(
                      border: InputBorder.none,
                      hintText: "Số điện thoại hoặc email",
                      hintStyle: TextStyle(color: Colors.grey[400]),
                    ),
                    textAlign: TextAlign.start,
                  ),
                ),
              ),
            ),
            WidgetSpacer(
              height: 10,
            ),
            Container(
              width: MediaQuery.of(context).size.width / 1.2,
              child: Card(
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(50.0),
                ),
                elevation: 3,
                child: Container(
                  margin: EdgeInsets.symmetric(horizontal: 20),
                  child: Row(
                    children: [
                      Expanded(
                        flex: 8,
                        child: TextFormField(
                          enableInteractiveSelection: false,
                          controller: _passwordController,
                          obscureText: obscurePassword,
                          onChanged: (value) {
                            // _loginBloc
                            //     .add(LoginPasswordChanged(password: value));
                          },
                          validator: AppValidation.validatePassword(
                              "Vui lòng nhập mật khẩu"),
                          decoration: InputDecoration(
                            border: InputBorder.none,
                            hintText: "Mật khẩu",
                            hintStyle: TextStyle(color: Colors.grey[400]),
                          ),
                          textAlign: TextAlign.start,
                        ),
                      ),
                      Expanded(
                        flex: 2,
                        child: IconButton(
                          icon: Icon(
                            obscurePassword
                                ? MaterialCommunityIcons.eye_outline
                                : MaterialCommunityIcons.eye_off_outline,
                            color: AppColor.GREY,
                          ),
                          onPressed: () {
                            setState(() {
                              obscurePassword = !obscurePassword;
                            });
                          },
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ),
            WidgetSpacer(
              height: 20,
            ),
            Align(
              alignment: Alignment.centerRight,
              child: Container(
                padding: EdgeInsets.only(right: 20),
                child: GestureDetector(
                  onTap: () {
                    AppNavigator.navigateForgotPassword();
                  },
                  child: Text(
                    'Quên mật khẩu?',
                    style: TextStyle(
                        fontSize: 16,
                        color: Colors.black,
                        decoration: TextDecoration.underline),
                  ),
                ),
              ),
            ),
            WidgetSpacer(
              height: 15,
            ),
            _buildButtonLogin(),
            WidgetSpacer(
              height: 15,
            ),
          ],
        ),
      ),
    );

  }

  _buildButtonLogin() {
    return Container(
      width: MediaQuery.of(context).size.width / 1.2,
      height: MediaQuery.of(context).size.height / 12,
      child: GestureDetector(
        onTap: () {
          AppNavigator.navigateNavigation();

        },
        child: Card(
          elevation: 2,
          shape:
          RoundedRectangleBorder(borderRadius: BorderRadius.circular(50)),
          color:  Colors.blue,
          child: Center(
              child: Text(
                "Đăng nhập",
                style: AppStyle.DEFAULT_MEDIUM.copyWith(color: AppColor.WHITE),
              )),
        ),
      ),
    );
  }

}
