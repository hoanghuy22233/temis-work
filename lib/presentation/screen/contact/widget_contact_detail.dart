import 'package:base_code_project/app/constants/barrel_constants.dart';
import 'package:base_code_project/presentation/common_widgets/barrel_common_widgets.dart';
import 'package:flutter/material.dart';
import 'package:url_launcher/url_launcher.dart';
class WidgetContactDetail extends StatelessWidget {
  WidgetContactDetail({Key key}) : super(key: key);
  // final _url = 'tel:0869258726';
  // final Uri _emailLaunchUri = Uri(
  //     scheme: 'mailto',
  //     path: 'letuanhuy98@gmail.com',
  //     queryParameters: {
  //       'subject': 'Example Subject & Symbols are allowed!'
  //     }
  // );
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.all(8.0),
      child: Column(
        children: [
          Text(
            "CÔNG TY TNHH GIẢI PHÁP CÔNG NGHỆ - TRUYỀN THÔNG TEMIS",
            textAlign: TextAlign.center,
            style: TextStyle(fontWeight: FontWeight.bold),
          ),
          SizedBox(
            height: 15,
          ),
          Row(
            children: [
              Expanded(
                  flex: 1,
                  child: Image.asset(
                    "assets/icons/ic_address.png",
                    width: 25,
                    height: 25,
                  )),
              Expanded(
                  flex: 9,
                  child: Text(": Số 10/115 Hồ Sen - Lê Chân - Hải Phòng",style: TextStyle(color: Colors.blue,),))
            ],
          ),
          SizedBox(
            height: 15,
          ),
          Row(
            children: [
              Expanded(
                flex: 1,
                child: Image.asset(
                  "assets/icons/ic_phone.png",
                  width: 25,
                  height: 25,
                ),
              ),
              Text(": "),
              Expanded(flex: 9,
                  // child: Text(": 092 846 84 83")
                // child: RaisedButton(
                //   onPressed: _launchURL,
                //   child: Text('0869258726'),
                // ),
                child: GestureDetector(
                  onTap: () {
                    launch("tel:0869258726");

                    // _launchURL();
                  },
                  child: Text('0869258726',style: TextStyle(color: Colors.blue, decoration: TextDecoration.underline,),),
                ),
              )
            ],
          ),
          SizedBox(
            height: 15,
          ),
          Row(
            children: [
              Expanded(
                flex: 1,
                child: Image.asset(
                  "assets/images/internet.png",
                  width: 25,
                  height: 25,
                ),
              ),
              Text(": "),
              Expanded(flex: 9, child: GestureDetector(
                onTap: () {
                  launch("http://temis.vn");
                },
                  child: Text("http://temis.vn/",style: TextStyle(color: Colors.blue, decoration: TextDecoration.underline,),)))
            ],
          ),
          SizedBox(
            height: 15,
          ),
          Row(
            children: [
              Expanded(
                flex: 1,
                child: Image.asset(
                  "assets/images/gmail.png",
                  width: 25,
                  height: 25,
                ),
              ),
              Text(": "),
              Expanded(flex: 9,
                  child: GestureDetector(onTap: () {
                    launch("mailto:temisvietnam@gmail.com?subject=News&body=New%20plugin");
                  },child: Text("temisvietnam@gmail.com ",style: TextStyle(color: Colors.blue, decoration: TextDecoration.underline,),))
              )
            ],
          ),
        ],
      ),
    );
  }
  // void _launchURL() async =>
  //     await canLaunch(_url) ? await launch(_url) : throw 'Could not launch $_url';
  // // void _launchURLEmail() async =>
  // //     await canLaunch(_urlEmail) ? await launch(_urlEmail) : throw 'Could not launch $_urlEmail';
}
