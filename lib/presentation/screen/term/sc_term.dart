import 'package:base_code_project/app/constants/barrel_constants.dart';
import 'package:base_code_project/presentation/common_widgets/barrel_common_widgets.dart';
import 'package:base_code_project/presentation/screen/term/widget_term_appbar.dart';
import 'package:flutter/material.dart';


class TermScreen extends StatefulWidget {
  @override
  _TermScreenState createState() => _TermScreenState();
}

class _TermScreenState extends State<TermScreen>
    with AutomaticKeepAliveClientMixin<TermScreen> {
  @override
  void initState() {
    super.initState();
//    BlocProvider.of<ProfileBloc>(context).add(LoadProfile());
  }

  // var logger = Logger(
  //   printer: PrettyPrinter(),
  // );

  @override
  Widget build(BuildContext context) {
    super.build(context);
    return SafeArea(
      child: Scaffold(
        body: Container(child: _buildContent()),
      ),
    );

  }

  _buildContent() {
    return Column(
      children: [_buildAppbar(), Expanded(child: _buildMenu())],
    );

  }

  Widget _buildAppbar() => WidgetTermAppbar();

  Widget _buildMenu() {
    return SingleChildScrollView(
      physics: BouncingScrollPhysics(),
      child: Padding(
        padding: const EdgeInsets.all(AppValue.APP_HORIZONTAL_PADDING),
        child: Column(
          children: [
            Text("Nhân viên vui lòng đọc kỹ nội quy và chính sách của công ty",textAlign: TextAlign.center,style: TextStyle(fontWeight: FontWeight.bold),),
            // HtmlWidget(
            //   state.appConfig.useOfTerms,
            // ),
            WidgetSpacer(
              height: 100,
            )
          ],
        ),
      ),
    );
  }

  @override
  bool get wantKeepAlive => true;
}
