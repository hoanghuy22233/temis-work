import 'package:base_code_project/app/constants/barrel_constants.dart';
import 'package:base_code_project/app/constants/value/value.dart';
import 'package:base_code_project/presentation/common_widgets/barrel_common_widgets.dart';
import 'package:base_code_project/presentation/common_widgets/widget_appbar_profile.dart';
import 'package:base_code_project/presentation/screen/detail_staff_and_map/map/map_screen.dart';
import 'package:base_code_project/presentation/screen/detail_staff_and_map/staff/staff_screen.dart';
import 'package:bubble_tab_indicator/bubble_tab_indicator.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';


// class MyProfileWork extends StatelessWidget {
//   // This widget is the root of your application.
//   @override
//   Widget build(BuildContext context) {
//     return MaterialApp(
//       debugShowCheckedModeBanner: false,
//       theme: ThemeData(
//         brightness: Brightness.light,
//         primaryColor: AppColor.PRIMARY_COLOR,
//         accentColor: AppColor.PRIMARY_COLOR,
//         hoverColor: AppColor.PRIMARY_COLOR,
//         fontFamily: 'Montserrat',
//       ),
//       navigatorObservers: [],
//       home: ProfileWorkScreen(),
//     );
//   }
// }

class DetailStaffAndMap extends StatefulWidget {
  final int id;
  DetailStaffAndMap({Key key, this.id}) : super(key: key);

  @override
  _DetailStaffAndMapState createState() => _DetailStaffAndMapState();
}

class _DetailStaffAndMapState extends State<DetailStaffAndMap>
    with SingleTickerProviderStateMixin {
  final PageController _pageController = PageController(initialPage: 0);
  TabController _tabController;
  @override
  void initState() {
    super.initState();
    _tabController = new TabController(vsync: this, length: 2);
    _tabController.addListener(() {
      print('my index is' + _tabController.index.toString());
    });
  }

  @override
  void dispose() {
    _tabController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: SafeArea(
          top: true,
          child: Container(
            color: Colors.grey[100],
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Stack(
                  children: [
                    Container(
                      color: Colors.grey[100],
                      // height: MediaQuery.of(context).size.height * 0.1,
                      child: _buildAppbar(),
                    ),
                    _buildTabBarMenu(),
                  ],
                ),
                Expanded(
                    child: TabBarView(
                      controller: _tabController,
                      children: [
                        _staffScreen(),
                        _mapScreen()],
                    )),
              ],
            ),
          ),
        ));
  }

  _buildAppbar() => WidgetAppbarWithColor(
    backgroundColor: Colors.blue,
    textColor: Colors.white,
    title: "Chi tiết",
    left: [
      WidgetAppbarMenuBack(),
    ],
  );

  Widget _buildTabBarMenu() {
    return new Container(
      margin: EdgeInsets.only(top: 40, left: 10, right: 10),
      decoration: BoxDecoration(
          color: Colors.grey[100],
          borderRadius: BorderRadius.circular(500)),
      height: AppValue.ACTION_BAR_HEIGHT,
      child: new TabBar(
        controller: _tabController,
        tabs: [
          Tab(
            text: "Nhân viên",
          ),
          Tab(
            text: "Bản đồ",
          ),
        ],
        labelStyle: AppStyle.DEFAULT_SMALL,
        unselectedLabelStyle: AppStyle.DEFAULT_SMALL,
        labelColor: Colors.blue,
        unselectedLabelColor: AppColor.BLACK,
        indicator: new BubbleTabIndicator(
          indicatorHeight: AppValue.ACTION_BAR_HEIGHT - 10,
          indicatorColor: Colors.white,
          tabBarIndicatorSize: TabBarIndicatorSize.tab,
          indicatorRadius: 500,
        ),
      ),
    );
  }

  Widget _staffScreen() => StaffScreen();
  Widget _mapScreen() => MapScreen();
}
