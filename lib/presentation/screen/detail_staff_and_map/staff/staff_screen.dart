import 'package:base_code_project/app/constants/barrel_constants.dart';
import 'package:base_code_project/presentation/screen/detail_staff_and_map/widget_item_staff.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import 'package:flutter_collapse/flutter_collapse.dart';

class StaffScreen extends StatefulWidget {
  StaffScreen({Key key, this.title}) : super(key: key);
  final String title;

  @override
  _StaffScreenState createState() => _StaffScreenState();
}

class _StaffScreenState extends State<StaffScreen>
    with AutomaticKeepAliveClientMixin<StaffScreen> {
  @override
  void initState() {
    super.initState();
  }

  @override
  bool get wantKeepAlive => true;

  @override
  Widget build(BuildContext context) {
    super.build(context);
    return _buildContent();
    //   BlocBuilder<ProfileBloc, ProfileState>(builder: (context, state) {
    //   if (state is ProfileLoaded) {
    //     return _buildContent(state);
    //   } else if (state is ProfileLoading) {
    //     return Center(
    //       child: Container(
    //         height: 100,
    //         width: 100,
    //         child: Lottie.asset(
    //           'assets/lottie/trail_loading.json',
    //         ),
    //       ),
    //     );
    //   } else if (state is ProfileNotLoaded) {
    //     return Center(
    //       child: Text('${state.error}'),
    //     );
    //   } else {
    //     return Center(
    //       child: Text('Unknown state'),
    //     );
    //   }
    // }); // This trailing comma makes auto-formatting nicer for build methods.
  }

  TextEditingController _controller;
  bool status1 = true;
  bool status2 = true;
  bool status3 = true;

  _buildContent() {
    return RefreshIndicator(
      onRefresh: () async {

        await Future.delayed(Duration(seconds: 2));
        return true;
      },
      color: AppColor.PRIMARY_COLOR,
      backgroundColor: Colors.blue,
      child: Column(
        children: [
          Container(
            margin: EdgeInsets.symmetric(horizontal: 5),
            child: Card(
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(25.0),
              ),
              child: Container(
                padding: EdgeInsets.only(left: 20),
                height: 45,
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Icon(
                      Icons.search,
                      color: Colors.black,
                    ),
                    SizedBox(
                      width: 10,
                    ),
                    Container(
                      width: MediaQuery.of(context).size.width*0.66,
                      child: Center(
                        child: TextField(
                          controller: _controller,
                          onChanged: (value){
                            //numberText = value.length;
                          },
                          decoration: InputDecoration(
                            labelText: 'Tìm kiếm',
                            border: InputBorder.none,
                          ),
                        ),
                      ),
                    ),
                    GestureDetector(
                      child: Container(
                        decoration: BoxDecoration(
                          color: Colors.blue,
                          borderRadius: BorderRadius.circular(25)
                        ),

                          height: 35,
                          width: 60,
                          child: Center(
                              child: Text('Tìm', style: TextStyle(color: Colors.white),
                              )
                          )
                      ),

                    ),
                  ],
                ),
              ),
            ),
          ),
          Expanded(
            child: Container(
              margin: EdgeInsets.only(top: 10, left: 10, right: 10),
              child: SingleChildScrollView(
                child: Column(
                  children: [
                    Collapse(
                      padding: EdgeInsets.all(0),
                      title: Text('Phòng IT'),
                      body: Container(
                        color: Colors.white,
                        padding: EdgeInsets.symmetric(vertical: 10, horizontal: 15),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            WidgetItemStaff(name: 'Nguyễn Hoàng Huy',email: 'hoanghuy@temisvn', status: 'Chấm công vào', time: '8:23  17/03/2021',),
                            WidgetItemStaff(name: 'Nguyễn Thị Lan Hương',email: 'lanhuong@temisvn', status: 'Chấm công vào', time: '8:25  17/03/2021',),
                            WidgetItemStaff(name: 'Nguyễn Bùi Tuấn Anh',email: 'tuananh@temisvn', status: 'Chấm công vào', time: '8:29  17/03/2021',),
                            WidgetItemStaff(name: 'Lê Tuấn Huy',email: 'tuanhuy@temisvn', status: 'Chấm công vào', time: '8:20  17/03/2021',),
                          ],
                        ),
                      ),
                      value: status1,
                      onChange: (bool value) {
                        setState(() {
                          status1 = value;
                        });
                      },
                    ),
                    Collapse(
                      padding: EdgeInsets.all(0),
                      title: Text('Quản lý'),
                      body: Container(
                        color: Colors.white,
                        padding: EdgeInsets.symmetric(vertical: 10, horizontal: 15),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            WidgetItemStaff(name: 'Nguyễn Hoàng Huy',email: 'hoanghuy@temisvn', status: 'Chấm công vào', time: '8:23  17/03/2021',),
                            WidgetItemStaff(name: 'Nguyễn Thị Lan Hương',email: 'lanhuong@temisvn', status: 'Chấm công vào', time: '8:25  17/03/2021',),
                            WidgetItemStaff(name: 'Nguyễn Bùi Tuấn Anh',email: 'tuananh@temisvn', status: 'Chấm công vào', time: '8:29  17/03/2021',),
                            WidgetItemStaff(name: 'Lê Tuấn Huy',email: 'tuanhuy@temisvn', status: 'Chấm công vào', time: '8:20  17/03/2021',),
                          ],
                        ),
                      ),
                      value: status2,
                      onChange: (bool value) {
                        setState(() {
                          status2 = value;
                        });
                      },
                    ),
                    Collapse(
                      padding: EdgeInsets.all(0),
                      title: Text('Phòng Maketing'),
                      body: Container(
                        color: Colors.white,
                        padding: EdgeInsets.symmetric(vertical: 10, horizontal: 15),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            WidgetItemStaff(name: 'Nguyễn Hoàng Huy',email: 'hoanghuy@temisvn', status: 'Chấm công vào', time: '8:23  17/03/2021',),
                            WidgetItemStaff(name: 'Nguyễn Thị Lan Hương',email: 'lanhuong@temisvn', status: 'Chấm công vào', time: '8:25  17/03/2021',),
                            WidgetItemStaff(name: 'Nguyễn Bùi Tuấn Anh',email: 'tuananh@temisvn', status: 'Chấm công vào', time: '8:29  17/03/2021',),
                            WidgetItemStaff(name: 'Lê Tuấn Huy',email: 'tuanhuy@temisvn', status: 'Chấm công vào', time: '8:20  17/03/2021',),
                          ],
                        ),
                      ),
                      value: status3,
                      onChange: (bool value) {
                        setState(() {
                          status3 = value;
                        });
                      },
                    ),
                  ],
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }


}
