import 'package:base_code_project/app/constants/barrel_constants.dart';
import 'package:bubble_tab_indicator/bubble_tab_indicator.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class WidgetItemStaff extends StatefulWidget {
  final String image, name, email, status, time, picture;

  const WidgetItemStaff({
    Key key,
    this.image = 'assets/icons/man.png',
    this.name,
    this.email,
    this.status,
    this.time,
    this.picture = '1 ảnh'
  }) : super(key: key);


  @override
  _WidgetItemStaffState createState() => _WidgetItemStaffState();
}

class _WidgetItemStaffState extends State<WidgetItemStaff> {
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(vertical: 10),
      child: Row(
        children: [
          Expanded(
            flex: 1,
            child: Center(
              child: Container(
                child: ClipRRect(
                    borderRadius: BorderRadius.circular(100),
                    child: Image.asset(widget.image)
                ),
              ),
            )
          ),
          Expanded(
            flex: 4,
            child: Container(
              padding: EdgeInsets.only(left: 10),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  Text(widget.name, style: AppStyle.DEFAULT_LARGE_BOLD,),
                  _item('assets/icons/email.png', widget.email, Colors.black),
                  _item('assets/icons/exclamation-mark.png', widget.status, Colors.red),
                  _item('assets/icons/clock.png', widget.time, Colors.black),
                  _item('assets/icons/camera.png', widget.picture, Colors.black),
                  Padding(
                    padding: const EdgeInsets.symmetric(vertical: 5),
                    child: Divider(
                      height: 1,
                      thickness: 1,
                      color: Colors.grey,
                    ),
                  )
                ],
              ),
            )
          ),

        ],
      ),
    );
  }

  _item(String icon, String content, Color contentColor){
    return Padding(
      padding: EdgeInsets.only(top: 5),
      child: Row(
        children: [
          Container(
            height: 20,
            width: 20,
            child: Image.asset(icon),
          ),
          SizedBox(width: 10,),
          Text(content, style: AppStyle.DEFAULT_MEDIUM.copyWith(color: contentColor),)
        ],
      ),
    );
  }
}


