import 'dart:async';
import 'dart:typed_data';
import 'package:android_intent/android_intent.dart';
import 'package:base_code_project/model/repo/user_repository.dart';
import 'package:base_code_project/presentation/common_widgets/barrel_common_widgets.dart';
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:geolocator/geolocator.dart';
import 'package:lottie/lottie.dart' as lottie;
import 'package:shared_preferences/shared_preferences.dart';
import 'package:modal_bottom_sheet/modal_bottom_sheet.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:modal_bottom_sheet/modal_bottom_sheet.dart';


class MapScreen extends StatefulWidget {
  @override
  _MapScreenState createState() => _MapScreenState();
}

class _MapScreenState extends State<MapScreen> {

  GoogleMapController mapController;
  static LatLng _center = const LatLng(21.007353, 105.781346);
  static LatLng DEST_LOCATION = LatLng(21.027353, 105.721346);
  Set<Marker> _markers = {};
  Set<Circle> _circles = {};

  Set<Polyline> _polylines = {};
  List<LatLng> polylineCoordinates = [];

  String googleAPIKey = 'AIzaSyC5sashDXaKHiNRR4DO4fDTnRQHMWnNbLI';


  LatLng _currentMapPosition = _center;
  MapType _currentMapType = MapType.normal;
  bool isLoading = true;
  String apiToken;
  UserRepository repository;

  void _onCameraMove(CameraPosition position) {
    _currentMapPosition = position.target;
  }

  void _onMapTypeButtonPressed() {
    setState(() {
      _currentMapType = _currentMapType == MapType.normal
          ? MapType.satellite
          : MapType.normal;
    });
  }
  BitmapDescriptor pinLocationIcon;

  Completer<GoogleMapController> _controller = Completer();

  final Geolocator geolocator = Geolocator()..forceAndroidLocationManager;
  Future _checkGps() async {
    if (!(await Geolocator().isLocationServiceEnabled())) {
      if (Theme.of(context).platform == TargetPlatform.android) {
        showDialog(
          context: context,
          builder: (BuildContext context) {
            return AlertDialog(
              title: Text("Không thể lấy vị trí hiện tại của bạn!"),
              content:
              const Text('Hãy bật GPS và thử lại'),
              actions: <Widget>[
                FlatButton(
                  child: Text('Ok'),
                  onPressed: () {
                    final AndroidIntent intent = AndroidIntent(
                        action: 'android.settings.LOCATION_SOURCE_SETTINGS');
                    intent.launch();
                    Navigator.of(context, rootNavigator: true).pop();
                  },
                ),
              ],
            );
          },
        );
      }
    }
  }

  _getCurrentLocation(GoogleMapController controller) {
    _checkGps();
    //WidgetCircleProgress();
    geolocator
        .getCurrentPosition()
        .then((Position position) {
      setState(() {
        _currentPosition = position;
        print("${_currentPosition.latitude}, ${_currentPosition.longitude}");

        for(Marker marker in _markers){
          if(marker.markerId.value.trim() == userName){
            //print('id marker: ${_markers.}');
            _markers.remove(marker);
            //marker.position = LatLng(_currentPosition.latitude, _currentPosition.longitude);
          }
        }
      });
    }).catchError((e) {
      print(e);
    });
    //BlocProvider.of<UserLocationBloc>(context).add(RefreshUserLocation());
    Future.delayed(Duration(seconds: 2), () {
      //Navigator.of(context, rootNavigator: true).pop('dialog');

      controller.animateCamera(
        CameraUpdate.newCameraPosition(
          CameraPosition(target: LatLng(_currentPosition.latitude, _currentPosition.longitude),zoom: 15),
        ),
      );
      setState(() {
        final marker1 = Marker(
          markerId: MarkerId('Vị trí của tôi'),
          position: LatLng(_currentPosition.latitude, _currentPosition.longitude),
          infoWindow: InfoWindow(
            title: 'Vị trí của tôi',
            onTap: () {

            },
          ),
          //icon: BitmapDescriptor.fromAsset('assets/images/rec.png'),
          icon: BitmapDescriptor.defaultMarker,
        );
        _markers.add(marker1);
      });
    });

  }

  Position _currentPosition;

  String userName='';
  double lat, long;


  @override
  void initState() {
    super.initState();
    _getCurrentLocation(mapController);
  }


  @override
  Widget build(BuildContext context) {
    Timer timer = Timer(Duration(seconds: 3), () {
      setState(() {

        isLoading = false;
      });
    });

    return Scaffold(
      body: SafeArea(
        top: true,
        child: Stack(
          children: <Widget>[
            GoogleMap(
                initialCameraPosition: CameraPosition(
                  target: _center,
                  zoom: 12.0,
                ),
                mapType: _currentMapType,
                markers: _markers,
                circles: _circles,
                polylines: _polylines,
                onCameraMove: _onCameraMove,
                onMapCreated: (GoogleMapController controller) {
                  mapController = controller;
                  _controller.complete(mapController);
                  setState(() {
                    // for(int i=0; i < state.user.length ; i++){
                    //   if(state.user[0].lat != null && state.user[0].lat!= null){
                    //     var marker = Marker(
                    //       markerId: MarkerId(state.user[i].name),
                    //       position: LatLng(double.parse(state.user[i].lat), double.parse(state.user[i].long)),
                    //       infoWindow: InfoWindow(
                    //         title: state.user[i].name,
                    //         onTap: () {
                    //           print('id user: ${state.user[i].id.toString()}');
                    //           showDialog(
                    //               context: context,
                    //               builder: (BuildContext context) {
                    //                 return WidgetDialogMapInfo(
                    //                   title: AppLocalizations.of(context).translate('map.information'),
                    //                   id: state.user[i].id,
                    //                 );
                    //               });
                    //         },
                    //       ),
                    //       icon: BitmapDescriptor.defaultMarker,
                    //     );
                    //     _markers.add(marker);
                    //   }
                    // }
                  }
                  );
                }
            ),

            Positioned(
              bottom: 110,
              right: 15,
              child: GestureDetector(
                onTap: (){
                  _getCurrentLocation(mapController);
                },
                child: Container(
                    height: 35,
                    width: 35,
                    decoration: BoxDecoration(shape: BoxShape.circle, color: Colors.white),
                    child: Icon(Icons.my_location, color: Colors.black,)
                ),
              ),
            ),

          ],
        ),
      ),

    );
  }
}
